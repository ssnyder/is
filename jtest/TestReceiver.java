import ipc.*;
import is.*;
import java.lang.reflect.*;
import java.lang.Runnable;
 
public class TestReceiver extends InfoWatcher
{
    private boolean is_re;
    
    public TestReceiver( Partition p, String name )
    {
	super ( p, name );
	this.is_re = false;
    }
    
    public TestReceiver( Partition p, String sn, String r )
    {
	super ( p, sn, new Criteria( java.util.regex.Pattern.compile( r ) ) );
	this.is_re = true;
    }
    
    public void infoCreated( InfoEvent e )
    {
        System.out.print( "TestReceiver:: " + ( is_re ? "regular expression callback: " : "simple callback: " ) );
        System.out.println( "TestReceiver:: Info " + e.getName() + " is created" );
        AnyInfo ai = new AnyInfo( );
        try {
    	    e.getValue( ai );
    	    System.out.println( ai.toString() );
        } catch (Exception ex) {
    	    System.err.println(ex.getMessage());
        }
    }
    
    public void infoUpdated( InfoEvent e )
    {
        System.out.print( "TestReceiver:: " + ( is_re ? "regular expression callback: " : "simple callback: " ) );
        System.out.println( "TestReceiver:: Info " + e.getName() + " is updated" );
	AnyInfo ai = new AnyInfo( );
	e.getValue( ai );    
	System.out.println( ai.toString() );
    
	InfoDocument id;
	try
	{
            id = new InfoDocument( getPartition(), ai );   
	}
	catch ( is.UnknownTypeException ex )
	{
            System.err.println( "No information about IS object type" );
            return;
	}
    
	for( int i = 0; i < ai.getAttributeCount(); i++ )
	{
            java.lang.Object attr = ai.getAttribute(i);
            InfoDocument.Attribute adoc = id.getAttribute(i);
            System.out.print( "Attribute \"" + adoc.getName() + "\" of type \"" 
                         + adoc.getType() + "\" - "
                         + adoc.getDescription() + " : " ); 
            
            if ( attr.getClass().isArray() )
            {
		for ( int j = 0; j < Array.getLength( attr ); j++ )
		{
		    System.out.print( Array.get( attr, j ) + " " );
		}
		System.out.println();
            }
            else
            {
		System.out.println( attr );
            }
        }
    }
    
    public void infoDeleted( InfoEvent e )
    {
        System.out.print( "TestReceiver:: " + ( is_re ? "regular expression callback: " : "simple callback: " ) );
        System.out.println( "TestReceiver:: Info " + e.getName() + " is deleted" );
	AnyInfo ai = new AnyInfo( );
	try {
            e.getValue( ai );
        }
        catch( Exception ex ) {
            System.err.println( ex );
        }
    
	System.out.println( ai.toString() );
    }

    public synchronized void infoSubscribed( InfoEvent e )
    {
        System.out.print( "TestReceiver:: " + ( is_re ? "regular expression callback: " : "simple callback: " ) );
        System.out.println( "TestReceiver:: Info " + e.getName() + " is subscribed" );
        AnyInfo ai = new AnyInfo( );
        try {
    	    e.getValue( ai );
    	    System.out.println( ai.toString() );
        } catch (Exception ex) {
    	    System.err.println(ex.getMessage());
        }
    }

    public static void main(String args[])
    {    
	if ( args.length < 2 )
	{
            System.err.println( "You must provide the IS server name, IS info name and subscription expression." );
            System.exit(2);
	}
    
	TestReceiver r1 = null , r2 = null;
    
	Partition p = ( args.length < 4 ) ? new Partition() : new Partition( args[3] );
    
	try
	{
            r1 = new TestReceiver( p, args[0] + "." + args[1] );
            r2 = new TestReceiver( p, args[0], args[2] );
            r1.subscribe();
            r2.subscribe();
            System.out.println( "TestReceiver:: subscription successful" );
	}
	catch ( Exception e )
	{
            System.err.println( e.getMessage() );
            System.exit( 1 );
	}
    
	///////////////////////////////////////////////////
	// Sleep for 10 seconds and stop the receiver
	///////////////////////////////////////////////////
	try
	{
            Thread.sleep( 10000 );
	}
	catch ( java.lang.InterruptedException e )
	{
            System.err.println( "TestReceiver:: Interrupted" );
            System.exit( 1 );
	}
    
	try
	{
    	    r1.unsubscribe();
    	    r2.unsubscribe();
    	    System.out.println( "TestReceiver:: Test completed successfully\n\n" );
	}
	catch ( Exception e )
	{
            System.err.println( e.getMessage() );
            System.exit( 1 );
	}
    }
    
    
    static void printArray( java.io.PrintStream out, java.lang.Object arr )
    {
	out.print( arr.getClass().getComponentType().getName() + "[" + Array.getLength( arr ) + "] = " );
	for ( int i = 0; i < Array.getLength( arr ); i++ )
	{
	    out.print( Array.get( arr, i ) + " " );
	}
	out.println();
    }
}
