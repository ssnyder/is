import ipc.*;
import is.*;
import java.lang.reflect.*;
 
public class ReadInfo
{
    public static void main(String args[])
    {	
	if ( args.length < 3 )
	{
	    System.err.println( "You must provide the IS server name, person name and employee name." );
	    System.exit( 1 );
	}
	
	String person_name = args[0] + "." + args[1];
	String employee_name = args[0] + "." + args[2];
	try
	{
	    Partition p = ( args.length < 4 ) ? new Partition() : new Partition( args[3] );
	    
	    PersonNamed person = new PersonNamed( p, person_name );
	    System.out.println( "Person's info id " + person_name );
	    person.checkout();
	    print( System.out , person );
	    System.out.println();
	    EmployeeNamed employee = new EmployeeNamed( p, employee_name );
	    System.out.println( "Employee's info id " + employee_name );
	    employee.checkout();
	    print( System.out , employee );
	    System.out.println();
	    
	    AnyInfo ai = new AnyInfo();
	    Repository repository = new Repository( p );
	    repository.getValue( person_name, ai );
	    	    
	    System.out.println( "Info has type \"" + ai.getType().getName() + "\":" );
	    printAnyInfo( System.out, ai, p, "    " );
	    System.out.println( "Info has type \"" + ai.getType().getName() + "\":" );
	    printAnyInfo( System.out, ai, p, "    " );
	    repository.getValue( employee_name, ai );
	    System.out.println( "Info has type \"" + ai.getType().getName() + "\":" );
	    printAnyInfo( System.out, ai, p, "    " );
	    
	}
	catch ( Exception e )
	{
	    System.err.println( e.getMessage() );
	    System.exit( 1 );
	}
    }
    
    static void printAnyInfo( java.io.PrintStream out, is.AnyInfo ai, Partition p, String offset ) throws is.UnknownTypeException
    {
	InfoDocument id = new InfoDocument( p, ai );
	for( int i = 0; i < ai.getAttributeCount(); i++ )
	{
	    byte type = ai.getAttributeType(i);
            java.lang.Object attr = ai.getAttribute(i);
	    InfoDocument.Attribute adoc = id.getAttribute(i);
            out.print( offset + "Attribute \"" + adoc.getName() + "\" of type \""
			     		+ adoc.getType() + "\" - "
			     		+ adoc.getDescription() + " : " );

	    if ( attr.getClass().isArray() )
	    {
		out.println( "(" + Array.getLength( attr ) + " elements) : " );
		for ( int j = 0; j < Array.getLength( attr ); j++ )
		{
		    if ( type != is.Type.INFO )
                    	out.print( Array.get( attr, j ) + " " );
                    else
                        printAnyInfo( out, (is.AnyInfo)Array.get( attr, j ), p, offset + offset );
		}
		if ( type != is.Type.INFO )
		    out.println();
	    }
	    else
	    {
		if ( type != is.Type.INFO )
		    out.println( attr );
                else
                {
		    out.println();
		    printAnyInfo( out, (is.AnyInfo)attr, p, offset + offset );
                }
	    }
	}
    }
    
    static void printArray( java.io.PrintStream out, java.lang.Object arr )
    {
	out.print( arr.getClass().getComponentType().getName() + "[" + Array.getLength( arr ) + "] = " );
	for ( int i = 0; i < Array.getLength( arr ); i++ )
	{
	    out.print( Array.get( arr, i ) + " " );
	}
	out.println();
    }
    
    static void print( java.io.PrintStream out , is.PersonNamed p )
    {
	out.println( "Person's name " + p.name );
	out.println( "Person's birhtday " + p.birth_date );
	out.println( "Person's sex " + p.sex );
	out.println( "Person's address: " );
	out.println( "	" + p.address.number );
	out.println( "	" + p.address.street );
	out.println( "	" + p.address.town );
	out.println( "	" + p.address.zip );
        for ( int i = 0; i < p.children.length; i++ )
        {
	    out.println( "Person's children :" );
	    out.println( "	" + p.children[i].name );
	    out.println( "	" + p.children[i].birth_date );
	    out.println( "	" + p.children[i].sex );
        }
    }
    
    static void print( java.io.PrintStream out , is.EmployeeNamed p )
    {
	out.println( "Employee's name " + p.name );
	out.println( "Employee's birhtday " + p.birth_date );
	out.println( "Employee's sex " + p.sex );
	out.println( "Employee's salary " + p.salary );
    }
}

