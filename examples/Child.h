#ifndef CHILD_H
#define CHILD_H

#include <is/info.h>

#include <string>
#include <ostream>
#include <owl/time.h>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * Describes a human's child
 * 
 * @author  produced by the IS generator
 */

class Child : public ISInfo {
public:
    enum Sex {male,female};


    /**
     * Person's name
     */
    std::string                   name;

    /**
     * Person's date of birth
     */
    OWLDate                       birth_date;

    /**
     * Person's sex
     */
    Sex                           sex;


    static const ISType & type() {
	static const ISType type_ = Child( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "name: " << name << "\t// Person's name" << std::endl;
	out << "birth_date: " << birth_date << "\t// Person's date of birth" << std::endl;
	out << "sex: " << sex << "\t// Person's sex";
	return out;
    }

    Child( )
      : ISInfo( "Child" )
    {
	initialize();
    }

    ~Child(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    Child( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << name << birth_date << sex;
    }

    void refreshGuts( ISistream & in ){
	in >> name >> birth_date >> sex;
    }

private:
    void initialize()
    {
	name = "Jone";
	sex = male;

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const Child & info ) {
    info.print( out );
    return out;
}

#endif // CHILD_H
