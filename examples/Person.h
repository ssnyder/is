#ifndef PERSON_H
#define PERSON_H

#include <is/info.h>

#include "Address.h"
#include "Child.h"
#include <string>
#include <ostream>
#include <vector>
#include <owl/time.h>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * Describes a human person
 * 
 * @author  produced by the IS generator
 */

class Person : public ISInfo {
public:
    enum Sex {male,female};


    /**
     * Person's name
     */
    std::string                   name;

    /**
     * Person's date of birth
     */
    OWLDate                       birth_date;

    /**
     * Person's sex
     */
    Sex                           sex;

    /**
     * Person's home address
     */
    Address                       address;

    /**
     * Person's children
     */
    std::vector<Child>            children;


    static const ISType & type() {
	static const ISType type_ = Person( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "name: " << name << "\t// Person's name" << std::endl;
	out << "birth_date: " << birth_date << "\t// Person's date of birth" << std::endl;
	out << "sex: " << sex << "\t// Person's sex" << std::endl;
	out << "address: " << address << "\t// Person's home address" << std::endl;
	out << "children[" << children.size() << "]:\t// Person's children" << std::endl;
	for ( size_t i = 0; i < children.size(); ++i )
	    out << i << " : " << children[i] << std::endl;
	return out;
    }

    Person( )
      : ISInfo( "Person" )
    {
	initialize();
    }

    ~Person(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    Person( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << name << birth_date << sex << address << children;
    }

    void refreshGuts( ISistream & in ){
	in >> name >> birth_date >> sex >> address >> children;
    }

private:
    void initialize()
    {
	name = "Jone";
	sex = male;

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const Person & info ) {
    info.print( out );
    return out;
}

#endif // PERSON_H
