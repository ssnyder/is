package is;

class InfoCallback extends CallbackHolder<is.info_callback>
			implements info_callbackOperations
{
    final String	server;

    InfoCallback( String server, Listener listener )
    {
        super(listener);
        this.server = server;
    }
    
    public final void receive( is.info info, is.reason r )
    {    
	info.name = server + '.' + info.name;
	receive(new InfoEvent( info ), r);
    }
}
