package is;

/**
 * The InfoUpdatedListener interface for receiving the actual value of information when the subscriber is attached to it.
 * If an IS subscriber wants to get the actual value of subscribed information when subscription is made,
 * it has to implement this interface.
 * @author Sergei Kolos
 * @see InfoListener
 * @see EventListener
 */

public interface InfoUpdatedListener
{
    /**
     * Invoked when the actual listener attaches to the information.
     * The information itself is not changed.
     * @param e info event
     */
    void infoUpdated( InfoEvent e );
}
