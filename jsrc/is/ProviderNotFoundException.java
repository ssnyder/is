package is;

/**
 * Thrown to indicate that the IS information probider either is not running
 * or is not accessible through the network.
 *
 * @author  Serguei Kolos
 * @version 20/01/01
 * @since   
 */
public class ProviderNotFoundException extends Exception 
{
    /**
     * Constructs an ProviderNotFoundException with the specified
     * detail message.
     *
     * @param name the information name
     * @param address the provider address
     */
    public ProviderNotFoundException( String name, String address ) 
    {
	super( "Provider of the '" + name + 
        	"' information is not found at the '" + address + "' address" );
    }
}
