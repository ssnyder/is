package is;

import java.util.Vector;

/**
 * This class implements a singltone, which can be used to register a user defined CommandListener
 * for the current information provider.
 * @author Sergei Kolos
 * @see InfoListener
 */

public class InfoProvider extends providerPOA
{
    private byte[]			address;
    private Vector<CommandListener>	listeners;
    
    private static final String CORBALOC_PREFIX = "corbaloc:iiop:";
    
    private static class SingletonHolder 
    { 
	public static final InfoProvider INSTANCE = new InfoProvider();
    }
    
    /**
     * Returns the instance of information provider.
     */
    static public InfoProvider instance()
    {
	return SingletonHolder.INSTANCE;
    }
    
    /**
     * Invoked when the command was sent to this provider.
     * @param lst listener object, which have to be added
     * @see CommandListener
     */
    public synchronized void addCommandListener ( CommandListener lst )
    {
    	listeners.add( lst );
    }
    
    /**
     * Invoked when the command was sent to this provider.
     * @param lst listener object, which have to be removed
     * @see CommandListener
     */
    public synchronized void removeCommandListener ( CommandListener lst )
    {
	for ( int i = 0; i < listeners.size(); i++ )
	{
	    if ( listeners.get( i ) == lst )
	    {
		listeners.remove( i );
		break;
	    }
	}
    }
    
    public final synchronized void command ( String name, String command )
    {
	for ( int i = 0; i < listeners.size(); i++ )
	{
	    listeners.get( i ).command( name, command );
	}
	
    }
    
    byte[] getAddress()
    {
    	return address;
    }

    static String makeConnectString( byte[] addr )
    { 
	return new String( CORBALOC_PREFIX + new String( addr ) );
    }
    
    private InfoProvider()
    {
	try
	{
	    String a = ipc.Core.objectToString( _this( ipc.Core.getORB() ), ipc.Core.CORBALOC );
	    a = a.replaceFirst( CORBALOC_PREFIX, "" );
            address = a.getBytes();
	    listeners = new Vector<CommandListener>();
	}
	catch ( Exception e )
	{
            System.err.println( "ERROR in [InfoProvider()] construction failed :" );
	    e.printStackTrace();
	}
    }        
}
