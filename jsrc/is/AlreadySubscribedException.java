package is;

/**
 * Thrown to indicate that the IS information object
 * already exist in the IS repository
 *
 * @author  Serguei Kolos
 * @version 20/01/01
 * @since   
 */
public class AlreadySubscribedException extends Exception 
{
    /**
     * Constructs an AlreadySubscribedException with the specified
     * infromation name.
     *
     * @param name information name
     */
    public AlreadySubscribedException( String name ) 
    {
	super( "Subscription to the '" + name 
        	+ "' information has aleady been done by this application" );
    }
    
    /**
     * Constructs an AlreadySubscribedException with the specified
     * infromation name.
     *
     * @param server_name IS server name
     * @param criteria subscription criteria
     */
    public AlreadySubscribedException( String server_name, Criteria criteria ) 
    {
	super( "Subscription to the '" + server_name + 
        	"' IS server with '" + criteria.toString()
                + "' criteria has aleady been done by this application" );
    }
}
