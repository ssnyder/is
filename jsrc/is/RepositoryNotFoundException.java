package is;

/**
 * Thrown to indicate that the IS repository either is not running
 * or is not accessible through the network.
 *
 * @author  Serguei Kolos
 * @version 20/01/01
 * @since   
 */
public class RepositoryNotFoundException extends Exception 
{
    /**
     * Constructs an RepositoryNotFoundException with the specified
     * detail message.
     *
     * @param partition the TDAQ partition
     * @param name the IS server name
     */
    public RepositoryNotFoundException( ipc.Partition partition, String name ) 
    {
	super( "IS repository '" + name + "' is not found in the '" 
        	+ partition.getName() + "' partition" );
    }
    
    public RepositoryNotFoundException( String reason ) 
    {
	super( reason );
    }
}
