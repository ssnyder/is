package is;

/**
 * Ostream provides interface for murshalling attributes of the user-defined 
 * infromation class to the IS stream.
 * @author Sergei Kolos
 */
public interface Ostream
{
    /**
     * Writes a single boolean value to this stream.
     * @param value the enumeration value to be written
     * @return this stream object
     */
    public <E extends Enum<E>> Ostream put( java.lang.Enum<E> value ); 
       
    /**
     * Writes a single boolean value to this stream.
     * @param value the boolean to be written
     * @return this stream object
     */
    public Ostream put( boolean value ); 
       
    /**
     * Writes a single byte value to this stream.
     * @param value the byte to be written
     * @param sign if true the v will be interpreted as signed byte; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( byte value, boolean sign ); 
       
    /**
     * Writes a single short value to this stream.
     * @param value the short to be written
     * @param sign if true the v will be interpreted as signed short; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( short value, boolean sign );
        
    /**
     * Writes a single integer value to this stream.
     * @param value the int to be written
     * @param sign if true the v will be interpreted as signed int; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( int value, boolean sign );     
       
    /**
     * Writes a single long value to this stream.
     * @param value the int to be written
     * @param sign if true the v will be interpreted as signed int; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( long value, boolean sign );

    /**
     * Writes a single float value to this stream.
     * @param value the float to be written
     * @return this stream object
     */
    public Ostream put( float value );
        
    /**
     * Writes a single double value to this stream.
     * @param value the double to be written
     * @return this stream object
     */
    public Ostream put( double value );
    
    /**
     * Writes a single string value to this stream.
     * @param value the String to be written
     * @return this stream object
     */
    public Ostream put( String value );
    

    /**
     * Writes a single date value to this stream.
     * @param value the Date to be written
     * @return this stream object
     */
    public Ostream put( Date value );   
     
    /**
     * Writes a single time value to this stream.
     * @param value the Time to be written
     * @return this stream object
     */
    public Ostream put( Time value );
    
    /**
     * Writes a single Information object value to this stream.
     * @param value the Information object to be written
     * @param type the IS type of Infomation object
     * @return this stream object
     */
    public Ostream put( Info value, is.Type type );    
    
    /**
     * Writes an array of boolean values to this stream.
     * @param array the array of enumeration values to be written
     * @return this stream object
     */
    public <E extends Enum<E>> Ostream put( java.lang.Enum<E>[] array );
    
    /**
     * Writes an array of boolean values to this stream.
     * @param array the array of booleans to be written
     * @return this stream object
     */
    public Ostream put( boolean[] array );
    
    /**
     * Writes an array of byte values to this stream.
     * @param array the array of bytes to be written
     * @param sign if true any alement of the v will be interpreted as signed byte; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( byte[] array, boolean sign );
	
    /**
     * Writes an array of short values to this stream.
     * @param array the array of shorts to be written
     * @param sign if true any alement of the v will be interpreted as signed short; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( short[] array, boolean sign );
    
    /**
     * Writes an array of int values to this stream.
     * @param array the array of ints to be written
     * @param sign if true any alement of the v will be interpreted as signed int; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( int[] array, boolean sign );
    
    /**
     * Writes an array of long values to this stream.
     * @param array the array of ints to be written
     * @param sign if true any alement of the v will be interpreted as signed int; otherwise as unsigned
     * @return this stream object
     */
    public Ostream put( long[] array, boolean sign );

    /**
     * Writes an array of float values to this stream.
     * @param array the array of floats to be written
     * @return this stream object
     */
    public Ostream put( float[] array );
    
    /**
     * Writes an array of double values to this stream.
     * @param array the array of doubles to be written
     * @return this stream object
     */
    public Ostream put( double[] array );

    /**
     * Writes an array of string values to this stream.
     * @param array the array of Strings to be written
     * @return this stream object
     */
    public Ostream put( String[] array );
    
    /**
     * Writes an array of date values to this stream.
     * @param array the array of Dates to be written
     * @return this stream object
     */
    public Ostream put( Date[] array );
    
    /**
     * Writes an array of time values to this stream.
     * @param array the array of Times to be written
     * @return this stream object
     */
    public Ostream put( Time[] array );
    
    /**
     * Writes an array of Infomation objects to this stream.
     * @param array the array of Infomation objects to be written
     * @param type the IS type of Infomation objects
     * @return this stream object
     */
    public <T extends Info> Ostream put( T[] array, is.Type type );
    
    /**
     * Writes a vector of objects to this stream.
     * @param vector the vector of objects to be written
     * @param type the IS type of Infomation objects
     * @return this stream object
     */
    public <T extends Info> Ostream put( java.util.Vector<T> vector, is.Type type );
}
