package is;

import java.util.Hashtable;

import ipc.Partition;
import rdb.*;
import java.util.Date;

/**
 * This class should be used to access meta-information for the IS information types: 
 * name and description for the type itself, and also names, types, and descriptions for
 * all the type attributes.
 * @author Serguei Kolos
 */
public class InfoDocument
{
    private static final String				dbName = "ISRepository";
    private static Hashtable<String, InfoDocument>	types = new Hashtable<String, InfoDocument>();
    private static Time					tableTime;
    static final Hashtable<String,Byte>			typecodes;    
    
    static 
    {
	typecodes = new Hashtable<String,Byte>();
	typecodes.put( "bool",	Type.BOOLEAN );
	typecodes.put( "s8",	Type.S8 );
	typecodes.put( "s16",	Type.S16 );
	typecodes.put( "s32",	Type.S32 );
	typecodes.put( "s64",	Type.S64 );
	typecodes.put( "u8",	Type.U8 );
	typecodes.put( "u16",	Type.U16 );
	typecodes.put( "u32",	Type.U32 );
	typecodes.put( "u64",	Type.U64 );
	typecodes.put( "float",	Type.FLOAT );
	typecodes.put( "double",Type.DOUBLE );
	typecodes.put( "string",Type.STRING );
	typecodes.put( "time",	Type.TIME );
	typecodes.put( "date",	Type.DATE );
	typecodes.put( "enum",	Type.S32 );
    }

    private String	name;
    private String	description;
    private Attribute[]	attributes;

    public class Attribute
    {
        String			name;
        String			description;
        String			type;
        String			range;
        boolean			isArray;
        rdb.IntegerFormat	format = rdb.IntegerFormat.int_format_na;
        
        /**
         * Gets name of the attribute.
         * @return attribute name
         */
        public String getName()
        {
            return name;
        }
        
        /**
         * Gets type of the attribute. Possible types are:
         * <table BORDER CELLSPACING=3 CELLPADDING=3 NOSAVE >
         * <tr NOSAVE><th NOSAVE>type name</th><th NOSAVE>description</th><th NOSAVE>Java type</th></tr>
         * <tr><td>"bool"</td><td>boolean value</td><td>boolean</td></tr>
         * <tr><td>"s8"</td><td>signed 8-bit value</td><td>byte</td></tr>
         * <tr><td>"s16"</td><td>signed 16-bit value</td><td>short</td></tr>
         * <tr><td>"s32"</td><td>signed 32-bit value</td><td>int</td></tr>
	 * <tr><td>"s64"</td><td>signed 64-bit value</td><td>int</td></tr>
         * <tr><td>"u8"</td><td>unsigned 8-bit value</td><td>byte</td></tr>
         * <tr><td>"u16"</td><td>unsigned 16-bit value</td><td>short</td></tr>
         * <tr><td>"u32"</td><td>unsigned 32-bit value</td><td>int</td></tr>
	 * <tr><td>"u64"</td><td>unsigned 64-bit value</td><td>int</td></tr>
         * <tr><td>"float"</td><td>float value</td><td>float</td></tr>
         * <tr><td>"double"</td><td>double precision value</td><td>double</td></tr>
         * <tr><td>"string"</td><td>character string</td><td>java.lang.String</td></tr>
         * <tr><td>"date"</td><td>calendar date</td><td>is.Date</td></tr>
         * <tr><td>"time"</td><td>instant in time</td><td>is.Time</td></tr>
         * </table>
         * @return Attribute type name
         */
        public String getType()
        {
            return type;
        }
        
        public byte getTypeCode()
        {
            Byte b = typecodes.get(type);
            if ( b == null )
            	return Type.INFO;
            else
            	return b;
        }
        
        /**
         * Gets range of the possible values of this attribute.
         * @return attribute values range
         */
        public String getRange()
        {
            return range;
        }
        
        /**
         * Gets description of the attribute.
         * @return attribute description
         */
        public String getDescription()
        {
            return description;
        }
        
        /**
         * Gets format of the attribute of type Integer.
         * possible values of the rdb.InetegerFormat enumeration type:
         * <ul> 
         * <li>int_format_oct
         * <li>int_format_dec
         * <li>int_format_hex
         * <li>int_format_na
         * </ul> 
         * @return attribute format
         */
        public rdb.IntegerFormat getFormat()
        {
            return format;
        }
        
        /**
         * Determines if this attribute is an array.
         * @return true if the attribute is an array; false otherwise
         */
        public boolean isArray()
        {
            return isArray;
        }
        
        private Attribute( String name, String type, String range, 
        		   String description, boolean isArray, rdb.IntegerFormat format )
        {
            this.name = name;
            this.type = type;
            this.range = range;
            this.description = description;
            this.isArray = isArray;
	    this.format = format;
       }        
       
       private Attribute( String name, String type, String range, 
        		   String description, boolean isArray )
       {
            this.name = name;
            this.type = type;
            this.range = range;
            this.description = description;
            this.isArray = isArray;
	    this.format = rdb.IntegerFormat.int_format_na;
       }        
    }
    
    /**
     * Creates the document object that contains description for the <tt>info</tt> object type.
     * @param partition current partition
     * @param info the information object to get description for
     * @exception is.UnknownTypeException if description is not available
     */
    public InfoDocument( Partition partition, Info info ) throws UnknownTypeException                    
    {
        this( partition, info.getType() );
    }
    
    /**
     * Creates the document object that contains description for the <tt>type</tt> type.
     * @param partition current partition
     * @param type the information object type to get description for
     * @exception is.UnknownTypeException if description is not available
     */
    public InfoDocument( Partition partition, Type type ) throws UnknownTypeException                    
    {
        name = type.getName();
        if ( name.equals( Type.UnknownName ) )
        {
            //
            // There is no type information available
            //
            throw new UnknownTypeException( "Infomation object has no type name" );
        }

        InfoDocument idoc = types.get( name );
        if ( idoc != null )
        {
            //
            // Type is already in the hashtable
            //
            description = idoc.description;
            attributes = idoc.attributes;
            return;
        }
                
	try
	{
	    rdb.cursor db = partition.lookup( rdb.cursor.class, dbName );
	    RDBClassHolder cl = new RDBClassHolder();
	    db.get_class( name, cl );
	    initialize( db, cl.value );
	}
	catch ( Exception ex )
	{
	    throw new UnknownTypeException( ex );
	}
    }

    private InfoDocument( rdb.cursor db, RDBClass cl ) throws UnknownTypeException
    {
    	initialize( db, cl );
    }

    private void initialize( rdb.cursor db, RDBClass cl ) throws UnknownTypeException
    {        
        name = cl.name;
        
	try
	{
	    description = cl.description;
	    attributes = new Attribute[ cl.attributeNumber + cl.relationshipNumber ];

	    RDBClassListHolder clist = new RDBClassListHolder();
	    db.get_all_super_classes( name, clist );

	    RDBClass[] tmpl = clist.value;
	    clist.value = new RDBClass[tmpl.length + 1];
	    for ( int k = 0; k < tmpl.length; k++ )
	    {
		clist.value[k] = tmpl[k];
	    }
	    clist.value[clist.value.length - 1] = cl;

	    int num = 0;
	    for ( int k = 0; k < clist.value.length; k++ )
	    {
		if ( "Info".equals( clist.value[k].name ) )
		    continue;

		RDBAttributeListHolder al = new RDBAttributeListHolder();
		db.get_attributes( clist.value[k].name, al );
		for ( int i = 0; i < al.value.length; i++ )
		{
		    if ( al.value[i].isDirect )
		    {
                        attributes[num++] = new Attribute ( al.value[i].name,
							    al.value[i].type,
							    al.value[i].range,
							    al.value[i].description,
							    al.value[i].isMultiValue,
							    al.value[i].intFormat
						    );
		     }
		}

		RDBRelationshipListHolder rl = new RDBRelationshipListHolder();
		db.get_relationships( clist.value[k].name, rl );
		for ( int i = 0; i < rl.value.length; i++ )
		{
		    if ( rl.value[i].isDirect )
		    {
			attributes[num++] = new Attribute ( rl.value[i].name,
							    rl.value[i].classid,
							    "",
							    rl.value[i].description,
							    rl.value[i].isMultiValue
						    );
						    }
		}
	    }
	    types.put( name, this );
	}
	catch ( Exception ex )
	{
	    throw new UnknownTypeException( ex );
	}
    }
    
    /**
     * Gets name of the information type.
     * @return information type name
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * Gets information type description.
     * @return information type description
     */
    public String getDescription()
    {
        return description;
    }
    
    /**
     * Gets number of attributes for the information type.
     * @return the attributes number
     */
    public int getAttributeCount()
    {
        return ( attributes == null ? 0 : attributes.length );
    }
    
    /**
     * Gets description for the attribute at the specified position.
     * @param i attribute position
     * @return attribute description
     */
    public Attribute getAttribute( int i )
    {
        return ( attributes[i] );
    }
    
    /**
     * Gets all the IS documents for the specified partition.
     * @param p current partition
     * @return enumeration of the InfoDocument objects
     */
    public static java.util.Enumeration<InfoDocument> allDocuments( Partition p )
    {
        initTable( p );
        return types.elements();
    }

    private static boolean upToDate( rdb.cursor db )
    {
	return false;
    }    
    
    private static void initTable( Partition p )
    {
        
        rdb.cursor db = null;
                        
	try
	{
	    db = p.lookup( rdb.cursor.class, dbName );

	    if ( upToDate( db ) )
		return;

	    rdb.RDBClassListHolder  list = new rdb.RDBClassListHolder();
	    db.get_all_classes( list );

	    types.clear();
	    for ( int i = 0; i < list.value.length; i++ )
	    {
		InfoDocument id = new InfoDocument( db, list.value[i] );
		types.put( list.value[i].name, id );
	    }
	}
	catch( Exception ex )
	{
	    return;
	}
        
        tableTime = new Time();        
    }    
}
