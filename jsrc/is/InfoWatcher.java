package is;

import ipc.Partition;

/**
 * An abstract adapter class for receiving callbacks from the IS repository. This class provides the same functionality 
 * as is.Receiver plus an additional callback function which is invoked with the actual information object value once,
 * when subscription is made. 
 * @author Sergei Kolos
 * @see InfoSubscriberListener
 * @see Receiver
 */
public abstract class InfoWatcher extends Receiver
{
    /**
     * Creates InfoEvent listener for the <tt>server_name</tt> IS server in the
     * <tt>partition</tt> partition. Subscriber will be notified about changes
     * of the information objects which satisfy the given <tt>criteria</tt>.
     * 
     * @param partition
     *            partition which IS server belongs to
     * @param server_name
     *            IS server name to listen information events on
     * @param criteria
     *            notification will be done only for information objects, which
     *            match thios criteria
     * @param receive_value
     *            If set to true, then subscriber will always receive the value
     *            of the changed information If false, then the value will be
     *            read aftwerwards only if necessary.
     * @see Criteria
     */
    protected InfoWatcher(Partition partition, String server_name, Criteria criteria)
    {
	super( partition, server_name, criteria );
    }
    
    /**
     * Creates InfoEvent listener for the <tt>server_name</tt> IS server in the <tt>partition</tt> partition.
     * Subscriber will be notified about changes of the information objects whose names match the <tt>pattern</tt> 
     * regular expression. This constructor creates callback, which will receive information value from the IS repository.
     * @param partition partition which IS server belongs to
     * @param server_name IS server name to subscribe
     * @param pattern regular expression, that is used for subscription. 
     * @deprecated Exists for compatibility with the previous IS versions.
     */
    public InfoWatcher(	Partition partition, String server_name, java.util.regex.Pattern pattern ) 
    {
        super( partition, server_name, pattern );
    }
    
    /**
     * Creates InfoEvent listener for the <tt>server_name</tt> IS server in the <tt>partition</tt> partition.
     * Subscriber will be notified about changes of the information objects whose names match the <tt>pattern</tt> 
     * regular expression.
     * @param partition partition which IS server belongs to
     * @param name name of the information object to subscribe to
     * @param receive_value If set to true, then subscriber will always receive the value of the changed information
     *				If false, then the value will be read aftwerwards only if necessary.
     */
    public InfoWatcher(	Partition partition, String name ) 
    {
        super( partition, name );
    }
    
    public static interface ExtendedInfoListener extends InfoListener, InfoSubscribedListener {}
    public static interface ExtendedEventListener extends EventListener, InfoSubscribedListener {}
    
    Listener createListener(boolean receive_value)
    {
        if (receive_value) {
	    return new ExtendedInfoListener() {
		    public void infoCreated(InfoEvent e) {
			InfoWatcher.this.infoCreated(e);
		    }
		    public void infoUpdated(InfoEvent e) {
			InfoWatcher.this.infoUpdated(e);
		    }
		    public void infoDeleted(InfoEvent e) {
			InfoWatcher.this.infoDeleted(e);
		    }
		    public void infoSubscribed(InfoEvent e) {
			InfoWatcher.this.infoSubscribed(e);
		    }
	    };
	}
	else {
	    return new ExtendedEventListener() {
		    public void infoCreated(InfoEvent e) {
			InfoWatcher.this.infoCreated(e);
		    }
		    public void infoUpdated(InfoEvent e) {
			InfoWatcher.this.infoUpdated(e);
		    }
		    public void infoDeleted(InfoEvent e) {
			InfoWatcher.this.infoDeleted(e);
		    }
		    public void infoSubscribed(InfoEvent e) {
			InfoWatcher.this.infoSubscribed(e);
		    }
	    };
	}
    }
    
    /**
     * Invoked when this subscriber establishes subscription to the given object.
     * 
     * @param e	Contains information about the event which caused this callback
     */
    public void infoSubscribed(InfoEvent e) 
    {
    }
}
