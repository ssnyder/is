package is;

/**
 * A time class which allows to read time from IS repository with microseconds precision.
 * @author Sergei Kolos
 * @see Info
 */
public class Time extends is.Date
{
    private static final java.text.SimpleDateFormat time_formatter
    	= new java.text.SimpleDateFormat( "yyyy-MMM-dd HH:mm:ss", java.util.Locale.US );

    private static final java.text.SimpleDateFormat time_formatter_backup
    	= new java.text.SimpleDateFormat( "yyyy-MM-dd HH:mm:ss", java.util.Locale.US );

    /**
     * Constructs object and initializes it so that it represents the time at which it was constructed 
     */
    public Time( )
    {
	super();
    }
    
    /**
     * Constructs object and initializes it so that it represents the time given by the input string 
     * The time string must be using the ISO format: "yyyy-MMM-dd [HH:mm:ss[.ffffff]]"
     */
    public Time( String time )
    {
	String[] tokens = time.split( "\\." );
        
	try {
	    synchronized (time_formatter) {
		super.setTime( time_formatter.parse( tokens.length != 0 ? tokens[0] : time ).getTime() );
	    }
	}
	catch( java.text.ParseException e ) {
	    try {
		synchronized (time_formatter_backup) {
		    super.setTime( time_formatter_backup.parse( tokens.length != 0 ? tokens[0] : time ).getTime() );
		}
            }
            catch( java.text.ParseException ex ) { }
	}
        
	microseconds = super.getTime() * 1000l;
	if ( tokens.length == 2 )
	{
	    microseconds += Long.parseLong( tokens[1] );
	}
    }
    
    /**
     * Constructs object from the number of microseconds since January 1, 1970, 00:00:00 GMT.
     */
    public Time( long microseconds )
    {
    	super( microseconds );
    }

    /**
     * Returns the string representation of this object.
     * The string will be given in ISO format: "yyyy-MMM-dd [HH:mm:ss[.ffffff]]"
     */
    public String toString()
    {
    	synchronized (time_formatter) {
    	    return ( time_formatter.format( this ) + "." + String.valueOf( microseconds % 1000000l ) );
    	}
    }
}
