package is;

/**
 * This interface must be implemented by all the IS information classes. 
 * The IS run-time uses this interface for marshaling and unmarshaling. 
 * @author Sergei Kolos
 */
public interface Streamable
{
    /**
     * This method is used to write the state of the information to the stream.
     * Called by the IS run-time on the information object when the IS repository 
     * is requested to be synchronized with the information state. This method 
     * is called for example by the Repository.insert and Repository.update methods.
     * @param out stream to write information state to
     * @see Repository
     */
    void    publishGuts( is.Ostream out );

    /**
     * This method is used to read from the stream and restore the IS information attributes.
     * Called by the IS run-time on the information object when the content of the object 
     * is requested to be synchronized with the IS repository. This method 
     * is called for example by the Repository.getValue method.
     * @param in stream to read information state from
     * @see Repository
     */
    void    refreshGuts( is.Istream in );    
}
