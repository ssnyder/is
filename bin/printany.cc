#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <memory>

#include "is/infoany.h"
#include "is/infodocument.h"

const size_t min_max_name_width = 13;

void print_info ( const IPCPartition & pp, ISInfoAny & isa, const ISType & type, std::ostream & out, 
		  bool p_name, bool p_type, bool p_description, const char * offset = "      " )
{        
    size_t count = type.entries();
    size_t max_name_width = min_max_name_width;
    
    std::unique_ptr<ISInfoDocument> isd;
    try {
    	if ( p_name || p_description ) {
            isd = std::unique_ptr<ISInfoDocument>( new ISInfoDocument( pp, type ) );
        }
    
	if ( p_name )
	{
	    for ( size_t i = 0; i < isd->attributeCount(); i++ )
	    {
		max_name_width = std::max( isd->attribute( i ).name().length(), max_name_width );
	    }
	}
    }
    catch( daq::is::Exception & ex ) {
    	// Ignore this exception
    }
    
    out << offset << count << " attribute(s):" << std::endl ;
    for ( size_t i = 0; i < count; i++ )
    {
	out << offset;
	if ( p_name && isd.get() )
	{
	    out.width( max_name_width + 5 );
	    out.setf( std::ios::left, std::ios::adjustfield );
      	    out << isd->attribute( i ).name();
	}
	    	    
        ISType::Basic btype = type.entryType(i);
	std::ostringstream value;
        
        // Setup more appropriate floating point representation
	value.unsetf( std::ios::showpoint );
        value << std::setprecision( btype == ISType::Float ? 7 : btype == ISType::Double ? 14 : 6 );
	
	size_t size = btype == ISType::InfoObject ? type.entryArray(i)  ? isa.readArraySize() 
									: 1
                                		  : is::print_attribute_value( value, isa, type, i );

	if ( isd.get() && btype != isd->attribute( i ).typeCode() )
	{
	    std::cerr	<< "ERROR:: Actual type of the above attribute is not the same as the declared one : " 
			<< isd->attribute( i ).typeName() << ( isd->attribute( i ).isArray() ? "[]" : "" ) << std::endl;
	    std::cerr	<< "HINT: Check the XML declaration for the \"" << isd->name() << "\" IS type." << std::endl;
	}
	
	if ( p_type )
	{
	    out.width( ISType::max_type_name_width );
	    out.setf( std::ios::left, std::ios::adjustfield );
      	    is::print_attribute_type( out, type, i, size );
	}
	
	if ( p_description && isd.get() )
	{
	    out << "/* " << isd->attribute( i ).description() << " */ ";
	}
	
        if ( btype == ISType::InfoObject )
        {
	    if ( p_type || p_name || p_description )
	    {
            	out << std::endl;
	    }
            ISType atype = type.entryInfoType(i);
            for ( size_t j = 0; j < size; j++ )
            {
		print_info( pp, isa, atype, out, p_name, p_type, p_description, "          " );
            }
        }
	else
	{
	    out << value.str() << std::endl;
	}
    }
}
