////////////////////////////////////////////////////////////////////////
//      isls.cc
//
//      IS binary
//
//      Sergei Kolos,  January 2000
//
//      description:
//           Lists Information Service servers in the specific partition
//		as well as the contents of the servers   
//		
////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cmdl/cmdargs.h>

#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/partition.h>

#include <is/infodictionary.h>
#include <is/infoiterator.h>
#include <is/serveriterator.h>


int main(int argc, char ** argv)
{
    try {
	std::list< std::pair< std::string, std::string > > options = IPCCore::extractOptions( argc, argv );
	options.push_front( std::make_pair( std::string("clientCallTimeOutPeriod"), std::string("60000") ) );
        IPCCore::init( options );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::fatal( ex );
        return 1;
    }
  
    CmdArgStr	partition_name ('p', "partition", "partition-name", "partition to work in.");
    CmdArgStr	server_name ('n', "server", "server-name", "server to work with (regular expressions allowed).", CmdArg::isREQ );
    CmdArgStr	regexp ('r', "rexpression", "regular-expression", "names of information to be removed.", CmdArg::isREQ);
    CmdArgBool	verbose ('v', "verbose", "produce verbose output.");
 
    // Declare command object and its argument-iterator
    CmdLine  cmd(*argv, &partition_name, &server_name, &regexp, &verbose, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);
 
    cmd.description( "Remove information from Information Service servers in the specific partition" );
    
    // Parse arguments
    cmd.parse( arg_iter );            
  
    IPCPartition  partition( partition_name );            

    try
    {
	ISServerIterator sit( partition, (const char*)server_name );
	if ( verbose )
	{
	    std::cout  << "Partition \"" << partition.name()
		       << "\" contains " << sit.entries()
		       << " IS server(s):" << std::endl;
	}

	while ( sit() )
	{
	    try {
		ISInfoDictionary d( partition );
		d.removeAll( sit.name(), ISCriteria(std::string(regexp)) );
            }
            catch( daq::is::Exception & ex ) {
		ers::error( ex );
		return 1;
            }
	}
    }
    catch ( ers::Issue & ex )
    {
    	ers::fatal( ex );
        return 1;
    }
    
    return 0; 
}
