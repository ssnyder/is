#include <is/callbackholder.h>
#include <is/inforeceiver.h>
#include <is/server.h>    
#include <is/exceptions.h>

#include <numeric>

struct ISSimpleCallback: public ISCallbackHolder
{
    ISSimpleCallback(const IPCPartition & p,
	const std::string & server,
	const std::string & object,
	IPCServantBase<POA_is::callback> * c,
	int mask)
	: ISCallbackHolder(p, server, c, mask),
	  m_object_name(object)
    {
	;
    }

    void subscribe()
    {
	try {
	    is::repository_var r = is::server::resolve(m_partition , m_server_name);
	    r->add_callback(m_object_name.c_str(), m_callback->_this(), m_mask);
	    m_subscription_time = std::chrono::system_clock::now();
	}
	catch( is::InvalidCallback & ) {
            throw daq::is::InvalidSubscriber( ERS_HERE, m_server_name );
        }
        catch( CORBA::SystemException & ex ) {
            throw daq::is::RepositoryNotFound( ERS_HERE, m_server_name, daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
        }
    }

    void unsubscribe()
    {
	IS_INVOKE(remove_callback(m_object_name.c_str(), m_callback->_this()),
	    m_partition, m_server_name, m_object_name)
    }

private:
    std::string m_object_name;
};

struct ISCriteriaCallback: public ISCallbackHolder
{
    ISCriteriaCallback(const IPCPartition & p,
	const std::string & server,
	const ISCriteria & criteria,
	IPCServantBase<POA_is::callback> * c,
	int mask)
	: ISCallbackHolder(p, server, c, mask),
	  m_criteria(criteria)
    {
	;
    }

    void subscribe()
    {
        try {
            is::repository_var r = is::server::resolve(m_partition , m_server_name);
            r->subscribe(m_criteria, m_callback->_this(), m_mask);
            m_subscription_time = std::chrono::system_clock::now();
        }
        catch( is::InvalidCriteria & ex ) {
            throw daq::is::InvalidCriteria( ERS_HERE, m_criteria.toString() );
        }
        catch( is::InvalidCallback & ) {
            throw daq::is::InvalidSubscriber( ERS_HERE, m_server_name );
        }
        catch( CORBA::SystemException & ex ) {
            throw daq::is::RepositoryNotFound( ERS_HERE, m_server_name, daq::ipc::CorbaSystemException( ERS_HERE, &ex ) );
        }
    }

    void unsubscribe()
    {
	IS_INVOKE(unsubscribe(m_criteria, m_callback->_this()),
	    m_partition, m_server_name, m_criteria.toString())
    }

private:
    ISCriteria m_criteria;
};

ISInfoReceiver::~ISInfoReceiver()
{
    CallbackMap::iterator it = m_callbacks.begin();
    for (; it != m_callbacks.end(); ++it)
    {
	try
	{
	    it->second->unsubscribe();
	}
	catch (daq::is::Exception & ex)
	{
	    ers::debug(ex, 1);
	}
    }
}

void
ISInfoReceiver::subscribe_impl(const std::string & key,
    const std::shared_ptr<ISCallbackHolder> & callback,
    bool no_exception)
{
    {
	boost::mutex::scoped_lock lock(m_mutex);
	if (m_callbacks.find(key) != m_callbacks.end())
	    throw daq::is::AlreadySubscribed(ERS_HERE, key);

	m_callbacks[key] = callback;
    }

    try {
	callback->subscribe();
    }
    catch(daq::is::RepositoryNotFound & ex) {
	if (no_exception)
	    return;

	boost::mutex::scoped_lock lock(m_mutex);

	CallbackMap::iterator it = m_callbacks.find(key);
	if (it != m_callbacks.end() && it->second == callback) {
	    m_callbacks.erase(it);
	}
	throw;
    }
    catch(daq::is::Exception & ex) {
	boost::mutex::scoped_lock lock(m_mutex);

	CallbackMap::iterator it = m_callbacks.find(key);
	if (it != m_callbacks.end() && it->second == callback) {
	    m_callbacks.erase(it);
	}
	throw;
    }
}

void
ISInfoReceiver::subscribe_impl_wrapper(const std::string & name,
    IPCServantBase<POA_is::callback> * cb,
    const std::vector<ISInfo::Reason>& event_mask,
    bool no_exception)
{
    std::string sid, oid;
    is::server::split_name(name, sid, oid);
    int mask = std::accumulate(event_mask.begin(), event_mask.end(),
	    0, [](int x, ISInfo::Reason y) { return x | (1 << y); });

    std::shared_ptr<ISCallbackHolder> callback(new ISSimpleCallback(m_partition, sid, oid, cb, mask));
    subscribe_impl(name, callback, no_exception);
}

void
ISInfoReceiver::subscribe_impl_wrapper(const std::string & server_name,
    const ISCriteria & criteria,
    IPCServantBase<POA_is::callback> * cb,
    const std::vector<ISInfo::Reason>& event_mask,
    bool no_exception)
{
    int mask = std::accumulate(event_mask.begin(), event_mask.end(),
	    0, [](int x, ISInfo::Reason y) { return x | (1 << y); });

    std::shared_ptr<ISCallbackHolder> callback(new ISCriteriaCallback(m_partition, server_name, criteria, cb, mask));
    subscribe_impl(server_name + "." + criteria.toString(), callback, no_exception);
}

void
ISInfoReceiver::unsubscribe(const std::string & name, bool wait_for_completion)
{
    std::shared_ptr<ISCallbackHolder> c;
    {
	boost::mutex::scoped_lock lock(m_mutex);
	CallbackMap::iterator it = m_callbacks.find(name);

	if (it == m_callbacks.end())
	    throw daq::is::SubscriptionNotFound(ERS_HERE, name);

	c = it->second;
	m_callbacks.erase(it);
    }
    c->wait() = wait_for_completion;
    c->unsubscribe();
}

void
ISInfoReceiver::unsubscribe(const std::string & server_name,
    const ISCriteria & criteria,
    bool wait_for_completion)
{
    const std::string subscription = criteria.toString();
    const std::string key = server_name + "." + subscription;
    unsubscribe(key, wait_for_completion);
}
