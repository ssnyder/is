/*
 * callbackmanager.cc
 *
 *  Created on: May 30, 2013
 *      Author: Serguei Kolos
 */
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/member.hpp>

#include <ipc/applicationcontext.h>
#include <is/callbackholder.h>
#include <is/callbackmanager.h>
#include <is/server.h>

using namespace boost::multi_index;

namespace
{
    struct PartitionHasher
    {
      int operator()(const IPCPartition & x)const
      {
	  return boost::hash_value(x.name());
      }
    };

    typedef boost::multi_index_container<
	ISCallbackManager*,
        indexed_by<
	    hashed_unique<
		composite_key<
		    ISCallbackManager*,
		    member<ISCallbackManager, const IPCPartition, &ISCallbackManager::m_partition>,
		    member<ISCallbackManager, const std::string, &ISCallbackManager::m_server_name>
		>,
		composite_key_hash<
		    PartitionHasher,
		    boost::hash<std::string>
		>
    >>> Set;
}

ISCallbackManager::ISCallbackManager(const IPCPartition & p, const std::string & server_name)
    : m_partition(p),
      m_server_name(server_name),
      m_checkup_period(10),
      m_alarm(m_checkup_period, std::bind(&ISCallbackManager::periodicAction, this))
{ ; }

ISCallbackManager&
ISCallbackManager::instance(const IPCPartition& p, const std::string& server_name)
{
    static Set * set = new Set;
    static std::mutex s_mutex;

    std::lock_guard<std::mutex> lock(s_mutex);
    Set::iterator it = set->find(boost::make_tuple(p, server_name));
    if ( it == set->end() ) {
	it = set->insert(new ISCallbackManager(p, server_name)).first;
    }
    return **it;
}

void ISCallbackManager::addCallback(ISCallbackHolder * c)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_callbacks.insert(c);
}

void ISCallbackManager::removeCallback(ISCallbackHolder * c)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    CallbackSet::const_iterator it = m_callbacks.find(c);
    if (it != m_callbacks.end())
	m_callbacks.erase(it);
}

bool ISCallbackManager::periodicAction()
{
    if (m_callbacks.empty()) {
	return true;
    }

    try {
	IPCApplicationContext acontext(is::server::resolve(m_partition, m_server_name));
	if (!acontext.m_valid) {
	    return true;
	}

        std::lock_guard<std::mutex> lock(m_mutex);
        CallbackSet::iterator it = m_callbacks.begin();
        while (it != m_callbacks.end())
        {
            try {
                (*it)->resubscribe(acontext.m_time);
                ++it;
            }
            catch(daq::is::InvalidSubscriber & ex) {
                it = m_callbacks.erase(it);
                continue;
            }
            catch(daq::is::Exception & ex) {
                return true;
            }
        }
    }
    catch (daq::is::Exception & ex) {
        return true;
    }

    return true;
}
