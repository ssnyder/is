#include <is/serveriterator.h>

#include <ipc/partition.h>    
#include <owl/regexp.h>    

ISServerIterator::ISServerIterator( const IPCPartition & p ) 
  : m_partition( p )
{
    std::map< std::string, is::repository_var > objects;
    m_partition.getObjects<is::repository, ipc::use_cache, ipc::narrow>( objects );
    
    std::map< std::string, is::repository_var >::iterator it = objects.begin();
    for ( ; it != objects.end(); it++ )
    {
	try {
            ipc::servant::ApplicationContext_var info = it->second -> app_context();
            m_list.push_back( std::make_pair( it->first, info ) );
	}
	catch(...) { }
    }
    m_index = -1;
    m_length = m_list.size();
}

ISServerIterator::ISServerIterator( const IPCPartition & p, const std::string & criteria ) 
  : m_partition( p )
{
    OWLRegexp regexp( criteria );
    if ( regexp.status() != OWLRegexp::Ok )
    {
    	throw daq::is::InvalidCriteria( ERS_HERE, criteria );
    }
    
    std::map< std::string, is::repository_var > objects;
    m_partition.getObjects<is::repository, ipc::use_cache, ipc::narrow>( objects );
    
    std::map< std::string, is::repository_var >::iterator it = objects.begin();
    for ( ; it != objects.end(); it++ )
    {
	if ( regexp.exact_match( it->first ) )
        {
	    try {
		ipc::servant::ApplicationContext_var info = it->second -> app_context();
		m_list.push_back( std::make_pair( it->first, info ) );
	    }
	    catch(...) { }
        }
    }
    m_index = -1;
    m_length = m_list.size();
}

int
ISServerIterator::entries() const
{
    return m_length;
}

const char *
ISServerIterator::name() const
{
    return m_list[m_index].first.c_str();
}

const char *
ISServerIterator::owner() const
{
    return m_list[m_index].second->owner;
}

const char *
ISServerIterator::host() const
{
    return m_list[m_index].second->host;
}

int
ISServerIterator::pid() const
{
    return m_list[m_index].second->pid;
}

OWLTime
ISServerIterator::time() const
{
    return OWLTime( m_list[m_index].second->time );
}

void
ISServerIterator::reset()
{
    m_index = -1;
}

bool
ISServerIterator::operator()()
{
    return ( ++m_index < m_length );
}
 
bool
ISServerIterator::operator++()
{
    return operator()();
}
 
bool
ISServerIterator::operator++( int )
{
    return operator()();
}
 
bool
ISServerIterator::operator--()
{
    return ( --m_index >= 0 && m_index < m_length );
}

bool
ISServerIterator::operator--( int )
{
    return operator--();
}
