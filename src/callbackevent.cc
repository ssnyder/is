#include <is/callbackevent.h>
    
ISCallbackEvent::ISCallbackEvent(   const IPCPartition & partition,
				    const std::string & server_name,
                                    void * user_param,
				    is::reason reason,
				    const is::event & event )
  : m_partition( partition ),
    m_server_name( server_name ),
    m_name( server_name + '.' + (const char *)event.name ),
    m_reason( reason ),
    m_event( event ),
    m_user_param( user_param )
{ ; }
