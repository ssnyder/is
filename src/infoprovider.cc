#include <stdlib.h>

#include <algorithm>

#include <boost/lexical_cast.hpp>

#include <ipc/core.h>

#include <is/infoprovider.h>

boost::mutex	 ISInfoProvider::m_guard;
ISInfoProvider * ISInfoProvider::m_provider;
std::string	 ISInfoProvider::m_corbaloc_prefix = "corbaloc:iiop:";

class lst_invoke
{
    std::string m_name;
    std::string m_command;
    
  public:
    lst_invoke( const char * name, const char * command )
      : m_name( name ),
        m_command ( command )
    { ; }
    
    void operator()( ISCommandListener * lst ) const
    {
	lst->command( m_name, m_command );
    }
};

ISInfoProvider::ISInfoProvider()
{
    try {
	is::provider_var self = _this();
	std::string address = IPCCore::objectToString( self, IPCCore::Corbaloc );
	if ( address.find( m_corbaloc_prefix ) == 0 )
	    address.erase( 0, m_corbaloc_prefix.size() );
	m_address.length( address.size() );
	memcpy( m_address.get_buffer(), address.c_str(), address.size() );
    }
    catch( daq::ipc::Exception & ex ) {
    	ers::warning( ex );
    }
}

ISInfoProvider & 
ISInfoProvider::instance()
{
    if ( !m_provider )
    {
	boost::mutex::scoped_lock ml( m_guard );
	if ( !m_provider )
	{
	    m_provider = new ISInfoProvider();
	    std::atexit( atExitFun );
	}
    }
    return *m_provider;
}

std::string 
ISInfoProvider::unique_id() const
{
    return std::string( ::getenv("TDAQ_APPLICATION_NAME") 
					? std::string(::getenv("TDAQ_APPLICATION_NAME"))
					: boost::lexical_cast<std::string>(::getpid()));
}
    
void 
ISInfoProvider::addCommandListener( ISCommandListener * lst )
{
    boost::recursive_mutex::scoped_lock ml( m_mutex );
    m_listeners.push_back( lst );
}

void 
ISInfoProvider::removeCommandListener( ISCommandListener * lst )
{
    boost::recursive_mutex::scoped_lock ml( m_mutex );
    m_listeners.remove( lst );
}

void 
ISInfoProvider::command ( const char * name, const char * command )
{
    boost::recursive_mutex::scoped_lock ml( m_mutex );
    std::for_each( m_listeners.begin(), m_listeners.end(), lst_invoke( name, command ) );
}

void 
ISInfoProvider::atExitFun()
{
    boost::mutex::scoped_lock ml( m_guard );
    if ( m_provider )
    {
	m_provider->_destroy();
	m_provider = 0;
    }
}



