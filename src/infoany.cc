#include <sstream>

#include <boost/type_traits/integral_promotion.hpp>
#include <is/infoany.h>

namespace is
{
    template <typename T>
    struct attribute 
    {
	static size_t
	print( std::ostream & out, ISInfoAny & isa, const ISType & type, size_t entry, int how_many = -1 )
	{
	    if ( type.entryArray( entry ) )
	    {
		std::vector<T> value;
		isa >> value;

		size_t printed_size = how_many > -1 && (int)value.size() > how_many ? how_many : value.size();

		if ( printed_size > 0 )
		{
		    for ( size_t i = 0; i < printed_size - 1; i++ )
			out << static_cast<typename boost::integral_promotion<T>::type>( value[i] ) << ", ";
		    out << static_cast<typename boost::integral_promotion<T>::type>( value[printed_size - 1] );
		    if ( printed_size != value.size() )
			out << " {... " << value.size() - printed_size << " more values}";
		}
		return value.size();
	    }
	    else
	    {
		T value;
		isa >> value;
		out << static_cast<typename boost::integral_promotion<T>::type>( value );
		return 1;
	    }
	}
    };
     
    template <>
    struct attribute<ISInfoAny> 
    {
	static size_t
	print( std::ostream & out, ISInfoAny & isa, const ISType & type, size_t entry, int how_many = -1 )
	{
	    size_t size = type.entryArray( entry ) ? isa.readArraySize() : 1;

	    ISType entry_type = type.entryInfoType( entry );
	    for ( size_t i = 0; i < size; i++ )
	    {
		out << "[";
		for ( size_t j = 0; j < entry_type.entries(); j++ )
		{
		    print_attribute_value( out, isa, entry_type, j, how_many );
		    if ( j < entry_type.entries() - 1 )
			out << ", ";
		}
		if ( i < size - 1 )
		    out << "], ";
		else
		    out << "]";
	    }
	    return size;
	}
    };
      
#define PRINT_ATTRIBUTE(x,y)		case ISType::y : return attribute<x>::print( out, isa, type, entry, how_many );
#define PRINT_ATTRIBUTE_SEPARATOR
#define PRINT_ATTRIBUTE_OBJECT_TYPE	ISInfoAny

    size_t
    print_attribute_value( std::ostream & out, ISInfoAny & isa, const ISType & type, size_t entry, int how_many )
    {    
	switch ( type.entryType( entry ) )
	{
	    IS_TYPES( PRINT_ATTRIBUTE )
	    default :	return 0;
	}
    }

    void
    print_attribute_type( std::ostream & out, const ISType & type, size_t entry, size_t size )
    {	
	std::string type_name( type.entryTypeName( entry ) );
	if ( type.entryArray( entry ) )
	{
	    std::ostringstream os;
	    os << size;
	    type_name += "[" + os.str() + "]";
	}
	out << type_name;
    }
}

ISInfoAny::ISInfoAny()
  : is::istream_tie<is::idatastream>( m_stream )
{
    ISInfo::m_polymorphic = true;
}

void 
ISInfoAny::refreshGuts ( ISistream & in )
{ 
    m_stream = static_cast<is::istream_tie<is::idatastream> * >( &in )->stream();
}

std::ostream&
ISInfoAny::print( std::ostream & out ) const
{
    ISInfo::print( out );
    size_t count = countAttributes( );
	
    out << " has " << count << " attribute(s):" << std::endl;
    for ( size_t i = 0; i < count; i++ )
    {	
	std::ostringstream value;
	
	size_t size = print_attribute_value( value, *const_cast<ISInfoAny*>( this ), type(), i );
	
	out.width( ISType::max_type_name_width );
	out.setf( std::ios::left, std::ios::adjustfield );
	is::print_attribute_type( out, type(), i, size );
	
	out << value.str() << std::endl;
    }
    
    const_cast<ISInfoAny*>( this )->reset();
    
    return out;
}

void
ISInfoAny::operator >>=( ISInfo & info ) const 
{
    if ( !info.m_polymorphic && !info.type().subTypeOf( m_type ) )
    {
	throw daq::is::InfoNotCompatible( ERS_HERE, m_name );
    }
    info.m_partition = m_partition;
    info.m_type = m_type;
    info.m_tag = m_tag;
    info.m_name = m_name;
    info.m_address = m_address;
    info.m_time = m_time;
    info.m_annotations = m_annotations;
    info.refreshGuts( *const_cast<ISInfoAny*>( this ) );
    
    const_cast<ISInfoAny*>( this )->reset();
}

void
ISInfoAny::operator <<=( const ISInfo & info ) noexcept
{
    m_partition = info.m_partition;
    m_type = info.m_type;
    m_tag = info.m_tag;
    m_name = info.m_name;
    m_address = info.m_address;
    m_time = info.m_time;
    m_annotations = info.m_annotations;

    is::odatastream dout;
    is::ostream_tie<is::odatastream> out(dout);
    const_cast<ISInfo &>(info).publishGuts(out);

    is::idatastream din(dout);
    is::istream_tie<is::idatastream> in(din);
    refreshGuts(in);
}

std::ostream &
operator<< ( std::ostream & out, const ISInfoAny & isa )
{
    return isa.print( out );
}
