#include <is/infoany.h>
#include <is/infoenumeration.h>

ISInfoEnumeration::ISInfoEnumeration( const IPCPartition & partition,
				const std::string & server_name,
				const ISCriteria & criteria )
  : ISInfoStream(partition, server_name, criteria, true, 0),
    m_dictionary(partition)
{
    move(-1);
}

void
ISInfoEnumeration::value( ISInfo & isi ) const
{
    m_dictionary.getValue(name(), isi);
}

void
ISInfoEnumeration::value( int tag, ISInfo & isi ) const
{
    m_dictionary.getValue(name(), tag, isi);
}

void
ISInfoEnumeration::tags( std::vector< std::pair<int,OWLTime> > & tags ) const
{
    m_dictionary.getTags(name(), tags);
}

void
ISInfoEnumeration::sendCommand( const std::string & command ) const
{
    ISInfoAny info;
    value(info);

    info.sendCommand( command );
}
