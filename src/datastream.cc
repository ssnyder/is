#include <zlib.h>
#include <stdlib.h>

#include <ers/ers.h>

#include <is/datastream.h>

namespace
{
  CORBA::ULong get_threshold_value()
  {
  	static const CORBA::ULong DefaultThreshold = 8192;
        
	char * var = getenv( "TDAQ_IS_COMPRESSION_THRESHOLD" ); // default value (in bytes) 
        if ( var )
        {
            char * endptr = 0;
            CORBA::ULong value = strtol( var, &endptr, 10 );
            if ( endptr && !*endptr )
            {
            	ERS_DEBUG ( 0, "TDAQ_IS_COMPRESSION_THRESHOLD was set to " << value << " bytes" );
            	return value;
            }
            else
            {
            	ERS_DEBUG( 0, " invalid value '" << var 
                	  << "' has been used for the TDAQ_IS_COMPRESSION_THRESHOLD variable."
                          << " Default value (" << DefaultThreshold << ") will be used instead" );
            }
        }
  	
  	return DefaultThreshold;
  }
  
  const CORBA::ULong TDAQ_IS_COMPRESSION_THRESHOLD = get_threshold_value();
  
  CORBA::Octet * compress( cdrMemoryStream & in, uLongf & compressed_size, bool & release_memory )
  {
    CORBA::Octet * data = (CORBA::Octet*)in.bufPtr();
    CORBA::ULong size = in.bufSize();
    
    //////////////////////////////////////////////////////////////////
    // read the byte order boolean value
    //////////////////////////////////////////////////////////////////
    CORBA::Boolean byteSwap = in.unmarshalBoolean();
    //////////////////////////////////////////////////////////////////
    // read the bool value which indicates compression
    //////////////////////////////////////////////////////////////////
    CORBA::Boolean compress = in.unmarshalBoolean();
    if ( !compress )
    {
    	compressed_size = size;
        release_memory = false;
        in.rewindInputPtr();
        return data;
    }
    
    //////////////////////////////////////////////////////////////////
    // no compression if data size is too small
    //////////////////////////////////////////////////////////////////
    if ( size < TDAQ_IS_COMPRESSION_THRESHOLD )
    {
    	compressed_size = size;
        release_memory = false;
        in.rewindPtrs();
        in.marshalBoolean( byteSwap );
        in.marshalBoolean( false );	// no compression
        return data;
    }

    //////////////////////////////////////////////////////////////////
    // skip the long value which indicates uncompressed data size
    //////////////////////////////////////////////////////////////////
    CORBA::ULong tmp;
    tmp <<= in;
    
    //////////////////////////////////////////////////////////////////
    // offset is the size of the service fields in the CDR stream
    //////////////////////////////////////////////////////////////////
    CORBA::ULong offset = in.currentInputPtr();
    
    //////////////////////////////////////////////////////////////////
    // we have to write the uncompressed data size
    // at the beginning of the stream
    //////////////////////////////////////////////////////////////////
    in.rewindPtrs();
    in.marshalBoolean( byteSwap );
    in.marshalBoolean( compress );	// compression is on
    CORBA::ULong data_size = size - offset;
    data_size >>= in;			// write the uncompressed data size
    
    //////////////////////////////////////////////////////////////////
    // compressed buffer must be at least 0.1% larger 
    // than sourceLen plus 12 bytes as suggested by zlib
    //////////////////////////////////////////////////////////////////
    compressed_size = (uLongf)( (float)data_size * 1.001 + 13 );
    CORBA::Octet * compressed_data = CORBA::OctetSeq::allocbuf( compressed_size + offset );
    
    //////////////////////////////////////////////////////////////////
    // copy service fields from CDR stream to the
    // compressed buffer
    //////////////////////////////////////////////////////////////////
    memcpy( compressed_data, data, offset );
    
    //////////////////////////////////////////////////////////////////
    // compress with maximum speed level (1)
    //////////////////////////////////////////////////////////////////
    int err = ::compress2( compressed_data + offset, &compressed_size, data + offset, data_size, 1 );
    if ( err != Z_OK )
    {
	ERS_LOG( "::compress2 returns error " << err );
	delete[] compressed_data;
    	compressed_size = size;
        release_memory = false;
        in.rewindPtrs();
        in.marshalBoolean( byteSwap );
        in.marshalBoolean( false );	// no compression
	(CORBA::ULong)0 >>= in;		// uncompressed data size
    
        return data;
    };
    
    ERS_DEBUG( 2, "information size = " << size << ", compressed size = " << compressed_size );    
    
    compressed_size += offset;
    release_memory = true;
    
    return compressed_data;
  }
  
  CORBA::Octet * uncompress( cdrMemoryStream & in, uLongf & uncompressed_size, bool & release_memory )
  {
    CORBA::Octet * data = (CORBA::Octet*)in.bufPtr();
    CORBA::ULong size = in.bufSize();
    
    //////////////////////////////////////////////////////////////////
    // read the byte order boolean value
    //////////////////////////////////////////////////////////////////
    CORBA::Boolean byteSwap = in.unmarshalBoolean();
    in.setByteSwapFlag(byteSwap);
    
    //////////////////////////////////////////////////////////////////
    // read the bool value which indicates compression
    //////////////////////////////////////////////////////////////////
    CORBA::Boolean compress = in.unmarshalBoolean();
    if ( !compress )
    {
    	uncompressed_size = size;
        release_memory = false;
        in.rewindInputPtr();
        return data;
    }

    //////////////////////////////////////////////////////////////////
    // read the long value which indicates uncompressed data size
    //////////////////////////////////////////////////////////////////
    CORBA::ULong sz;
    sz <<= in; 
    uncompressed_size = sz;
    
    //////////////////////////////////////////////////////////////////
    // offset is the size of the service fields in the CDR stream
    //////////////////////////////////////////////////////////////////
    CORBA::ULong offset = in.currentInputPtr();

    //////////////////////////////////////////////////////////////////
    // prepare service fields for the new uncompressed buffer
    //////////////////////////////////////////////////////////////////
    cdrMemoryStream tmp( 16 );
    tmp.setByteSwapFlag( byteSwap );
    tmp.marshalBoolean( byteSwap );
    tmp.marshalBoolean( false );
    (CORBA::ULong)0 >>= tmp;
    CORBA::ULong uncompressed_offset = tmp.bufSize();

    //////////////////////////////////////////////////////////////////
    // allocate uncompressed buffer
    //////////////////////////////////////////////////////////////////
    CORBA::Octet * uncompressed_data = CORBA::OctetSeq::allocbuf( uncompressed_size  + uncompressed_offset );
    
    //////////////////////////////////////////////////////////////////
    // copy service fields for the new uncompressed buffer
    //////////////////////////////////////////////////////////////////
    memcpy( uncompressed_data, tmp.bufPtr(), uncompressed_offset );
    
    //////////////////////////////////////////////////////////////////
    // uncompress
    //////////////////////////////////////////////////////////////////
    int err = ::uncompress( uncompressed_data + uncompressed_offset, &uncompressed_size, data + offset, size - offset );
    if ( err != Z_OK )
    {
	ERS_LOG( "::uncompress returns error " << err );
    	delete[] uncompressed_data;
        uncompressed_size = size;
        release_memory = false;
        in.rewindInputPtr();
        return data;
    };

    uncompressed_size += uncompressed_offset;
    release_memory = true;
    
    return uncompressed_data;
  }
}

is::odatastream::odatastream( )
  : m_data( 100, true ) // initial memory size is 100 bytes, memory is cleared
{
    m_data.setByteSwapFlag( true );	// always
    m_data.marshalBoolean( true );	// use little-endian
    m_data.marshalBoolean( true );	// reserve space for compressed/uncompressed tag
    (CORBA::ULong)0 >>= m_data;		// reserve space for the uncompressed data size
}

void
is::odatastream::data( is::data & data )
{
    uLongf size = 0;
    bool release_memory = false;
    CORBA::Octet * cdata = compress( m_data, size, release_memory );
    data.replace( size, size, cdata, release_memory );
}


is::idatastream::idatastream( is::data & data )
  : m_data( (void*)data.get_buffer(), data.length() ),
    m_data_holder( data.get_buffer( true ) ),
    m_pos( 0 )
{
    uLongf size = 0;
    bool release_memory = false;
    CORBA::Octet * ucdata = uncompress( m_data, size, release_memory );
    if ( release_memory )
    {
    	m_data_holder.reset( ucdata );
    	m_data = cdrMemoryStream( (void*)ucdata, size );
    }
    
    CORBA::Boolean b = m_data.unmarshalBoolean();	// byte order	
    m_data.setByteSwapFlag(b);
    m_data.unmarshalBoolean();				// compression	
    CORBA::ULong tmp;
    tmp <<= m_data;					// uncompressed data size
}

is::idatastream::idatastream( is::odatastream & out )
  : m_data( out.m_data ),
    m_pos( 0 )
{
    CORBA::Boolean b = m_data.unmarshalBoolean();       // byte order
    m_data.setByteSwapFlag(b);
    m_data.unmarshalBoolean();                          // compression
    CORBA::ULong tmp;
    tmp <<= m_data;                                     // uncompressed data size
}

is::idatastream::idatastream( const idatastream & s )
  : m_data( s.m_data, true ), // read-only
    m_data_holder( s.m_data_holder ),
    m_pos( s.m_pos )
{    
    rewind();
}

is::idatastream &
is::idatastream::operator=( const idatastream & s )
{
    if ( this != &s )
    {
        m_data = s.m_data;
	m_data_holder = s.m_data_holder;
	m_pos = s.m_pos;
	rewind();
    }
    
    return *this;
}

void
is::idatastream::rewind( )
{
    m_pos = 0;
    if ( m_data.bufSize() )
    {
	m_data.rewindInputPtr();
	m_data.unmarshalBoolean();
	m_data.unmarshalBoolean();
	CORBA::ULong tmp;
	tmp <<= m_data;
    }
}

size_t
is::idatastream::read_size()
{
    size_t pos = m_data.currentInputPtr();
    CORBA::ULong size;
    size <<= m_data;
    m_data.rewindInputPtr();
    m_data.skipInput( pos - m_data.currentInputPtr() );
    return size;
}

size_t
is::idatastream::unmarshal_size()
{
    CORBA::ULong size;
    size <<= m_data;
    return size;
}
