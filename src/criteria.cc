#include <is/criteria.h>
#include <sstream>

std::ostream &
operator<<( std::ostream & out, const is::criteria::logic & lg )
{
    switch ( lg )
    {
	case is::criteria::And:
	    out << "AND";
	    break;
	case is::criteria::Or:
	    out << "OR";
	    break;
	default:
	    out << "WRONG LOGIC VALUE(" << lg << ")";
	    break;
    }
    return out;
}

std::ostream &
operator<<( std::ostream & out, const is::criteria::type::logic & tg )
{
    switch ( tg )
    {
	case is::criteria::type::Ignore:
	    out << "IGNORE";
	    break;
	case is::criteria::type::Base:
	    out << "BASE";
	    break;
	case is::criteria::type::Exact:
	    out << "EXACT";
	    break;
	case is::criteria::type::Not:
	    out << "NOT";
	    break;
	case is::criteria::type::NotBase:
	    out << "NOT-BASE";
	    break;
	default:
	    out << "WRONG LOGIC VALUE(" << (int)tg << ")";
	    break;
    }
    return out;
}

std::ostream &
operator<<( std::ostream & out, const is::criteria & crt )
{
    if ( !crt.name_.ignore_ )
	out << crt.name_.value_;
    if ( crt.type_.logic_ != is::criteria::type::Ignore )
    {
	if ( !crt.name_.ignore_ )
	    out << "-" << crt.logic_ << "-";
        
        std::string id;
	id.append( (const char *)crt.type_.value_.code.get_buffer(), 
			crt.type_.value_.code.length() );
        for ( size_t i = 0; i < id.size(); ++i )
            id[i] += 'A';

	out << crt.type_.logic_ << "-of-"
        	<< crt.type_.value_.name << "<" << id << ">";
    }
    return out;
}

std::ostream &
operator<<( std::ostream & out, const ISCriteria & crt )
{
    out << (const is::criteria&)crt;
    return out;
}

ISCriteria
operator&&( const std::string & reg_exp, const ISType & type )
{
    return ISCriteria( reg_exp, type, ISCriteria::AND );
}

ISCriteria
operator&&( const ISType & type, const std::string & reg_exp )
{
    return operator&&( reg_exp, type );
}

ISCriteria
operator||( const std::string & reg_exp, const ISType & type )
{
    return ISCriteria( reg_exp, type, ISCriteria::OR );
}

ISCriteria
operator||( const ISType & type, const std::string & reg_exp )
{
    return operator||( reg_exp, type );
}

void
ISCriteria::constructor( const std::string & reg_exp,
			 bool n_ignore,
			 const ISType & type,
			 bool t_ignore,
			 Logic logic ) 
{
    name_.ignore_ = n_ignore;
    if ( !name_.ignore_ )
	name_.value_ = reg_exp.c_str();

    logic_ = ( logic == AND ? is::criteria::And : is::criteria::Or );

    if ( t_ignore )
    {
	type_.logic_ = is::criteria::type::Ignore;
    }
    else
    {
	type.type( type_.value_ );
	if ( type.inverted() && type.weak() )
	    type_.logic_ = is::criteria::type::NotBase;
	else if ( type.inverted() )
	    type_.logic_ = is::criteria::type::Not;
	else if ( type.weak() )
	    type_.logic_ = is::criteria::type::Base;
	else
	    type_.logic_ = is::criteria::type::Exact;
    }
}

ISCriteria::ISCriteria( const std::string & reg_exp ) 
{
    constructor( reg_exp );
}

ISCriteria::ISCriteria( const ISType & type )
{
    constructor( std::string(), true, type, false );
}

ISCriteria::ISCriteria( const std::string & reg_exp, 
			const ISType & type, 
                        Logic logic ) 
{
    constructor( reg_exp, false, type, false, logic );
}

std::string
ISCriteria::toString() const
{
    std::ostringstream out;
    out << *this;
    return out.str();
}
