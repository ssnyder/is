#include <is/callbackinfo.h>
#include <is/exceptions.h>

ISCallbackInfo::ISCallbackInfo( const IPCPartition & partition,
				const std::string & server_name,
				void * user_param,
                                is::reason reason,
				const is::info & info )
  : ISCallbackEvent( partition, server_name, user_param, reason, reinterpret_cast<const is::event &>( info ) ),
    m_data( const_cast<is::info &>(info).value.data ),
    m_info( info )
{ ; }

void
ISCallbackInfo::value ( ISInfo & isi ) const 
{	 
    isi.fromWire( m_partition, m_name.c_str(), m_info.type, m_info.value, m_data );
}

