/*
 * mirror.cc
 *
 *  Created on: Sep 16, 2013
 *      Author: kolos
 */

#include <is/server/mirror.h>

ISMirror::ISMirror( const IPCPartition & partition, const std::string & server_name )
 : ISInformer( server_name, server_name ),
   m_partition( partition.getMirror() )
 {
    connect();
 }

void ISMirror::connect()
{
    try {
	m_mirror = m_partition.lookup<is::repository, ipc::use_cache, ipc::non_existent>( getServerName() );
	ERS_DEBUG( 1, "Mirror server has been connected" );
	m_failed = false;
    }
    catch( daq::ipc::Exception & ex ) {
	ERS_DEBUG( 3, "Mirror server is not found " << ex );
	m_failed = true;
    }
}

bool ISMirror::failed( )
{
    if (m_failed)
	connect();

    return false;
}



