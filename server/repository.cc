#include <tbb/tbb.h>

#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/signal.h>

#include <is/criteria.h>

#include <is/server/backup.h>
#include <is/server/exceptions.h>
#include <is/server/repository.h>

namespace is
{
    namespace backup
    {
	const unsigned int MagicNumber = 0xefeffefe;
	const unsigned int VersionNumber = 0x0000000e;
    }
}

ISRepository::ISRepository(const IPCPartition& partition, const std::string & name)
    : IPCNamedObject<POA_is::repository>(partition, name),
      m_mirror(partition, name),
      m_stream_pool(20)
{
    ;
}

ISRepository::~ISRepository()
{
    ERS_DEBUG( 1,
	    "IS server '" << name() << "' for partition '" << partition().name() << "' is destroyed");
    ISReceiverSet::iterator it = m_receivers.begin();
    while ( it != m_receivers.end() ) {
	(*it)->release();
	it = m_receivers.erase(it);
    }
}

void ISRepository::shutdown()
{
    daq::ipc::signal::raise(SIGTERM);
}

void ISRepository::receive(const is::info & info, is::reason reason)
{
    switch (reason)
    {
	case is::Created:
	case is::Updated:
	    checkin(info, false, true);
	    break;
	case is::Deleted:
	    remove(info.name);
	default:
	    break;
    }
}

boost::shared_ptr<ISInfoHolder>
ISRepository::find(const char * name)
{
    ISInfoSetLock::scoped_lock lock(m_info_mutex, false);	// read-lock
    ISInfoSet::iterator it = m_information.find(name);
    if (it == m_information.end()) {
	ERS_DEBUG( 3, "Information object '" << name << "' does not exist");
	throw is::NotFound();
    }

    ERS_DEBUG( 2, "Information object '" << name << "' is found");
    return *it;
}

void ISRepository::create(const is::info & info)
{
    ERS_DEBUG( 2,
	    "Request to create the '" << info.name << "' information object");

    const char * key((const char *) info.name);
    {
	ISInfoSetLock::scoped_lock lock(m_info_mutex, true);	// write-lock
	ISInfoSet::iterator it = m_information.find(key);

	if (it == m_information.end()) {
	    m_information.insert(
		    boost::shared_ptr<ISInfoHolder>(
		            new ISInfoHolder(info, m_receivers, m_receiver_mutex, m_mirror)));
	}
	else {
	    ERS_DEBUG( 3,
		    "Information object '" << info.name << "' already exist");
	    throw is::AlreadyExist();
	}
    }

    ERS_DEBUG( 2, "Information object '" << info.name << "' is created");
}

void ISRepository::remove(const char * name)
{
    ERS_DEBUG( 2, "Request to remove the '" << name << "' information object");

    ISInfoSetLock::scoped_lock lock(m_info_mutex, true);	// write-lock
    ISInfoSet::iterator it = m_information.find(name);
    if (it == m_information.end()) {
	ERS_DEBUG( 3, "Information object '" << name << "' does not exist");
	throw is::NotFound();
    }
    m_information.erase(it);

    ERS_DEBUG( 2, "Information object '" << name << "' has been destroyed");
}

void ISRepository::checkin(const is::info & info, bool keep_history, bool use_tag)
{
    ERS_DEBUG( 2,
	    "Request to checkin the '" << info.name
	    << "' information object (keep_history = " << keep_history
	    << ", use_tag = " << use_tag << ")");

    const char * key((const char *) info.name);
    {
	ISInfoSetLock::scoped_lock lock(m_info_mutex, false);	// read-lock
	ISInfoSet::iterator it = m_information.find(key);

	if (it == m_information.end()) {
	    lock.upgrade_to_writer();

	    m_information.insert(
		    boost::shared_ptr<ISInfoHolder>(
		            new ISInfoHolder(info, m_receivers, m_receiver_mutex, m_mirror)));
	    ERS_DEBUG( 3,
		    "Information object '" << info.name << "' is created");
	}
	else {
	    boost::shared_ptr<ISInfoHolder> ih(*it);
	    lock.release();
	    ih->update(info, keep_history, use_tag);
	    ERS_DEBUG( 3,
		    "Information object '" << info.name << "' is updated");
	}
    }
}

void ISRepository::update(const is::info & info, bool keep_history, bool use_tag)
{
    ERS_DEBUG( 2,
	    "Request to update the '" << info.name
	    << "' information object (keep_history = " << keep_history
	    << ", use_tag = " << use_tag << ")");

    const char * key((const char *) info.name);
    boost::shared_ptr<ISInfoHolder> i = find(key);

    i->update(info, keep_history, use_tag);
    ERS_DEBUG( 2, "Information object '" << info.name << "' is updated");
}

void ISRepository::get_type(const char * name, is::type_out type)
{
    ERS_DEBUG( 2,
	    "Request to find the type of the '" << name << "' information object");

    boost::shared_ptr<ISInfoHolder> i = find(name);

    ERS_DEBUG( 2, "Information object '" << name << "' found");
    type = new is::type(i->type());
}

void ISRepository::get_last_value(const char * name, is::info_out result)
{
    ERS_DEBUG( 2, "Request to find the '" << name << "' information object");

    boost::shared_ptr<ISInfoHolder> i = find(name);

    result = new is::info;
    i->getLastValue(*((is::info*) result));
}

void ISRepository::get_value(const char * name, CORBA::Long tag,
        is::info_out result)
{
    ERS_DEBUG( 2, "Request to find the '" << name << "' information object");

    boost::shared_ptr<ISInfoHolder> ih = find(name);

    result = new is::info;
    ih->getTagValue(tag, *((is::info*) result));
}

bool ISRepository::contains(const char * name)
{
    ERS_DEBUG( 2, "Request to check the '" << name << "' information object");

    ISInfoSetLock::scoped_lock lock(m_info_mutex, false);	    // read-lock
    ISInfoSet::iterator it = m_information.find(name);

    bool found = (it != m_information.end());

    ERS_DEBUG( 2,
	    "Information object '" << name << ( found ? "' exists" : "' does not exist" ));

    return found;
}

void ISRepository::get_last_values(
    const char * name, CORBA::Long how_many, is::sorted order, is::info_history_out result)
{
    ERS_DEBUG( 2,
	    "Request for the history values of the '" << name
	    << "' information object (how_many = " << how_many << ")");

    boost::shared_ptr<ISInfoHolder> i = find(name);

    is::info_history * r = new is::info_history;
    i->getLastValues(*r, how_many, order);
    result = r;
}

void ISRepository::get_tags(const char * name, is::tag_list_out tags)
{
    ERS_DEBUG( 2,
	    "Request for the history tags of the '" << name << "' information object");

    boost::shared_ptr<ISInfoHolder> i = find(name);

    i->getTags(tags);
}

std::string
ISRepository::constructReceiverID(is::callback_ptr r)
{
    std::string s = IPCCore::objectToString(r, IPCCore::Corbaloc);
    return s.substr(0, s.find('/'));
}

void
ISRepository::test(const char * name)
{
    if (::strcmp(name, "dump_receivers"))
	return;

    ISReceiverSetLock::scoped_lock lock(m_receiver_mutex, false); // read-only lock
    std::clog << "IS server contains " << m_receivers.size()
	<< " receivers:" << std::endl;
    ISReceiverSet::iterator it = m_receivers.begin();
    for ( ; it != m_receivers.end(); ++it ) {
    	(*it)->dump(std::clog);
        std::clog << std::endl;
    }
}

void
ISRepository::clearDeadReceivers()
{
    ISReceiverSet::iterator it = m_receivers.begin();
    while ( it != m_receivers.end() ) {
    	if ( (*it)->failed() ) {
    	    (*it)->release();
    	    it = m_receivers.erase(it);
    	}
    	else {
    	    ++it;
    	}
    }
}

void ISRepository::subscribe(
    const is::criteria & criteria, is::callback_ptr r, CORBA::Long event_mask)
{
    std::string receiver_id = constructReceiverID(r);
    
    ERS_LOG("subscribe request with criteria (" << criteria
	<< ") with event_mask=" << event_mask
	<< " from the " << receiver_id << " receiver");

    std::pair<boost::shared_ptr<ISCriteriaHelper>,boost::shared_ptr<ISCallback>> pair;
    {
	ISReceiverSetLock::scoped_lock lock(m_receiver_mutex, true); // write lock
	ISReceiverSet::iterator rit = m_receivers.find(receiver_id);
	if ( rit == m_receivers.end() ) {
	    clearDeadReceivers();
	    rit = m_receivers.insert(
		boost::shared_ptr<ISReceiver>(new ISReceiver(name(), r))).first;
	}

	pair = (*rit)->addCallback(criteria, r, event_mask);
    }

    ISInfoSetLock::scoped_lock lock(m_info_mutex, false);	    // read-lock
    for( ISInfoSet::iterator it = m_information.begin(); it != m_information.end(); ++it) {
	(*it)->maybeAddCallback(*pair.first, pair.second);
    }

    ERS_DEBUG( 1, "request successful");
}

void
ISRepository::unsubscribe(const is::criteria & criteria,
        is::callback_ptr r)
{
    std::string receiver_id = constructReceiverID(r);

    ERS_LOG("unsubscribe request with criteria (" << criteria << ") from " << receiver_id << " receiver");

    ISReceiverSetLock::scoped_lock lock(m_receiver_mutex, true); // write lock
    ISReceiverSet::iterator rit = m_receivers.find(receiver_id);
    if ( rit == m_receivers.end() ) {
	throw is::SubscriptionNotFound();
    }

    if (!(*rit)->removeCallback(r)) {
	(*rit)->release();
	m_receivers.erase(rit);
    }

    ERS_DEBUG( 1, "request successful");
}

void
ISRepository::add_callback(const char * info_name, is::callback_ptr r, CORBA::Long event_mask)
{
    std::string receiver_id = constructReceiverID(r);

    ERS_LOG("request to subscribe for the '" << info_name
	<< "' information object with event_mask=" << event_mask
	<< " from the " << receiver_id << " receiver");

    boost::shared_ptr<ISCallback> callback;
    {
	ISReceiverSetLock::scoped_lock lock(m_receiver_mutex, true); // write lock
	ISReceiverSet::iterator rit = m_receivers.find(receiver_id);
	if ( rit == m_receivers.end() ) {
	    clearDeadReceivers();
	    rit = m_receivers.insert(
		boost::shared_ptr<ISReceiver>(new ISReceiver(name(), r))).first;
	}

	callback = (*rit)->addCallback(info_name, r, event_mask);
    }

    ISInfoSetLock::scoped_lock lock(m_info_mutex, false);	// read-lock
    ISInfoSet::iterator it = m_information.find(info_name);
    if (it != m_information.end())
	(*it)->addCallback(callback);

    ERS_DEBUG( 1, "request successful");
}

void
ISRepository::remove_callback(const char * name, is::callback_ptr r)
{
    std::string receiver_id = constructReceiverID(r);

    ERS_LOG("request to unsubscribe from the '" << name
	<< "' information object from " << receiver_id << " receiver");

    ISReceiverSetLock::scoped_lock lock(m_receiver_mutex, true); // write lock
    ISReceiverSet::iterator rit = m_receivers.find(receiver_id);
    if ( rit == m_receivers.end() ) {
	throw is::SubscriptionNotFound();
    }

    if (!(*rit)->removeCallback(r)) {
	(*rit)->release();
	m_receivers.erase(rit);
    }

    ERS_DEBUG( 1, "request successful");
}

void
ISRepository::create_stream(const is::criteria & criteria,
        is::stream_ptr stream, CORBA::Long history_depth, is::sorted order)
{
    ERS_DEBUG( 1, "request with criteria " << criteria);

    // Don't worry, this is self-destructing object
    new ISStreamHelper(*this, criteria, stream, history_depth, order);
}

void ISRepository::remove_all(const is::criteria & c)
{
    ERS_DEBUG( 2, "request with criteria " << c);

    ISCriteriaHelper ch(c);
    ISInfoSetLock::scoped_lock lock(m_info_mutex, true);	// read-lock
    ISInfoSet::iterator it = m_information.begin();
    while (it != m_information.end())
    {
	if (ch.match((*it)->name(), (*it)->type()))
	    it = m_information.erase(it);
	else
	    ++it;
    }
}

void
ISRepository::write(std::ostream & out) const
{
    ERS_DEBUG( 1, "Saving information:");

    out.write((const char *) &is::backup::MagicNumber, sizeof(is::backup::MagicNumber));
    out.write((const char *) &is::backup::VersionNumber,
	sizeof(is::backup::VersionNumber));
    out.write((const char *) &ISConfig::HistoryNumDepth, sizeof(ISConfig::HistoryNumDepth));

    ISInfoSetLock::scoped_lock lock(m_info_mutex, false);	// read-lock
    unsigned int isize = m_information.size();
    out.write((const char *) &isize, sizeof(isize));

    for (ISInfoSet::const_iterator it = m_information.begin();
	it != m_information.end(); ++it)
    {
	out <<= (*it)->name();
	out <<= (*it)->type();
	(*it)->write(out);

	ERS_DEBUG( 3, "Object " << (*it) -> name() << " has been saved");
    }

    ERS_DEBUG( 1, m_information.size() << " information object(s) have been saved");
}

void
ISRepository::read(std::istream & in)
{
    ERS_DEBUG( 1, "Restoring information:");

    unsigned int magic_num = 0;
    in.read((char *) &magic_num, sizeof(magic_num));
    if (magic_num != is::backup::MagicNumber)
    {
	throw is::BadBackupFile(ERS_HERE, "backup file is invalid or corrupted" );
    }

    unsigned int version_num = 0;
    in.read((char *) &version_num, sizeof(version_num));
    if (version_num != is::backup::VersionNumber)
    {
	throw is::BadBackupFile(ERS_HERE, "old backup file is not supported" );
    }

    unsigned int history;
    in.read((char *) &history, sizeof(history));
    ISConfig::HistoryNumDepth = std::max(ISConfig::HistoryNumDepth, history);

    unsigned int info_num = 0;
    in.read((char *) &info_num, sizeof(info_num));
    ERS_LOG( "Restoring " << info_num << " information object(s) ...");

    unsigned int restored_info = 0;
    {
	ISInfoSetLock::scoped_lock lock(m_info_mutex, true);	// write-lock
	for (unsigned int i = 0; i < info_num; i++)
	{
	    ERS_DEBUG( 3, "Restoring information object # " << i << " ... ");
	    std::string name;
	    in >>= name;
	    is::type type;
	    in >>= type;
	    boost::shared_ptr<ISInfoHolder> info(new ISInfoHolder(name, type, m_mirror));
	    info->read(in);

	    m_information.insert(info);
	    ++restored_info;
	    ERS_DEBUG( 3, "Information object " << name.c_str() << " is restored");
	}
    }

    ERS_LOG( restored_info << " information object(s) restored");

    if (info_num != restored_info)
    {
	throw is::BadBackupFile(ERS_HERE, "check for number of restored objects failed" );
    }
}

