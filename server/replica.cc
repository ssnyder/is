#include <ipc/core.h>

#include <is/server/exceptions.h>
#include <is/server/backup.h>
#include <is/server/replica.h>

///////////////////////////////////////////////////////////////////////////////////////////////////
//
// ISReplica class implementation
// 
///////////////////////////////////////////////////////////////////////////////////////////////////
ISReplica::ISReplica(	const IPCPartition & partition,
    			const std::string & name,
                	const std::string & restore_file,
                	const std::string & backup_file,
                	int backup_period )
  : m_repository( new ISRepository(partition, name ) ),
    m_backup_file( backup_file ),
    m_backup_period( backup_period ),
    m_backup_alarm( 0 )
{
    restore( restore_file );
    m_repository->publish();

    if ( m_backup_file.empty() )
    {
	m_backup_file = partition.name() + "." + name + ".is.backup";
    }

    if ( m_backup_period > 0 )
    {
	m_backup_alarm = new IPCAlarm( m_backup_period, 
        		boost::bind( &ISReplica::periodicAction, this ) );
    }
}

ISReplica::~ISReplica()
{
    delete m_backup_alarm;
    
    if ( m_backup_period > -1 )
    {
	backup();
    }
    
    try {
	m_repository->withdraw();
    }
    catch ( daq::ipc::Exception & ex ) {
    	ers::log( ex );
    }

    m_repository->_destroy( true );
}

void
ISReplica::backup( )
{
    ERS_LOG( " Saving information ... " );

    try {
	bofstream fout(m_backup_file);
	m_repository->write( fout );
    }
    catch( std::exception & ex ) {
	ers::error( is::CantWriteFile( ERS_HERE, m_backup_file, ex ) );
	return ;
    }
    ERS_LOG( "data was sucessfully saved." );
}

void
ISReplica::restore( const std::string & restore_file )
{
    if ( restore_file.empty() )
    {
    	return;
    }
    
    ERS_LOG( "Restoring information from the " << restore_file << " file ... " );
    try {
	bifstream fin(restore_file);
	m_repository->read( fin );
    }
    catch( is::BadBackupFile & ex ) {
	ers::error( is::CantReadFile( ERS_HERE, restore_file, ex ) );
	return ;
    }
    catch( std::exception & ex ) {
	ers::error( is::CantReadFile( ERS_HERE, restore_file, ex ) );
	return ;
    }
    ERS_LOG( "data was successfully restored." );
}

bool 
ISReplica::periodicAction()
{
    backup( );
    return true;
}
