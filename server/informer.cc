#include <boost/lexical_cast.hpp>

#include <ipc/core.h>
#include <ipc/exceptions.h>
#include <ipc/applicationcontext.h>

#include <is/server/informer.h>
#include <is/server/backup.h>
#include <is/server/config.h>
#include <is/server/repository.h>

ERS_DECLARE_ISSUE( is,
    HangingSubscriber,
    "The number of timeouts exceeds the maximum tolerated configuration parameter ("
	<< max_config << ")"
	<< " the '" << subscriber << "' receiver will no longer be serviced",
    ((unsigned int)max_config )
    ((std::string)subscriber )
    )

ISInformer::ISInformer(const std::string & server, is::callback_ptr r)
    : m_max_active_callbacks(ISConfig::MaxConcurrentCallbacks),
      m_active_callbacks(0),
      m_failed(false),
      m_timedout(0),
      m_terminated(false),
      m_slow_reported(false),
      m_dropped_counter(0),
      m_server_name(server),
      m_receiver_id(ISRepository::constructReceiverID(r)),
      m_thread(std::bind(&ISInformer::thread_wrapper, this))
{
    IPCApplicationContext c(r);
    if (c.m_valid) {
        m_receiver_name = c.m_name + '@' + c.m_host + ':' + boost::lexical_cast<std::string>(c.m_pid);
    }
    else {
        m_receiver_name = m_receiver_id;
        ++m_timedout;
    }
}

ISInformer::ISInformer(const std::string & server, const std::string & receiver)
    : m_max_active_callbacks(ISConfig::MaxConcurrentCallbacks),
      m_active_callbacks(0),
      m_failed(false),
      m_timedout(0),
      m_terminated(false),
      m_slow_reported(false),
      m_dropped_counter(0),
      m_server_name(server),
      m_receiver_name(receiver),
      m_receiver_id(receiver),
      m_thread(std::bind(&ISInformer::thread_wrapper, this))
{
    ;
}

ISInformer::~ISInformer()
{
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_terminated = true;
        m_condition.notify_one();
    }
    m_thread.join();
}

void
ISInformer::handleTimeout()
{
    ERS_LOG( "got TIMEOUT exception for the " << m_receiver_name
	<< " receiver, m_active_callbacks = " << m_active_callbacks
	<< ", m_max_active_callbacks = " << m_max_active_callbacks
	<< ", queue occupancy = " << m_invocations.size());

    bool hanging_subscriber = false;
    {
	std::lock_guard<std::mutex> lock(m_mutex);

	if (m_max_active_callbacks > 0)
	    --m_max_active_callbacks;

	if (++m_timedout > ISConfig::MaxToleratedTimeouts) {
	    m_failed = true;
	    m_invocations = std::queue<std::shared_ptr<Invocation>>();
	    hanging_subscriber = true;
	}
    }

    if (hanging_subscriber)
	ers::error(is::HangingSubscriber(ERS_HERE, ISConfig::MaxToleratedTimeouts, m_receiver_name));
}

bool
ISInformer::handleSystemException(CORBA::SystemException & ex)
{
    if (ex.minor() == omni::TRANSIENT_CallTimedout) {
	handleTimeout();
	return true;
    }

    {
	std::lock_guard<std::mutex> lock(m_mutex);
	m_failed = true;
	m_invocations = std::queue<std::shared_ptr<Invocation>>();
    }
    ERS_LOG(
	"got " << ex << " exception for the " << m_receiver_name << " receiver");
    return false;
}

void
ISInformer::thread_wrapper()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    while (!m_terminated)
    {
	m_condition.wait_for(lock,
        	std::chrono::seconds(ISConfig::ReceiversCheckupPeriod),
        	[this](){return !m_invocations.empty() || m_terminated;});

	if (failed()) {
	    break;
	}

	while (!m_invocations.empty() && !m_terminated)
	{
	    std::shared_ptr<Invocation> ii = m_invocations.front();

	    try {
		lock.unlock();
		(*ii)();
		lock.lock();
		if (!m_invocations.empty())
		    m_invocations.pop();
	    }
	    catch(CORBA::TIMEOUT & ) {
		handleTimeout();
		lock.lock();
	    }
	    catch(CORBA::SystemException & ex) {
		bool timeout = handleSystemException(ex);
		lock.lock();
		if (!timeout) {
                    break;
		}
	    }
	}
        
	if (m_max_active_callbacks != ISConfig::MaxConcurrentCallbacks)
	{
	    ERS_DEBUG( 1,
		"notifications queue is empty, restoring initial configuration parameters");
	    m_max_active_callbacks = ISConfig::MaxConcurrentCallbacks;
	    m_slow_reported = false;
	}
    }
    ERS_DEBUG(1, "Informer thread terminated, m_failed = " << m_failed << "; m_terminated = " << m_terminated);
}
