#include <boost/bind/bind.hpp>

#include <is/server/config.h>
#include <is/server/repository.h>
#include <is/server/streamhelper.h>

#include <owl/time.h>

const size_t ISStreamHelper::m_max_length = 10000;

//////////////////////////////////////////////////
// ISStreamHelper constructor implementation
//////////////////////////////////////////////////
ISStreamHelper::ISStreamHelper(ISRepository & repository,
    const is::criteria & criteria,
    is::stream_ptr stream,
    size_t history_depth, is::sorted order)
    : m_repository(repository),
      m_criteria(criteria),
      m_stream(is::stream::_duplicate(stream)),
      m_history_depth(history_depth),
      m_history_sort_order(order),
      m_error(false),
      m_entries(0),
      m_fill_buffer(0),
      m_send_buffer(1),
      m_ready_to_send(false),
      m_done(false)
{
    m_repository.m_stream_pool.addJob(boost::bind(&ISStreamHelper::fill, this));
}

//////////////////////////////////////////////////
// ISStreamHelper methods implementation
//////////////////////////////////////////////////
size_t
ISStreamHelper::push(const boost::shared_ptr<ISInfoHolder> & info)
{
    CORBA::ULong l = m_buffers[m_fill_buffer].length();
    m_buffers[m_fill_buffer].length(l + 1);
    info->getLastValues(m_buffers[m_fill_buffer][l], m_history_depth, m_history_sort_order);

    return m_buffers[m_fill_buffer][l].values.length();
}

void
ISStreamHelper::fill()
{
    ERS_LOG("creating (" << this << ") object stream");
    boost::thread send_thread(boost::bind(&ISStreamHelper::send, this));

    m_buffers[m_fill_buffer].length(m_max_length);
    m_buffers[m_fill_buffer].length(0);
    m_buffers[m_send_buffer].length(m_max_length);
    m_buffers[m_send_buffer].length(0);
    const ISInfoSet * set;
    {
	ISRepository::DataLock data_lock(m_repository, set);
	ERS_DEBUG( 1, "IS stream helper acquires data lock "  << this);
	size_t length = 0;
	for (ISInfoSetSorted::const_iterator it = set->get<1>().begin();
	    it != set->get<1>().end(); ++it)
	{
	    const boost::shared_ptr<ISInfoHolder> & info = *it;
	    if (m_criteria.match(info->name(), info->type()))
	    {
	        length += push(info);
	        if (length >= m_max_length)
		{
		    ERS_DEBUG(1, "Buffer is full, let's wait for the sending thread");
		    std::unique_lock lock(m_mutex);
		    m_condition.wait(lock, [this]{return m_ready_to_send;});
		}
	    }

	    if (m_ready_to_send && m_buffers[m_fill_buffer].length())
	    {
                if (m_error) {
                    break;
                }
		std::unique_lock lock(m_mutex);
		ERS_DEBUG( 1, "IS stream (" << this << ") is ready to send data: " << m_buffers[m_fill_buffer].length());
		m_send_buffer = m_fill_buffer;
		m_fill_buffer = !m_send_buffer;
	        m_ready_to_send = false;
	        length = 0;
		m_condition.notify_one();
	    }
	}
    }
    if (!m_error)
    {
	std::unique_lock lock(m_mutex);
        ERS_DEBUG( 1, "IS stream (" << this << ") is ready to send data: " << m_buffers[m_fill_buffer].length());
	m_condition.wait(lock, [this]{return m_ready_to_send;});
	m_send_buffer = m_fill_buffer;
	m_fill_buffer = !m_send_buffer;
	m_done = true;
	m_condition.notify_one();
    }

    send_thread.join();
    ERS_LOG(m_entries << " objects have been sent to (" << this << ") object stream");
    delete this;
}

void
ISStreamHelper::send()
{
    omniORB::setClientCallTimeout(m_stream, 10000);

    std::unique_lock lock(m_mutex);
    while (!m_done)
    {
	m_ready_to_send = true;
	m_condition.notify_one();
	m_condition.wait(lock);

        if (m_buffers[m_send_buffer].length()) {
            try {
                    ERS_DEBUG(1, "Sending " << m_buffers[m_send_buffer].length() << " objects to (" << this << ") stream");

                    m_stream->push(m_buffers[m_send_buffer]);

                    m_entries += m_buffers[m_send_buffer].length();
                    m_buffers[m_send_buffer].length(0);
                    ERS_DEBUG(1, "Total number of objects sent to is (" << this << ") stream is " << m_entries);
            }
            catch (CORBA::SystemException & ex) {
                ERS_LOG( "exception " << ex << " sending data to is (" << this << ") stream");
                m_error = true;
                m_ready_to_send = true;
                m_condition.notify_one();
                return;
            }
        }
    }

    try {
	m_stream->eof();
    }
    catch (CORBA::SystemException & ex) {
	ERS_LOG( "exception " << ex << " sending data to is (" << this << ") stream");
    }
}
