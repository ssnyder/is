#include <is/criteria.h>

#include <is/server/criteriahelper.h>
#include <is/server/exceptions.h>

using namespace std::placeholders;

bool
is::types_equal( const is::type & t1, const is::type & t2 )
{
    const size_t length = t1.code.length();
    if ( length != t2.code.length() )
	return false;
    const CORBA::Char * const b1 = t1.code.get_buffer();
    const CORBA::Char * const b2 = t2.code.get_buffer();
    for ( size_t i = 0; i < length; ++i )
	if ( b1[i] != b2[i] )
	    return false;
    return !strcmp( t1.name, t2.name );
}

bool
is::super_sub_type( const is::type & t1, const is::type & t2 )
{
    const size_t length = t1.code.length();
    if ( length > t2.code.length() )
	return false;
    for ( size_t i = 0; i < length; ++i )
	if ( t1.code.get_buffer()[i] != t2.code.get_buffer()[i] )
	    return false;
    return true;
}

ISCriteriaHelper::ISCriteriaHelper( const is::criteria & criteria )
  : m_criteria( criteria )
{ 
    std::ostringstream o;
    o << m_criteria;
    m_string = o.str();

    if ( !m_criteria.name_.ignore_ )
    {
	m_owl_regex = m_criteria.name_.value_;
        
        try {
            m_boost_regex = boost::regex( m_criteria.name_.value_ );
        }
        catch ( boost::bad_expression & ex ){
	    throw is::InvalidCriteria( m_criteria.name_.value_ );
        }
        
        bool (ISCriteriaHelper::*logic)( NameMatcher , TypeMatcher , const char * , const is::type & ) const;
        
	switch ( m_criteria.logic_ )
	{
	    case is::criteria::And:
		logic = &ISCriteriaHelper::AND;
		break;
	    case is::criteria::Or:
		logic = &ISCriteriaHelper::OR;
		break;
	    default:
		throw is::InvalidCriteria( "Bad logic value" );
	}
	
        NameMatcher nmatcher;
	if ( m_owl_regex.status() == OWLRegexp::Ok )
	    nmatcher = std::bind( &ISCriteriaHelper::matchName, this,
            				std::cref( m_owl_regex ), _1 );
	else
            nmatcher = std::bind( &ISCriteriaHelper::matchNameBoost, this,
            				std::cref( m_boost_regex ), _1 );
        
        TypeMatcher tmatcher;
        switch ( m_criteria.type_.logic_ )
        {
            case is::criteria::type::Ignore:
		if ( m_owl_regex.status() == OWLRegexp::Ok )
		    m_match = std::bind( &ISCriteriaHelper::matchName, this,
						std::cref( m_owl_regex ), _1 );
		else
		    m_match = std::bind( &ISCriteriaHelper::matchNameBoost, this,
						std::cref( m_boost_regex ), _1 );
                return;
	    case is::criteria::type::Exact:
		tmatcher = std::bind( &ISCriteriaHelper::matchTypeExact, this, _1 );
                break;
	    case is::criteria::type::Base:
		tmatcher = std::bind( &ISCriteriaHelper::matchTypeBase, this, _1 );
                break;
	    case is::criteria::type::Not:
		tmatcher = std::bind( &ISCriteriaHelper::matchTypeNot, this, _1 );
                break;
	    case is::criteria::type::NotBase:
		tmatcher = std::bind( &ISCriteriaHelper::matchTypeNotBase, this, _1 );
                break;
            default:
            	throw is::InvalidCriteria( "Bad type logic value" );
        }
	
        m_match = std::bind( logic, this, nmatcher, tmatcher, _1, _2 );
    }
    else
    {
	switch ( m_criteria.type_.logic_ )
        {
            case is::criteria::type::Ignore:
		throw is::InvalidCriteria( "Void criteria" );
                break;
	    case is::criteria::type::Exact:
		m_match = std::bind( &ISCriteriaHelper::matchTypeExact, this, _2 );
                break;
	    case is::criteria::type::Base:
		m_match = std::bind( &ISCriteriaHelper::matchTypeBase, this, _2 );
                break;
	    case is::criteria::type::Not:
		m_match = std::bind( &ISCriteriaHelper::matchTypeNot, this, _2 );
                break;
	    case is::criteria::type::NotBase:
		m_match = std::bind( &ISCriteriaHelper::matchTypeNotBase, this, _2 );
                break;
            default:
            	throw is::InvalidCriteria( "Bad type logic value" );
        }
    }
}

const std::string &
ISCriteriaHelper::toString() const
{
    return m_string;
}
