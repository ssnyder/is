#include <cstdio>

#include <ipc/core.h>
#include <is/server/backup.h>

using namespace std;

namespace {
    const std::string name_suffix = ".shadow";
}

bofstream::bofstream(const string & fname)
  : m_name(fname), m_bck_name(m_name + name_suffix)
{
    if (rename(m_name.c_str(), m_bck_name.c_str())) {
	ERS_LOG("Can not rename '" + m_name + "' backup file: " + strerror(errno));
    }
    this->open(m_name, ofstream::out | ofstream::trunc);
    exceptions( ios::badbit | ios::failbit | ios::eofbit );
}

bofstream::~bofstream()
{
    try {
	this->close();
	if (remove(m_bck_name.c_str())) {
	    ERS_LOG("Can not remove '" + m_bck_name + "' backup file: " + strerror(errno));
	}
    } catch (std::exception & ex) {
	ERS_LOG("Can not properly close the '" + m_name + "' backup file: " + strerror(errno));
    }
}

bifstream::bifstream(const string & fname)
  : m_name(fname), m_bck_name(m_name + name_suffix)
{
    this->open(m_bck_name);
    if (good()) {
	this->close();
	if (rename(m_bck_name.c_str(), m_name.c_str())) {
	    ERS_LOG("Can not rename '" + m_bck_name + "' backup file: " + strerror(errno));
	    this->open(m_bck_name);
	} else {
	    this->open(m_name);
	}
    } else {
	this->open(m_name);
    }
    exceptions( ios::badbit | ios::failbit | ios::eofbit );
}

ostream &
operator<<=( ostream & out, const string & str )
{
    unsigned int size = str.size();
    out.write( (const char*)&size, sizeof( size ) );
    out.write( (const char*)str.c_str(), size );
    return out;
}

istream &
operator>>=( istream & in, string & str )
{
    unsigned int size = 0;
    in.read( (char*)&size, sizeof( size ) );

    char * buff = new char[size + 1];
    in.read( buff, size );
    buff[size] = 0;

    str = buff;
    delete[] buff;
    return in;
}

ostream &
operator<<=( ostream & out, const char * str )
{
    unsigned int size = strlen(str);
    out.write( (const char*)&size, sizeof( size ) );
    out.write( (const char*)str, size );
    return out;
}

ostream &
operator<<= ( ostream & out, const CORBA::String_var & str )
{
    unsigned int size = strlen(str);
    out.write( (const char*)&size, sizeof( size ) );
    out.write( (const char*)str, size );
    return out;
}

istream &
operator>>=( istream & in, CORBA::String_var & str )
{
    unsigned int size = 0;
    in.read( (char*)&size, sizeof( size ) );

    char * buff = new char[size + 1];
    in.read( buff, size );
    buff[size] = 0;

    str = buff;
    return in;
}

ostream &
operator<<=( ostream & out, const is::data & dd )
{
    unsigned int length = dd.length();
    out.write( (const char *)&length, sizeof(length) );
    out.write( (const char *)dd.get_buffer(), length );
    return out;
}

istream &
operator>>=( istream & in, is::data & dd )
{
    unsigned int length = 0;
    in.read( (char *)&length, sizeof(length) );
    dd.length( length );
    in.read( (char *)dd.get_buffer(), length );
    return in;
}

ostream &
operator<<=( ostream & out, const is::address & dd )
{
    unsigned int length = dd.length();
    out.write( (const char *)&length, sizeof(length) );
    out.write( (const char *)dd.get_buffer(), length );
    return out;
}

istream &
operator>>=( istream & in, is::address & dd )
{
    unsigned int length = 0;
    in.read( (char *)&length, sizeof(length) );
    dd.length( length );
    in.read( (char *)dd.get_buffer(), length );
    return in;
}

ostream &
operator<<=( ostream & out, const is::type & tt )
{
    unsigned int length = tt.nested_types.length();
    out <<= tt.name;
    out <<= tt.code;
    out.write( (const char *)&length, sizeof(length) );
    for ( size_t i = 0; i < length; i++ )
	out <<= tt.nested_types[i];
    return out;
}

istream &
operator>>=( istream & in, is::type & tt )
{
    in >>= tt.name;
    in >>= tt.code;
    unsigned int length = 0;
    in.read( (char *)&length, sizeof(length) );
    tt.nested_types.length( length );
    for ( size_t i = 0; i < length; i++ )
	in >>= tt.nested_types[i];
    return in;
}

ostream &
operator<<=( ostream & out, const is::value & vv )
{
    out.write( (const char *)&vv.attr.tag, sizeof(vv.attr.tag) );
    out.write( (const char *)&vv.attr.time, sizeof(vv.attr.time) );
    out <<= vv.attr.address;
    out <<= vv.data;
    return out;
}

istream &
operator>>=( istream & in, is::value & vv )
{
    in.read( (char *)&vv.attr.tag, sizeof(vv.attr.tag) );
    in.read( (char *)&vv.attr.time, sizeof(vv.attr.time) );
    in >>= vv.attr.address;
    in >>= vv.data;
    return in;
}

ostream &
operator<<=( ostream & out, const CORBA::String_member & vv )
{
    unsigned int length = strlen(vv);
    out.write( (const char *)&length, sizeof(length) );
    out.write( (const char *)vv, length );
    return out;
}

istream &
operator>>=( istream & in, CORBA::String_member & vv )
{
    unsigned int length = 0;
    in.read( (char *)&length, sizeof(length) );
    char * buf = new char[length+1];
    in.read( (char *)buf, length );
    buf[length] = 0;
    vv = buf;
    return in;
}
