#include <iostream>

#include <boost/thread.hpp>

#include <ipc/core.h>

#include <is/infoany.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <is/infodocument.h>
#include <is/infoiterator.h>
#include <is/serveriterator.h>

#include <infodialog.h>

#include <X11/Intrinsic.h>
#include <X11/cursorfont.h>
 
#include <Xm/Xm.h>
#include <Xm/List.h>
#include <Xm/PushB.h>
#include <Xm/Text.h>
#include <Xm/TextF.h>
#include <Xm/Matrix.h>
#include <Xm/MessageB.h>
#include <Xm/CascadeBG.h>
#include <Xm/DialogS.h>
#include <Xm/RowColumn.h>

#include <ismonitor.h>

extern Widget wServerList;
extern void MouseMotionOverTable( Widget w, XEvent * event, String * , Cardinal * );

void mouseExitMenuItem(Widget, XtPointer, XEvent *, Boolean *);
void mouseEnterMenuItem(Widget, XtPointer, XEvent *, Boolean *);

Cursor		WaitCursor;
Cursor		ResizeCursor;

namespace
{
    IPCPartition    * partition;
    const XmString  null = XmStringCreate ( (char*)"", XmFONTLIST_DEFAULT_TAG );
}

char * ListGetSelectedText( Widget w )
{
    int row = XbaeMatrixFirstSelectedRow( w );
    if( row != -1 )
  	return XbaeMatrixGetCell( w, row, 0 );

    return 0;
}

void destroyInfoWindow( void * param )
{
    InfoDialog * info = (InfoDialog*)param;
    delete info;
}

void buttonClosePressed(Widget , XtPointer ptr, XtPointer )
{
    boost::thread( boost::bind( destroyInfoWindow, ptr ) );
}

void buttonExitPressed(Widget, XtPointer, XtPointer)
{
    exit(0);
}

void buttonShowPressed(Widget , XtPointer, XtPointer)
{
    char * server_name = ListGetSelectedText( wServerList );
        
    if ( !server_name )
	return;
        
    InfoDialog * info = new InfoDialog( *partition, server_name );
    create_wInfoDialog( wMainApp, (XtPointer)info );    
}

void yesPressed( Widget , XtPointer ptr, XtPointer cb )
{
    InfoDialog * info = (InfoDialog*)ptr;
    
    info->RemoveSelectedInfo( );
    
    if ( cb != 0 )
    {
    	XtUnmanageChild( wConfirmationDialog );
	XtDestroyWidget( wConfirmationDialog );
    }
}

void noPressed(Widget , XtPointer , XtPointer)
{
    XtUnmanageChild( wConfirmationDialog );
    XtDestroyWidget( wConfirmationDialog );
}

void buttonDeleteInfoPressed(Widget , XtPointer ptr, XtPointer )
{
    InfoDialog * info = (InfoDialog*)ptr;
    info->AskConfirmation( );
}

void buttonDeleteInfoPressed(Widget w, XEvent *, String*, Cardinal *)
{
    InfoDialog * info;
    XtVaGetValues( XtParent( w ), XmNuserData, &info, NULL );
    yesPressed( w, info, 0 );
}

void aknowledgePressed(Widget , XtPointer , XtPointer)
{
    XtUnmanageChild( wErrorDialog );
    XtDestroyWidget( wErrorDialog );
}

void buttonSendCommandPressed(Widget , XtPointer ptr, XtPointer )
{
    InfoDialog * info = (InfoDialog*)ptr;
    info->SendCommand();
}

void valueChangedCallback( Widget w, XtPointer user, XtPointer )
{
    int column = 0;
    XtVaGetValues( w, XmNuserData, &column, NULL );
    char * value = 0;
    XtVaGetValues( w, XmNvalue, &value, NULL );
    InfoDialog * info = (InfoDialog *)user;
    info->SetFilter( column, value );
    free( value );
}


///////////////////////////////////////////////////////////////////////////////////////////
//
// Functions specific to the main dialog window
// Should be moved to a new class sometimes
//
///////////////////////////////////////////////////////////////////////////////////////////

void SetServerNumberToWidget( Widget w, int number )
{
    char    string[32];
    sprintf( string, "%d server%s", number, number != 1 ? "s" : "" ); 

    XmString ss = XmStringCreate ( string, XmFONTLIST_DEFAULT_TAG );
    XtVaSetValues( w, XmNlabelString, ss, NULL );
    XmStringFree( ss );
}

int ListAddServers( IPCPartition & p )
{
    XbaeMatrixDeselectAll( wServerList );
    XbaeMatrixDeleteRows( wServerList, 0, XbaeMatrixNumRows( wServerList ) );
                    
    int row = 0;
    ISServerIterator isi(p);
    XtVaSetValues( wServerList, XmNrows, isi.entries(), NULL );
    
    while ( isi() )
    {
        std::string time = isi.time().str();
        XbaeMatrixSetCell( wServerList, row, 0, (char*)isi.name() );
        XbaeMatrixSetCell( wServerList, row, 1, (char*)time.c_str() );
        XbaeMatrixSetCell( wServerList, row, 2, (char*)isi.host() );
        XbaeMatrixSetCell( wServerList, row, 3, (char*)isi.owner() );
        
        char buf[16];
        sprintf( buf, "%d", isi.pid() );
        XbaeMatrixSetCell( wServerList, row, 4, buf );
        row++;
    }
    
    return row;
}

void partitionMenuEntryCallback(Widget, XtPointer , XtPointer xtp)
{
    XmString	xmstring;
    char *	string;
    XmRowColumnCallbackStruct *     rcb = (XmRowColumnCallbackStruct*)xtp;
 
    XtVaGetValues( rcb->widget, XmNlabelString, &xmstring, NULL );
    XmStringGetLtoR( xmstring, XmFONTLIST_DEFAULT_TAG, &string );
    XmStringFree( xmstring );
        
    delete partition;
    if ( !strcmp(string,ipc::partition::default_name) )
	partition = new IPCPartition( );
    else
	partition = new IPCPartition( string );
        
    int num = ListAddServers( *partition );
    
    SetServerNumberToWidget( wMainInfo2, num );

    XtFree( string );
    XtVaSetValues( wButtonShow, XmNsensitive, FALSE, NULL );
}

void selectServerListCallback(Widget w, XtPointer , XtPointer call )
{
    XbaeMatrixSelectCellCallbackStruct *cbs = (XbaeMatrixSelectCellCallbackStruct*) call;
    int row = XbaeMatrixFirstSelectedRow( w );
    
    if ( row != -1 )
	XbaeMatrixDeselectRow( w, row );
    XbaeMatrixSelectRow( w, cbs->row );
    XtVaSetValues( wButtonShow, XmNsensitive, TRUE, NULL );
}

void selectValueCellCallback(Widget w, XtPointer user, XtPointer call)
{
    XbaeMatrixSelectCellCallbackStruct *cbs = (XbaeMatrixSelectCellCallbackStruct*) call;

    XbaeMatrixUnhighlightAll( w );
    XbaeMatrixHighlightCell( w, cbs->row, cbs->column );
    XbaeMatrixRefresh( w );
    
    InfoDialog * info = (InfoDialog*)user;
    info->DisplayAttributeValue( cbs->row, cbs->column );
}

void partitionMenuActivateCallback( Widget , XtPointer , XtPointer )
{
    int		num;
    Widget *	buttons;
        
    XtVaGetValues( wPartitionMenu, XmNnumChildren, &num, NULL );
    XtVaGetValues( wPartitionMenu, XmNchildren, &buttons, NULL );
    
    for ( int i = 0; i < num; i++ )
    {
	XtUnmanageChild( buttons[i] );
	XtDestroyWidget( buttons[i] );        
    }
    
    std::list< IPCPartition > pl;
    IPCPartition::getPartitions( pl );
    
    IPCPartition p;
    if ( p.isValid() )
        pl.push_back( p );
        
    for ( std::list< IPCPartition >::iterator pi = pl.begin(); pi != pl.end(); pi++ )
    {
        int ac = 0;
        Arg al[64];
        XmString xmstring;
        
        XtSetArg(al[ac], XmNshadowThickness, 0); ac++;
        xmstring = XmStringCreate ( (char*)pi->name().c_str(), XmFONTLIST_DEFAULT_TAG );
        XtSetArg(al[ac], XmNlabelString, xmstring); ac++;
	XtSetArg(al[ac], XmNmarginLeft, 5); ac++;
	XtSetArg(al[ac], XmNmarginRight, 5); ac++;
	XtSetArg(al[ac], XmNmarginHeight, 4); ac++;
        Widget button = XmCreatePushButton ( wPartitionMenu, (char*)pi->name().c_str(), al, ac );
	XtAddEventHandler(button, EnterWindowMask, False, mouseEnterMenuItem, (XtPointer) 0) ;
	XtAddEventHandler(button, LeaveWindowMask, False, mouseExitMenuItem, (XtPointer) 0) ;
        XmStringFree ( xmstring );
        XtManageChild ( button );
    }
}

void mouseExit( Widget w, XtPointer ptr, XEvent *, Boolean *)
{    
    InfoDialog * info = (InfoDialog*)ptr;
    
    int ml, mr;
    XtVaGetValues(w, XmNmarginLeft, &ml, XmNmarginRight, &mr, NULL);

    XtVaSetValues(w, XmNshadowThickness, 0, XmNmarginLeft, ml+1, XmNmarginRight, mr+1, NULL );
    
    Widget wtip = info ? info->GetLogWidget() : wMainInfo1;
    
    XtVaSetValues( wtip, XmNlabelString, null, NULL );
}

void mouseEnter( Widget w, XtPointer ptr, XEvent *, Boolean *)
{
    InfoDialog * info = (InfoDialog*)ptr;
    
    int ml, mr;
    XtVaGetValues(w, XmNmarginLeft, &ml, XmNmarginRight, &mr, NULL);

    XtVaSetValues(w, XmNshadowThickness, 1, XmNmarginLeft, ml-1, XmNmarginRight, mr-1, NULL );
    
    Widget wtip = info ? info->GetLogWidget() : wMainInfo1;
    XmString ss;
    XtVaGetValues( w, XmNuserData, &ss, NULL );
    XtVaSetValues( wtip, XmNlabelString, ss, NULL );
}

void mouseExitMenu( Widget menu, XtPointer , XEvent *, Boolean *)
{    
    Widget w = XmOptionButtonGadget ( menu );

    XtVaSetValues( w, XmNshadowThickness, 0, NULL );
    XtVaSetValues( wMainInfo1, XmNlabelString, null, NULL );
}

void mouseEnterMenu( Widget menu, XtPointer , XEvent *, Boolean *)
{
    Widget w = XmOptionButtonGadget ( menu );

    XtVaSetValues( w, XmNshadowThickness, 1, NULL );
    
    XmString ss;
    XtVaGetValues( menu, XmNuserData, &ss, NULL );
    XtVaSetValues( wMainInfo1, XmNlabelString, ss, NULL );
}

void mouseExitMenuItem( Widget w, XtPointer, XEvent *, Boolean * )
{
    Display * dpy = XtDisplay( w );
    XtVaSetValues(w, XmNforeground, BlackPixel(dpy, DefaultScreen( dpy ) ), NULL );
}

void mouseEnterMenuItem( Widget w, XtPointer, XEvent *, Boolean * )
{
    Display * dpy = XtDisplay( w );
    XtVaSetValues(w, XmNforeground, WhitePixel(dpy, DefaultScreen( dpy ) ), NULL );
}

namespace ismon
{
    Pixel 	ErrorColor;
    Pixel 	LineColor;
    Pixel 	OddBgColor;
    Pixel 	EvenBgColor;
    Pixel 	BgColor;
    Pixel 	TextBgColor;
    Pixel 	TextColor;
    XmFontList	TableCaptionFont;
}

void SetupFonts( Display * dpy )
{
        XFontStruct * font = XLoadQueryFont( dpy, "*adobe*helvetica*bold*r*normal*10*" );
        if ( font )
        {
	    ismon::TableCaptionFont = XmFontListCreate( font, XmSTRING_DEFAULT_CHARSET );
        }
}

void SetupColors( Display * dpy )
{
    int 	scr = DefaultScreen( dpy );
    Colormap    cmap = DefaultColormap( dpy, scr );
    XColor	error_color, odd_color, even_color, line_color, bg_color, text_bg_color, text_color;
    
    error_color.red = 40000;
    error_color.green = 0;
    error_color.blue = 0;
    
    odd_color.red = 61000;
    odd_color.green = 61000;
    odd_color.blue = 61000;

    even_color.red = 65000;
    even_color.green = 65000;
    even_color.blue = 65000;

    line_color.red = 50000;
    line_color.green = 50000;
    line_color.blue = 50000;

    bg_color.red = 57000;
    bg_color.green = 57000;
    bg_color.blue = 57000;

    text_bg_color.red = 56320;
    text_bg_color.green = 56320;
    text_bg_color.blue = 65000;

    text_color.red = 39400;
    text_color.green = 17400;
    text_color.blue = 17400;

    if ( XAllocColor( dpy, cmap, &error_color ) )
    	ismon::ErrorColor = error_color.pixel;
    else
    	ismon::ErrorColor = BlackPixel(dpy, scr);
    
    if ( XAllocColor( dpy, cmap, &odd_color ) )
    	ismon::OddBgColor = odd_color.pixel;
    else
    	ismon::OddBgColor = WhitePixel(dpy, scr);
    
    if ( XAllocColor( dpy, cmap, &even_color ) )
    	ismon::EvenBgColor = even_color.pixel;
    else
    	ismon::EvenBgColor = WhitePixel(dpy, scr);
    
    if ( XAllocColor( dpy, cmap, &line_color ) )
    	ismon::LineColor = line_color.pixel;
    else
    	ismon::LineColor = BlackPixel(dpy, scr);
    
    if ( XAllocColor( dpy, cmap, &bg_color ) )
    	ismon::BgColor = bg_color.pixel;
    else
    	ismon::BgColor = WhitePixel(dpy, scr);
    
    if ( XAllocColor( dpy, cmap, &text_bg_color ) )
    	ismon::TextBgColor = text_bg_color.pixel;
    else
    	ismon::TextBgColor = WhitePixel(dpy, scr);
    
    if ( XAllocColor( dpy, cmap, &text_color ) )
    	ismon::TextColor = text_color.pixel;
    else
    	ismon::TextColor = WhitePixel(dpy, scr);
}

int main (int argc, char **argv)
{    
    IPCCore::init( argc, argv );
    partition = new IPCPartition();
    
    XInitThreads();
    XtToolkitThreadInitialize();
    XtToolkitInitialize();
    
    XtAppContext app_context = XtCreateApplicationContext();
    Display * display = XtOpenDisplay( app_context, NULL, argv[0], "ISMonitor", NULL, 0, &argc, argv);
    ResizeCursor = XCreateFontCursor( display, XC_sb_h_double_arrow);
    WaitCursor = XCreateFontCursor( display, XC_watch);
    
    if (!display)
    {
	std::cerr << "ERROR :: can't open display, exiting..." << std::endl;
	exit (-1);
    }

    SetupColors( display );
    SetupFonts( display );
    
    XtActionsRec actions[] = {  {(char*)"MouseMotionOverTable", MouseMotionOverTable},
    				{(char*)"buttonDeleteInfoPressed", buttonDeleteInfoPressed},
                             };
    
    XtAppAddActions( app_context, actions, 2 );
    create_wMainApp( display, argv[0], argc, argv );
    
    XtRealizeWidget( wMainApp );
    XtAppMainLoop( app_context );
    
    return 0;
}
