#include <is/infodocument.h>
#include <is/infoiterator.h>

#include <infodialog.h>

#include <Xm/Matrix.h>
#include <Xm/TextF.h>

#include <ismonitor.h>

extern Widget wValueTable;
extern Widget wInfoList;
extern Cursor WaitCursor;
extern Cursor ResizeCursor;
extern char * ListGetSelectedText( Widget w );
extern void print_value ( Widget w, IPCPartition & p, ISType & last_type, ISInfoAny& isa );
extern void AddSearchFields( Widget w, int columns, XtPointer info );

const unsigned char	InfoDialog::mask_impl_1[] = { 255, 255, 255, 0 };
const unsigned char	InfoDialog::mask_impl_2[] = { 1, 1, 1, 0 };
const long 		InfoDialog::mask1 = *((long*)mask_impl_1);
const long 		InfoDialog::mask2 = *((long*)mask_impl_2);

namespace
{
    const XmString  null = XmStringCreate ( (char*)"", XmFONTLIST_DEFAULT_TAG );
}
    
bool operator<( const ISType & t1, const ISType & t2 )
{
    return ( t1 != t2 );
}

int get_row(const char * str)
{
    int value;
    sscanf( str, "%d", &value );
    return value;
}

char * put_row(int v)
{
    std::string s(boost::lexical_cast<std::string>(v));
    char * r = (char *)malloc(s.size()+1);
    ::strcpy( r, s.c_str() );
    return r;
}

void GetHighlitedCell( Widget w, int * row, int * column )
{
    unsigned char ** highlited_cells;
    XtVaGetValues( w, XmNhighlightedCells, &highlited_cells, NULL );
    
    *row = *column = -1;
    if ( !highlited_cells )
    	return;
        
    int rows, columns;
    XtVaGetValues( w, XmNrows, &rows, XmNcolumns, &columns, NULL );
    for (int i = 0; i < rows; i++)
        for (int j = 0; j < columns; j++)
            if (highlited_cells[ i ][ j ])
            {
                *row = i;
                *column = j;
                return;
            }
}

void selectInfoListCallback(Widget w, XtPointer user, XtPointer call)
{
    static XrmQuark QUp = XrmStringToQuark("Up");
    static XrmQuark QDown = XrmStringToQuark("Down");
    static XrmQuark QPageUp = XrmStringToQuark("PageUp");
    static XrmQuark QPageDown = XrmStringToQuark("PageDown");
    
    XbaeMatrixSelectCellCallbackStruct *cbs = (XbaeMatrixSelectCellCallbackStruct*) call;
    int row = XbaeMatrixFirstSelectedRow( w );
    int new_selection = cbs->row;
    
    int rows;
    XtVaGetValues( w, XmNrows, &rows, NULL );
    int fixed_rows = 0;
    XtVaGetValues( w, XmNfixedRows, &fixed_rows, NULL );
    int visible_rows = 0;
    XtVaGetValues( w, XmNvisibleRows, &visible_rows, NULL );
    if ( cbs->num_params )
    {
    	XrmQuark quark = XrmStringToQuark( cbs->params[0] );
        if ( quark == QUp )
        {
            new_selection = ( row > fixed_rows ) ? row - 1 : row;
        }
        else if ( quark == QDown )
        {
            new_selection = ( row < rows - 1 ) ? row + 1 : row;
        }
        else if ( quark == QPageUp )
        {
            new_selection = ( row - visible_rows ) < fixed_rows ? fixed_rows : row - visible_rows;
        }
        else if ( quark == QPageDown )
        {
            new_selection = ( row + visible_rows > rows - 1 ) ? rows - 1 : row + visible_rows;
        }
    }

    if ( row != new_selection && new_selection >= fixed_rows && new_selection < rows )
    {
	if ( row != -1 ) XbaeMatrixDeselectRow( w, row );
        
	XbaeMatrixSelectRow( w, new_selection );
	((InfoDialog*)user)->SelectInfo( new_selection );
	XbaeMatrixRefresh( w );
    }
}

const char * mystristr(const char *haystack, const char *needle)
{
    if ( !*needle )
    {
	return haystack;
    }
    
    for ( ; *haystack; ++haystack )
    {
	if ( ::toupper(*haystack) == ::toupper(*needle) )
	{
	     /*
	      * Matched starting char -- loop through remaining chars.
	      */
	    const char *h, *n;
	    for ( h = haystack, n = needle; *h && *n; ++h, ++n )
	    {
		if ( ::toupper(*h) != ::toupper(*n) )
		{
		    break;
		}
	    }
	    if ( !*n ) /* matched all of 'needle' to null termination */
	    {
		return haystack; /* return the start of the match */
	    }
	}
    }
    return 0;
}

long InfoDialog::hash( const char * s )
{
    long h = 0;
    for( ; *s; ++s )
    {
    	h = 17*h + 13*(*s) + *s;
    }
    return ( ( h & mask1 ) | mask2 );
}

InfoDialog::InfoDialog( const IPCPartition & pp, const char * server )
  : rows_total( 0 ),
    rows_selected( 0 ),
    selected_row(-1),
    partition( pp ),
    dictionary( pp ),
    receiver( pp ),
    server_name( server ),
    uqueue( new UpdateQueue( *this, wInfoList ) )
{
    create_wInfoDialog( wMainApp, this );
    shell = wInfoDialog;
    table = wInfoList;
    value_table = wValueTable;
    value = wValueText;
    log1 = wInfo1;
    log2 = wInfo2;
    log3 = wInfo3;
    log4 = wInfo4;
    delete_button = wButtonDeleteInfo;
    exit_button = wButtonClose;
    send_button = wButtonSendCommand;
    command_field = wCommand;

    XtVaSetValues( XtParent( delete_button ), XmNuserData, this, NULL );

    char title[1024];
    sprintf( title, "Partition '%s', server '%s'", partition.name().c_str(), server );
    XtVaSetValues( shell, XmNtitle, title, NULL);
    XtManageChild( shell );

    fillInfoList();
}

InfoDialog::~InfoDialog() {
    try {
	receiver.unsubscribe( server_name, ISCriteria(std::string( ".*" )), true );
    }
    catch( daq::is::Exception & ex )
    { }
    
    delete uqueue;
    
    XtDestroyWidget( shell );
}

void 
InfoDialog::SetLogText( int number, int val1, int val2 )
{
    Widget log;
    char    string[64];
    switch( number )
    {
    	case 1:
	    sprintf( string, "reading %d of %d object%s", val1, val2, val2 != 1 ? "s" : "" );
            log = log1;
            break;
    	case 2:
	    sprintf( string, "%d object%s", val1, val2 != 1 ? "s" : "" ); 
            log = log2;
            break;
    	case 3:
	    sprintf( string, "%d object%s selected", val1, val1 > 1 ? "s" : "" ); 
            log = log3;
            break;
    	case 4:
	    sprintf( string, "%d attribute%s", val1, val1 > 1 ? "s" : "" ); 
            log = log4;
            break;
        default:
            return;
    }
    
    XmString ss = XmStringCreate ( string, XmFONTLIST_DEFAULT_TAG );
    XtVaSetValues( log, XmNlabelString, ss, NULL );
    XmStringFree( ss );
}

void 
InfoDialog::SetFilter( int column, const char * filter )
{
    if ( !filter || column < 0 || column > 3 )
    {
    	return;
    }
    
    filters[column] = filter;
    
    String ** cells;
    XtVaGetValues( table, XmNcells, &cells, NULL );
    for ( int i = 1; i <= rows_total; i++ )
    {
        if (		mystristr( cells[i][0], filters[0].c_str() )
		&&	mystristr( cells[i][1], filters[1].c_str() )
		&&	mystristr( cells[i][2], filters[2].c_str() )
		&&	mystristr( cells[i][3], filters[3].c_str() )
	   )
	{
	    if ( !XbaeMatrixGetRowHeight( table, i ) )
	    {
		XbaeMatrixSetRowHeightEx( table, i, -1 );
		rows_selected++;
	    }
	}
	else
	{
	    if ( XbaeMatrixGetRowHeight( table, i ) )
	    {
		XbaeMatrixSetRowHeightEx( table, i, 0 );
		rows_selected--;
	    }
	}
    }

    XbaeMatrixSetRowHeight( table, 0, -1 );
    SetLogText( 3, rows_selected );
}

void 
InfoDialog::ApplyFilters( int row )
{
    if ( row < 0 || row > rows_total )
    	return;
        
    String ** cells;
    XtVaGetValues( table, XmNcells, &cells, NULL );
    bool changed = false;
    if (	    mystristr( cells[row][0], filters[0].c_str() )
	    &&	    mystristr( cells[row][1], filters[1].c_str() )
	    &&	    mystristr( cells[row][2], filters[2].c_str() )
	    &&	    mystristr( cells[row][3], filters[3].c_str() )
       )
    {
	if ( !XbaeMatrixGetRowHeight( table, row ) )
        {
	    XbaeMatrixSetRowHeightEx( table, row, -1 );
	    rows_selected++;
            changed = true;
        }
    }
    else
    {
        if ( XbaeMatrixGetRowHeight( table, row ) )
        {
	    XbaeMatrixSetRowHeightEx( table, row, 0 );
	    rows_selected--;
            changed = true;
        }
    }
    
    if ( changed )
    {
	XbaeMatrixSetRowHeight( table, 0, -1 ); // force recalculation of heights of all rows
	SetLogText( 3, rows_selected );
    }
}

void 
InfoDialog::callback( ISCallbackEvent * isi )
{    
    if ( isi->reason() == ISInfo::Created )
    {
        uqueue -> add( isi->objectName(), isi->time(), isi->type() );
    }
    else if ( isi->reason() == ISInfo::Deleted )
    {                    
        uqueue -> add( isi->objectName() );
    }
    else if ( isi->reason() == ISInfo::Updated )
    {
        uqueue -> add( isi->objectName(), isi->time() );
    }        
}

void 
InfoDialog::AddInfo( const std::string & name, const OWLTime & time, const ISType & type )
{
    std::string descr;
    try {
        descr = ISInfoDocument( partition, type ).description();
    }
    catch( daq::is::Exception & ex ) { }
    
    std::unique_lock <std::mutex> lock(m_mutex);

    rows_total++;
    rows_selected++;
    std::string tt = time.str();
    std::string rr = boost::lexical_cast<std::string>( rows_total );
    char * row[] = {	(char*)name.c_str(), 
			(char*)type.name().c_str(),
			(char*)tt.c_str(),
			(char*)descr.c_str(),
                        (char*)rr.c_str() };

    XbaeMatrixAddRows( table, rows_total, row, 0, 0, 1 );
    XbaeMatrixSetRowHeight( table, rows_total, -1 );
    ApplyFilters( rows_total );
    SetLogText( 2, rows_total );
    
    String ** cells;
    XtVaGetValues( table, XmNcells, &cells, NULL );
    map.insert( cells[rows_total] );
}

void
InfoDialog::UpdateInfo( const std::string & name, const OWLTime & time )
{
    const char * s = name.c_str();
    TableMap::iterator it = map.find(&s);
    if ( it == map.end() )
    	return;
        
    std::unique_lock <std::mutex> lock(m_mutex);
    String ** cells;
    XtVaGetValues( table, XmNcells, &cells, NULL );

    Row row = *it;
    XtFree( row[2] );
    row[2] = time.c_str();

    int i = get_row( row[4] );
    XbaeMatrixRefreshCell( table, i, 2 );
    ApplyFilters( i );
    XFlush( XtDisplay( table ) );

    if ( selected_row == i )
    {
	int vrow, vcolumn;
	GetHighlitedCell( value_table, &vrow, &vcolumn );

	ISInfoAny isa;
	try
	{
	    dictionary.getValue( server_name + "." + name, isa );
	    print_value( value_table, partition, last_type, isa );
	}
	catch( daq::is::Exception & ex )
	{
	    ers::error( ex );
	}

	if ( vrow != -1 && vcolumn != -1 )
	{
	    char * attr = XbaeMatrixGetCell( value_table, vrow, vcolumn );
	    XtVaSetValues( value, XmNvalue, attr, NULL );
	}
	XFlush( XtDisplay( value_table ) );
    }
}

void
InfoDialog::RemoveInfo( const std::string& name )
{
    const char * s = name.c_str();

    std::unique_lock <std::mutex> lock(m_mutex);
    TableMap::iterator it = map.find(&s);
    if ( it == map.end() )
    	return;
    
    int deleted_row = get_row( (*it)[4] );
    
    bool was_selected = (selected_row == deleted_row);

    map.erase(it);

    if ( XbaeMatrixGetRowHeight( table, deleted_row ) )
    {
	rows_selected--;
	SetLogText( 3, rows_selected );
    }
    XbaeMatrixDeleteRows( table, deleted_row, 1 );
    XbaeMatrixRefresh( table );

    if ( was_selected )
    {
	XbaeMatrixDeleteRows( value_table, 0, XbaeMatrixNumRows( value_table ) );
	SetLogText( 4, 0 );
	XtVaSetValues( delete_button, XmNsensitive, FALSE, NULL );
	XtVaSetValues( send_button, XmNsensitive, FALSE, NULL );
	last_type = ISType::Null;
            
	XbaeMatrixSelectCellCallbackStruct cbs;
	cbs.row = deleted_row;
	cbs.num_params = 0;
	selectInfoListCallback( table, this, &cbs );
    }
        
    if (rows_total > deleted_row) 
    {
	// update row numbers
	String ** cells;
	XtVaGetValues( table, XmNcells, &cells, NULL );
    
	XtFree( cells[rows_total-1][4] );
	for (int i = rows_total-1; i > deleted_row; --i)
	{
	    cells[i][4] = cells[i-1][4];
	}
	cells[deleted_row][4] = put_row(deleted_row);
    }    

    SetLogText( 2, --rows_total );
}

int 
InfoDialog::readInfo( )
{                    
    ISInfoIterator    isi( partition, server_name, ISCriteria(std::string(".*")) );
    
    std::map<ISType,std::string> type_descriptions;

    XbaeMatrixAddRows( table, 1, 0, 0, 0, isi.entries() );

    String ** cells;
    XtVaGetValues( table, XmNcells, &cells, NULL );
    int row = 1;
    while ( isi() )
    {
        const char * description = "";
        const char * type_name = "";
        ISType type = isi.type();
        std::map<ISType,std::string>::iterator it = type_descriptions.find( type );
        if ( it == type_descriptions.end() )
        {
	    try {
            	ISInfoDocument doc( partition, type );
                it = type_descriptions.insert( std::make_pair( type, doc.description() ) ).first;
            }
            catch( daq::is::NotFound & )
            {
                it = type_descriptions.insert( std::make_pair( type, "" ) ).first;
            }
        }
	description = it->second.c_str();
	type_name = it->first.name().c_str();
        bool description_found = strlen(description);

        XtFree(cells[row][0]);
        cells[row][0] = ::strdup(isi.objectName().c_str());
        XtFree(cells[row][1]);
	cells[row][1] = ::strdup(type_name);
        XtFree(cells[row][2]);
	cells[row][2] = isi.time().c_str(); 	// this function allocates memory
	if ( description_found )
        {
	    XtFree(cells[row][3]);
	    cells[row][3] = ::strdup(description);
        }
	XtFree(cells[row][4]);
	cells[row][4] = put_row(row);
	map.insert( cells[row++] );        
    }

    SetLogText( 1, isi.entries(), isi.entries() );
    
    XtVaSetValues( log1, XmNlabelString, null, NULL );
        
    return isi.entries();
}

void 
InfoDialog::fillInfoList( )
{
    XDefineCursor( XtDisplay(shell), XtWindow(shell), WaitCursor );
    XFlush( XtDisplay( shell ) );

    try {
	rows_total = rows_selected = readInfo(  );
    }
    catch( daq::is::Exception & ex ) {
        ers::error( ex );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::error( ex );
    }
    
    SetLogText( 2, rows_total );
        
    try {
	receiver.subscribe( server_name, ISCriteria( ".*" ), &InfoDialog::callback, this );
    }
    catch( daq::is::Exception & ex )
    { }

    XUndefineCursor( XtDisplay(shell), XtWindow(shell) );
    XtVaSetValues( exit_button, XmNsensitive, TRUE, NULL );
}

void
InfoDialog::SendCommand( )
{
    char * object_name = ListGetSelectedText( table );

    if ( !object_name )
	return;

    std::string info_name = server_name + "." + object_name;
    
    char * command = XmTextFieldGetString( command_field );
    
    try
    {
	ISInfoAny any;       
    	dictionary.getValue( info_name, any );
        any.sendCommand( command );
    }
    catch( daq::is::Exception & ex )
    {    
        std::ostringstream err;
        err << "Sending command '" << command << "' to the provider of the '" << info_name << "' info failed : ";
        std::ostringstream diag;
        diag << ex.message();
        std::string errmsg = err.str();
        std::string diagmsg = diag.str();
        
	create_wErrorDialog( shell, this );
	XmString errstring = XmStringCreate ( (char*)errmsg.c_str(), XmFONTLIST_DEFAULT_TAG );
	XmString diagstring = XmStringCreate ( (char*)diagmsg.c_str(), XmFONTLIST_DEFAULT_TAG );
	XtVaSetValues( wErrorText, XmNlabelString, errstring, NULL );
	XtVaSetValues( wDiagText, XmNlabelString, diagstring, NULL );
	XmStringFree( errstring );
	XmStringFree( diagstring );
	XtManageChild( wErrorForm );
	XtManageChild( wErrorDialog );    
    }
    XtFree( command );
}

void
InfoDialog::SelectInfo( int row )
{
    char * object_name = ListGetSelectedText( table );

    if ( !object_name ) {
	selected_row = -1;
        return;
    }
    
    selected_row = row;
    
    std::string info_name = server_name + "." + object_name;
                        
    XtVaSetValues( delete_button, XmNsensitive, TRUE, NULL );
    XtVaSetValues( send_button, XmNsensitive, TRUE, NULL );
        
    try {
	ISInfoAny isa;
	dictionary.getValue(info_name,isa);
	print_value( value_table, partition, last_type, isa );
	SetLogText( 4, XbaeMatrixNumRows( value_table ) );
    }
    catch( daq::is::Exception & ex ) {
    	ers::error( ex );
    }
}

void 
InfoDialog::RemoveSelectedInfo( )
{
    char * object_name = ListGetSelectedText( table );

    if ( !object_name )
	return;

    std::string info_name = server_name + "." + object_name;
	
    XtVaSetValues( delete_button, XmNsensitive, FALSE, NULL );
    XtVaSetValues( send_button, XmNsensitive, FALSE, NULL );
        
    try {
    	dictionary.remove( info_name );
    }
    catch( daq::is::Exception & ex )
    {
	ers::error( ex );
	XtVaSetValues( delete_button, XmNsensitive, TRUE, NULL );
	XtVaSetValues( send_button, XmNsensitive, TRUE, NULL );
    }
}

void 
InfoDialog::AskConfirmation( )
{
    char * name = ListGetSelectedText( table );
    
    if ( !name )
	return;
        
    std::string ss = "Are you sure you want to delete '";
    ss += server_name + ".";
    ss += name;
    ss += "' information from the IS repository?";
        
    create_wConfirmationDialog( shell, this );
    
    XmString string = XmStringCreate ( (char*)ss.c_str(), XmFONTLIST_DEFAULT_TAG );
    XtVaSetValues( wConfirmationText, XmNlabelString, string, NULL );
    XmStringFree( string );
    XtManageChild( wDialogControl );
    XtManageChild( wConfirmationDialog );
}

void
InfoDialog::DisplayAttributeValue( int row, int column )
{
    char * text = XbaeMatrixGetCell( value_table, row, column );
    XtVaSetValues( value, XmNvalue, text, NULL );
}
