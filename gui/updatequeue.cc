#include <infodialog.h>
#include <updatequeue.h>
#include <ismonitor.h>

UpdateQueue::UpdateQueue( InfoDialog & dialog, Widget table )
  : m_dialog( dialog ),
    m_table( table ),
    m_terminated( false ),
    m_thread( std::bind( &UpdateQueue::thread_wrapper, this ) )
{ ; }

UpdateQueue::~UpdateQueue( )
{
    {
	std::unique_lock lock( m_mutex );
	m_terminated = true;
	m_condition.notify_one();
    }
    m_thread.join();
}

void
UpdateQueue::thread_wrapper()
{
    std::unique_lock lock( m_mutex );
    while( !m_terminated )
    {
	m_condition.wait( lock );
	
        while( !m_terminated )
        {
	    if ( m_updates.empty() )
		break;

	    Task udata = m_updates.front();
	    m_updates.pop();

            lock.unlock();
            udata();
            lock.lock();
        }
    }
}

void 
UpdateQueue::add( const std::string & name, const OWLTime & time )
{
    std::unique_lock lock( m_mutex );
    if ( m_terminated )
    	return;
    m_updates.push( std::bind( &InfoDialog::UpdateInfo, &m_dialog, name, time ) );
    m_condition.notify_one();
}

void 
UpdateQueue::add( const std::string & name )
{
    std::unique_lock lock( m_mutex );
    if ( m_terminated )
    	return;
    m_updates.push( std::bind( &InfoDialog::RemoveInfo, &m_dialog, name ) );
    m_condition.notify_one();
}

void 
UpdateQueue::add( const std::string & name, const OWLTime & time, const ISType & type )
{
    std::unique_lock lock( m_mutex );
    if ( m_terminated )
    	return;
    m_updates.push( std::bind( &InfoDialog::AddInfo, &m_dialog, name, time, type ) );
    m_condition.notify_one();
}
