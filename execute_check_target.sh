#!/bin/sh
#################################################################
#
#	Check target script for the IS package
#	Created by Sergei Kolos; 26.02.02
#
#################################################################

test_script_result()
{
  if test $? -ne 0
  then
	echo "*************** Check target failed ***************"
	ipc_rm -i ".*" -n ".*" -t -f
	exit 1
  fi
}

$1test/run.sh $2 $3 $1
test_script_result

if test $4 -a '$5' -a $6	# Java supported platform
then
	$1examples/run.sh $4 "$5" $6
	test_script_result
	$1jtest/run.sh "$5" $6
	test_script_result
fi

echo "*************** Check target finished successfuly ***************"
ipc_rm -i ".*" -n ".*" -t -f
echo "done."
