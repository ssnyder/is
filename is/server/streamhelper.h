#ifndef IS_STREAM_HELPER_H
#define IS_STREAM_HELPER_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/server/streamhelper.h
//
//      private header file for the IS library
//
//      Sergei Kolos December 2001
//
//      description:
//              Class ISStreamHelper the IS info iteration facility
//////////////////////////////////////////////////////////////////////////////////////
#include <atomic>
#include <condition_variable>
#include <string>
#include <thread>

#include <boost/noncopyable.hpp>

#include <owl/regexp.h>

#include <ipc/object.h>

#include <is/is.hh>
#include <is/server/criteriahelper.h>
#include <is/server/infoholder.h>

//////////////////////////
// Forward declaration
//////////////////////////
class ISRepository;

class ISStreamHelper: boost::noncopyable
{
public:
    ISStreamHelper(ISRepository & repository,
	const is::criteria & criteria,
	is::stream_ptr stream,
	size_t history_depth,
	is::sorted order);

private:
    void fill();
    void send();
    size_t push(const boost::shared_ptr<ISInfoHolder> & info);

    static const size_t m_max_length;

    ISRepository & m_repository;
    ISCriteriaHelper m_criteria;
    is::stream_var m_stream;
    const size_t m_history_depth;
    is::sorted m_history_sort_order;
    bool m_error;
    std::atomic_uint m_entries;
    is::info_list m_buffers[2];
    bool m_fill_buffer;
    bool m_send_buffer;
    bool m_ready_to_send;
    bool m_done;
    std::condition_variable m_condition;
    std::mutex m_mutex;
};

#endif
