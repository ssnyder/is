#ifndef IS_SERVER_MIRROR_H
#define IS_SERVER_MIRROR_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      mirror.h
//
//      private header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		declaration of the ISReceiver class
//////////////////////////////////////////////////////////////////////////////////////
#include <string>

#include <ipc/partition.h>
#include <is/server/informer.h>

////////////////////////////////////////////////////////////////////////////////////////////////
// Class ISMirror defines the reference holder for the mirror IS server callback
////////////////////////////////////////////////////////////////////////////////////////////////

struct ISMirror : public ISInformer
{
    ISMirror(const IPCPartition & partition, const std::string & server_name);

    void push(const is::info & i, is::reason r)
    {
	inform(m_mirror, i, r);
    }

private:
    void connect();

    bool failed();

private:
    IPCPartition		m_partition;
    is::info_callback_var	m_mirror;
};

#endif
