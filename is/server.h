#ifndef IS_SERVER_H
#define IS_SERVER_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/server.h
//
//      public header file for the IS library
//
//      Sergei Kolos October 2004
//
//      description:
//              Provides macro for handling remote invocation exceptions.
//////////////////////////////////////////////////////////////////////////////////////
#include <is/info.h>
#include <is/exceptions.h>

namespace is {
    namespace server {    
	void
        split_name(	const std::string & name, 
		 	std::string & server_name, 
                 	std::string & object_name );
                 
        is::repository_var
	resolve( const IPCPartition& p, 
        	 const std::string & name );
	
        bool 
        exist(	const IPCPartition & partition, 
        	const std::string & name );
    }
}

#define __TRY_INVOCATION__( __result__, __operation__, __partition__, __sid__, __oid__ ) {\
    try { \
	is::repository_var REP = is::server::resolve( __partition__, __sid__ ); \
	__result__ REP -> __operation__; \
    } \
    catch( is::InvalidCriteria & ex ) { \
	throw daq::is::InvalidCriteria( ERS_HERE, (const char *)ex.expression ); \
    } \
    catch( is::NotFound & ) { \
	throw daq::is::InfoNotFound( ERS_HERE, __sid__ + "." + __oid__ ); \
    } \
    catch( is::SubscriptionNotFound & ) { \
	throw daq::is::SubscriptionNotFound( ERS_HERE, __sid__ + "." + __oid__ ); \
    } \
    catch( is::NotCompatible & ) { \
	throw daq::is::InfoNotCompatible( ERS_HERE, __sid__ + "." + __oid__ ); \
    } \
    catch( is::AlreadyExist & ) { \
	throw daq::is::InfoAlreadyExist( ERS_HERE, __sid__ + "." + __oid__ ); \
    } \
    catch( CORBA::SystemException & ex ) { \
	throw daq::is::RepositoryNotFound( ERS_HERE, __sid__, daq::ipc::CorbaSystemException( ERS_HERE, &ex ) ); \
    } \
}

#define IS_EMPTY

#define IS_INVOKE( operation, partition, sid, oid ) \
	__TRY_INVOCATION__( IS_EMPTY, operation, partition, sid, oid )

#define IS_INVOKE_AND_RETURN( operation, partition, sid, oid ) \
	__TRY_INVOCATION__( return, operation, partition, sid, oid )

#endif
