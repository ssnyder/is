#ifndef IS_CALLBACK_MANAGER_H
#define IS_CALLBACK_MANAGER_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      callbackmanager.h
//
//      private header file for the IS library
//
//      Sergei Kolos, Mai 2013
//
//      description:
//		Singleton which implements subscriptions monitoring
//////////////////////////////////////////////////////////////////////////////////////
#include <chrono>
#include <memory>
#include <mutex>
#include <thread>
#include <unordered_set>

#include <ipc/alarm.h>
#include <ipc/partition.h>

struct ISCallbackHolder;

class ISCallbackManager
{
public:
    static ISCallbackManager & instance(const IPCPartition & p, const std::string & server_name);

    void addCallback(ISCallbackHolder * c);

    void removeCallback(ISCallbackHolder * c);

    const IPCPartition	m_partition;
    const std::string	m_server_name;

private:
    ISCallbackManager(const IPCPartition & p, const std::string & server_name);

    bool periodicAction();

    typedef std::unordered_set<ISCallbackHolder*>	CallbackSet;

    const std::time_t m_checkup_period;
    std::mutex	m_mutex;
    CallbackSet	m_callbacks;
    IPCAlarm	m_alarm;
};

#endif
