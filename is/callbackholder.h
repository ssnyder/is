/*
 * callbackholder.h
 *
 *  Created on: Jun 17, 2013
 *      Author: kolos
 */

#ifndef IS_CALLBACKHOLDER_H_
#define IS_CALLBACKHOLDER_H_

#include <string>

#include <ipc/servantbase.h>
#include <ipc/partition.h>

#include <is/is.hh>
#include <is/callbackmanager.h>

struct ISCallbackHolder
{
    ISCallbackHolder(const IPCPartition & p, const std::string & server,
	IPCServantBase<POA_is::callback> * c, int mask)
	: m_partition(p),
	  m_server_name(server),
	  m_callback(c),
	  m_mask(mask),
	  m_wait(false)
    {
	ISCallbackManager::instance(m_partition, m_server_name).addCallback(this);
    }

    virtual ~ISCallbackHolder()
    {
	ISCallbackManager::instance(m_partition, m_server_name).removeCallback(this);
	m_callback->_destroy(m_wait);
    }

    virtual void subscribe() = 0;

    virtual void unsubscribe() = 0;

    void resubscribe(const std::chrono::system_clock::time_point & server_start_time) {
        if (m_subscription_time < server_start_time) {
            subscribe();
        }
    }

    bool & wait()
    {
	return m_wait;
    }

protected:
    IPCPartition m_partition;
    std::string m_server_name;
    IPCServantBase<POA_is::callback> * m_callback;
    int m_mask;
    bool m_wait;
    std::chrono::system_clock::time_point m_subscription_time;
};

#endif /* IS_CALLBACKHOLDER_H_ */
