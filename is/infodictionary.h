#ifndef _IS_INFO_DICTIONARY_H
#define _IS_INFO_DICTIONARY_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/infodictionary.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		ISInfoDictionary allows IS objects manipulation ( create, update, delete )
//////////////////////////////////////////////////////////////////////////////////////
#include <vector>
#include <map>

#include <ipc/partition.h>
#include <is/criteria.h>
#include <is/infoany.h>
#include <is/type_traits.h>

class ISInfoDictionary
{
    friend class ISNamedInfo;
  public:
  
    ISInfoDictionary( ) 
    { ; }
    
    ISInfoDictionary( const IPCPartition & p ) 
      : m_partition( p )
    { ; }
    
    ~ISInfoDictionary( )
    { ; }    
    
    const IPCPartition & partition() { return m_partition; }
    
    void checkin ( const std::string & name, const ISInfo & info, bool keep_history = false ) const;
                                        
    void checkin ( const std::string & name, int tag, const ISInfo & info ) const;
                                        
    bool contains ( const std::string & name ) const;
                                        
    void insert ( const std::string & name, const ISInfo & info ) const;
                                        
    void insert ( const std::string & name, int tag, const ISInfo & info ) const;
                                        
    void findType ( const std::string & name, ISType & type ) const;
                                        
    void findValue ( const std::string & name, ISInfo & info ) const;
                                        
    void findValue ( const std::string & name, int tag, ISInfo & info ) const;
                                        
    void getType ( const std::string & name, ISType & type ) const;
                                        
    void getValue ( const std::string & name, ISInfo & info ) const;

    void getValue ( const std::string & name, int tag, ISInfo & info ) const;

    void getTags( const std::string & name, std::vector<std::pair<int,OWLTime> > & tags ) const;

    void remove ( const std::string & name ) const;

    void removeAll ( const std::string & server_name, const ISCriteria & criteria ) const;

    template<class T>
    void getValues ( const std::string & name, std::vector<T> & values,
            int how_many = -1, ISInfo::HistorySorted order = ISInfo::ByTime ) const;

    void update ( const std::string & name, const ISInfo & info, bool keep_history = false ) const;
                                        
    void update ( const std::string & name, int tag, const ISInfo & info ) const;
                                        
  private:
    
    void update (const std::string & name, const ISInfo & info, bool create_if_not_exist,
            bool keep_history, bool use_tag, int tag = 0) const;
                                        
    void history (const std::string & name, int how_many, is::info_history_var & info,
            ISInfo::HistorySorted order) const;

  private:
    IPCPartition m_partition;    
};

template<class T>
void
ISInfoDictionary::getValues( const std::string & name, std::vector<T> & values,
        int how_many, ISInfo::HistorySorted order) const
{
    is::info_history_var info;
    history( name, how_many, info, order);
    
    std::vector<is::type_traits::copy_wrapper<T> > * wrp 
    	= reinterpret_cast<std::vector<is::type_traits::copy_wrapper<T> > *>( &values );
    wrp->resize( info->values.length() );

    for ( unsigned int i = 0; i < info->values.length(); i++ )
    {
        is::idatastream in( info->values[i].data );
        (*wrp)[i].fromWire( m_partition, name.c_str(), info->type, info->values[i], in);
    }
}

#endif 
