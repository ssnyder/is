#ifndef IS_ISTREAM_H
#define IS_ISTREAM_H

#if (__GNUC__ == 4 && __GNUC_MINOR__ == 8 && __GNUC_PATCHLEVEL__ == 1)
// This pragma is a workaround for the GCC 4.8.1 bug
// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=57319
// Should be removed when we move to another version
#pragma GCC diagnostic ignored "-Wvirtual-move-assign"
#endif

#include <string>
#include <vector>
#include <memory>

#include <is/types.h>
#include <boost/type_traits/remove_const.hpp>

#define IS_ISTREAM_INTERFACE(x,_)		protected virtual is::istream_impl_unit_base<x>
#define IS_ISTREAM_INTERFACE_SEPARATOR		,
#define IS_ISTREAM_INTERFACE_OBJECT_TYPE	ISInfo

#define IS_ISTREAM_USING(x,_)			using is::istream_impl_unit_base<x>::delegate;
#define IS_ISTREAM_USING_SEPARATOR
#define IS_ISTREAM_USING_OBJECT_TYPE		ISInfo

#define IS_ISTREAM_IMPL(x,_)			protected virtual is::istream_impl_unit<x,S>
#define IS_ISTREAM_IMPL_SEPARATOR		,
#define IS_ISTREAM_IMPL_OBJECT_TYPE		ISInfo

namespace is
{  
    template <typename S>
    struct istream_base
    {
      	virtual ~istream_base() { ; }
	virtual S & stream() = 0;
    };
  
    template <typename T>
    class istream_impl_unit_base
    {
      public:
      	virtual ~istream_impl_unit_base() { ; }
        
      protected:
	virtual void delegate( T & val ) = 0;
	virtual void delegate( T * val, size_t size, size_t item_size ) = 0;
    };
    
    class sizeable
    {
      protected:
	virtual ~sizeable(){ ; }
	virtual size_t get_size() = 0;
    };

    template <typename T, typename S>
    class istream_impl_unit :	public virtual istream_impl_unit_base<T>,
				public virtual istream_base<S>
    {
	void delegate( T & val ) {
	    this->stream() >> val;
	}
    
	void delegate( T * val, size_t size, size_t item_size ) {
	    this->stream().get( val, size, item_size );
	}	
    };
  
    class istream :	IS_TYPES(IS_ISTREAM_INTERFACE),
			protected virtual sizeable
    {        
        IS_TYPES(IS_ISTREAM_USING)
        
      public:
	template <class T>
	istream & operator>>( T & val ) {
	    delegate( is::cast( val ) );
	    return *this;
	}
    
	template <class T>
	istream & operator>>( std::vector<T> & val ) {
	    size_t size = get_size();
	    val.resize( size );
	    delegate( is::cast( &val[0] ), size, sizeof( T ) );
	    return *this;
	}

	template <class T>
	istream & get( T ** val, size_t & size ) {
	    size = get_size();
	    typename boost::remove_const<T>::type * buf = new typename boost::remove_const<T>::type[size];
	    delegate( is::cast( buf ), size, sizeof( T ) );
            *val = buf;
	    return *this;
	}
	
	template <class T>
	istream & get( T * const val, size_t & size ) {
	    size_t real_size = get_size();
	    T * real_val = new T[real_size];
	    
	    delegate( is::cast( real_val ), real_size, sizeof( T ) );
            
	    size = real_size < size ? real_size : size;
	    std::copy( real_val, real_val + size, val );
	    delete[] real_val;
	    return *this;
	}
        
	istream & operator>>( std::vector<bool> & val ) {
	    size_t size = get_size();
	    bool * array = new bool[size];
            
	    delegate( array, size, sizeof( bool ) );
	    
            val.resize( size );
	    std::copy( array, array + size, val.begin() );
	    delete[] array;
	    return *this;
	}

    };

    template <typename S>
    class istream_impl_base :	IS_TYPES(IS_ISTREAM_IMPL),
    				protected virtual sizeable
    {
	virtual size_t get_size() {
	    return this->stream().read_size();
	}
    };
  
    template <typename S>
    class istream_tie :	public  istream,
			protected istream_impl_base<S>
    {
	S & m_stream;        
      public:
        S & stream() { return m_stream; }
      
        istream_tie( S & stream )
	  : m_stream( stream )
	{ ; }
    };  
}

typedef is::istream ISistream;

#endif
