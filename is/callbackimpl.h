#ifndef IS_CALLBACK_IMPL_H
#define IS_CALLBACK_IMPL_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      callbackbase.h
//
//      private header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		implementation of the is::callback interface
//////////////////////////////////////////////////////////////////////////////////////
#include <boost/function.hpp>
#include <boost/lexical_cast.hpp>

#include <ipc/object.h>
#include <ipc/servant.h>

#include <is/callback_traits.h>
#include <is/is.hh>
 
template <typename F, typename TP>
class ISCallbackImpl :	public is::callback_traits<F>::interface_type,
			public IPCObject<POA_is::callback,TP,ipc::transient>,
			public IPCServant
{
  public:
    ISCallbackImpl( const IPCPartition & p,
		    const std::string & server_name,
		    F callback,
                    typename is::callback_traits<F>::parameter_type param )
      : m_partition( p ),
        m_server_name( server_name ),
        m_user_param( param ),
	m_callback( is::callback_traits<F>::make_functor( callback, param ) )
    { ; }
        
  private:
    typedef boost::function<void(typename is::callback_traits<F>::type*)> Callback;
    
    struct helper
    {
    	typedef void result_type;
        
        void operator()( Callback callback,
        		 const IPCPartition & partition, 
        		 const std::string & server_name,
                         void * user_param,
                         is::reason reason,
                         const typename is::callback_traits<F>::input_type & data )
        {
	    typename is::callback_traits<F>::type p( partition, server_name, user_param, reason, data );
	    callback( &p );
        }
    };
    
    void receive( const typename is::callback_traits<F>::input_type & data, is::reason reason )
    {
	typename is::callback_traits<F>::type 
        	p( m_partition, m_server_name, m_user_param, reason, data );
	m_callback( &p );
    }

  private:
    IPCPartition	m_partition;
    std::string		m_server_name;
    void *		m_user_param;
    Callback		m_callback;
};

#endif
