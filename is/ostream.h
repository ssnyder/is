#ifndef IS_OSTREAM_H
#define IS_OSTREAM_H

#include <string>
#include <vector>

#include <is/types.h>

#define IS_OSTREAM_INTERFACE(x,_)		protected virtual is::ostream_impl_unit_base<x>
#define IS_OSTREAM_INTERFACE_SEPARATOR		,
#define IS_OSTREAM_INTERFACE_OBJECT_TYPE	ISInfo

#define IS_OSTREAM_USING(x,_)			using is::ostream_impl_unit_base<x>::delegate;
#define IS_OSTREAM_USING_SEPARATOR
#define IS_OSTREAM_USING_OBJECT_TYPE		ISInfo

#define IS_OSTREAM_IMPL(x,_)			protected virtual is::ostream_impl_unit<x,S>
#define IS_OSTREAM_IMPL_SEPARATOR		,
#define IS_OSTREAM_IMPL_OBJECT_TYPE		ISInfo

namespace is
{  
    template <typename S>
    struct ostream_base
    {
      	virtual ~ostream_base() { ; }
	virtual S & stream() = 0;
    };
  
    template <typename T>
    class ostream_impl_unit_base
    {
      public:
      	virtual ~ostream_impl_unit_base() { ; }
        
      protected:
	virtual void delegate( const T & val ) = 0;
	virtual void delegate( const T * val, size_t size, size_t item_size, const T & ) = 0;
    };

    template <typename T, typename S>
    class ostream_impl_unit :	public virtual ostream_impl_unit_base<T>,
				public virtual ostream_base<S>
    {
	void delegate( const T & val ) {
	    this->stream() << val;
	}
    
	void delegate( const T * val, size_t size, size_t item_size, const T & obj ) {
	    this->stream().put( val, size, item_size, obj );
	}
    };
    
    class ostream : IS_TYPES(IS_OSTREAM_INTERFACE)
    {
	IS_TYPES(IS_OSTREAM_USING)
	
      public:
	template <typename T>
	ostream & operator<<( const T & val ) {
	    delegate( is::cast( val ) );
	    return *this;
	}
    
	template <typename T>
	ostream & operator<<( const std::vector<T> & val ) {
	    delegate( is::cast( &val[0] ), val.size(), sizeof( T ), T() );
	    return *this;
	}

	template <typename T>
	ostream & put( const T * val, size_t size ) {
	    delegate( is::cast( val ), size, sizeof( T ), T() );
	    return *this;
	}
        
	ostream & operator<<( const std::vector<bool> & val ) {
	    bool * array = new bool[val.size()];
	    std::copy( val.begin(), val.end(), array );
	    delegate( array, val.size(), sizeof( bool ), true );
	    delete[] array;
	    return *this;
	}

    };

    template <typename S>
    class ostream_impl_base : IS_TYPES(IS_OSTREAM_IMPL)
    { };
  
    template <typename S>
    class ostream_tie :	public ostream,
			protected ostream_impl_base<S>
    {
	S & m_stream;
        S & stream() { return m_stream; }
      public:
        ostream_tie( S & stream )
	  : m_stream( stream )
	{ ; }
    };  
}

typedef is::ostream ISostream;

#endif
