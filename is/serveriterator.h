#ifndef IS_SERVER_ITERATOR_H
#define IS_SERVER_ITERATOR_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/serveriterator.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		ISServerIterator allows sequential access to IS servers
//////////////////////////////////////////////////////////////////////////////////////
#include <vector>

#include <ipc/partition.h>
#include <is/infoany.h>
#include <owl/time.h>

class ISServerIterator
{
  public:
    ISServerIterator( const IPCPartition & ) ;
    ISServerIterator( const IPCPartition &, const std::string & criteria );
    
    bool operator() ( );
    bool operator++ ( );
    bool operator-- ( );
    bool operator++ ( int );
    bool operator-- ( int );

    void reset( );
    int entries( ) const;
    const char * name( ) const;
    const char * owner( ) const;
    const char * host( ) const;
    int pid( ) const;
    OWLTime time( ) const;
    const IPCPartition & partition() const
    { return m_partition; }
  
  private:    
    ////////////////////////////////
    // Disallow object copying
    ////////////////////////////////
    ISServerIterator( const ISServerIterator & );
    ISServerIterator& operator= ( const ISServerIterator & );
    
    typedef std::vector< std::pair<std::string, ipc::servant::ApplicationContext_var> > Servers;
    
    IPCPartition m_partition;
    Servers	 m_list;
    int	 	 m_index;
    int	 	 m_length;
};

#endif    // define IS_SERVER_ITERATOR_H
