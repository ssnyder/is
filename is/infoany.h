#ifndef IS_INFOANY_H
#define IS_INFOANY_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/infoany.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//              Class ISInfoAny allows access to the information, 
//              whose type is not known at compilation time
//////////////////////////////////////////////////////////////////////////////////////

#include <is/info.h>
#include <is/datastream.h>

class ISInfoAny : public ISInfo,
		  public is::istream_tie<is::idatastream>
{
    is::idatastream m_stream;
    
  public:	
    ISInfoAny();
    
    void reset ( ) { m_stream.rewind(); }
    
    size_t countAttributes( ) const
    { return type().entries(); }
    
    ISType::Basic getAttributeType( ) const
    { return type().entryType( m_stream.position() ); }
    
    size_t getArraySize( )
    { return m_stream.read_size(); }
    
    size_t readArraySize( )
    { return m_stream.unmarshal_size(); }
    
    bool isAttributeArray( ) const
    { return type().entryArray( m_stream.position() ); }
    
    ISType getAttributeInfoType( ) const
    { return type().entryInfoType( m_stream.position() ); }
    
    std::ostream & print( std::ostream & out ) const;
    
    void operator >>=( ISInfo & info ) const ;
    void operator <<=( const ISInfo & info ) noexcept;

  private:    
    void publishGuts ( ISostream & ) { ; }
    void refreshGuts ( ISistream & in );
    
    ////////////////////////////////
    // Disallow object copying
    ////////////////////////////////
    ISInfoAny( const ISInfoAny & );
    ISInfoAny& operator= ( const ISInfoAny & );
};

///////////////////////////////////////////////
// Output operations for the ISInfoAny
///////////////////////////////////////////////

std::ostream& operator<< ( std::ostream& , const ISInfoAny & );

namespace is
{
  size_t print_attribute_value( std::ostream & out, ISInfoAny & isa, const ISType & type, size_t entry, int how_many = -1 );
  void   print_attribute_type ( std::ostream & out, const ISType & type, size_t entry, size_t size );
}

#endif	// define IS_INFOANY_H
