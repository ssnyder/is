#ifndef IS_INFODOCUMENT_H
#define IS_INFODOCUMENT_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/infodocument.h
//
//      public header file for the IS library
//
//      Sergei Kolos January 1999
//
//      description:
//              Class ISInfoDocument allows access to the information 
//              definition ( attributes names, types and descriptions )
//////////////////////////////////////////////////////////////////////////////////////
#include <vector>

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/mem_fun.hpp>

#include <boost/thread/mutex.hpp>

#include <ipc/partition.h>
#include <is/info.h>

using namespace boost::multi_index;

namespace rdb
{
    struct RDBAttribute;
    struct RDBClass;
    struct RDBRelationship;
}

class ISInfoDocument
{
  public:    
    
    class Attribute
    {
      public:            
	typedef std::ios_base& (*IntegerFormat)(std::ios_base&);
                
	Attribute( const rdb::RDBAttribute & attribute, size_t position );
	Attribute( const rdb::RDBRelationship & relationship, size_t position );
        
	const std::string & description ( ) const 
        { return m_description; }
        
        bool isArray ( ) const 
        { return m_is_array; }
        
        IntegerFormat format ( ) const 
        { return m_format; }
	
        const std::string & name ( ) const 
        { return m_name; }
        
        const std::string & range ( ) const 
        { return m_range; }
        
        ISType::Basic typeCode ( ) const 
        { return m_typecode; }
        
        const std::string & typeName ( ) const 
        { return m_type; }
        
        size_t position() const
        { return m_position; }
               
      private:
	std::string	m_name;
	std::string	m_type;
        std::string	m_range;
	std::string	m_description;
	bool		m_is_array;
        ISType::Basic	m_typecode;
        IntegerFormat	m_format;
        size_t		m_position;
    };
        
  private:
    typedef boost::multi_index_container<
	Attribute,
	indexed_by<
	    hashed_unique <
		const_mem_fun<Attribute,const std::string &,&Attribute::name>
	    >,
	    ordered_unique <
		const_mem_fun<Attribute,size_t,&Attribute::position>
	    >
	>
    > AttributeSet;
    
    typedef AttributeSet::nth_index<1>::type AttributeVector;  
    
    struct Holder
    {
	IPCPartition	m_partition;
	std::string	m_description;
	AttributeSet	m_attributes;
	std::string	m_name;
    };
    
    class Directory : public std::map< std::string, Holder >
    {
        const IPCPartition m_partition;
        boost::mutex m_mutex;
        
      public:        
        static Directory & 
        getAllDocuments( const IPCPartition & partition );
        
	static const ISInfoDocument::Holder &
	findDocument( const IPCPartition & partition, const std::string & name,
	        bool lookup_if_not_in_cache = false );
        
      private:
	Directory( const IPCPartition & partition );
        
        const ISInfoDocument::Holder & 
        addDocument( const char * name, const rdb::RDBClass * cl = 0 );
        
        static boost::mutex			 s_guard;
	static std::map<IPCPartition,Directory*> s_documents;
    };
    
  public:
    class Iterator
    {       
      public:

	Iterator ( const IPCPartition & partition );
	
	bool	operator() ();
		operator bool ();
		
	ISInfoDocument operator* ( ) ;
	
	Iterator operator++ ( );
	Iterator operator++ ( int );
	Iterator operator-- ( );
	Iterator operator-- ( int );
        
	size_t	size();
       
      private:
               
        const Directory &		m_documents;
	Directory::const_iterator	m_it;
    };
    
    friend class Iterator;

    
    ISInfoDocument( const IPCPartition & partition, ISInfo & info,
            bool lookup_if_not_in_cache = false );
    ISInfoDocument( const IPCPartition & partition, const ISType & type,
            bool lookup_if_not_in_cache = false );
    ISInfoDocument( const IPCPartition & partition, const std::string & type,
            bool lookup_if_not_in_cache = false );
                     
    size_t attributeCount ( ) const
    { return m_holder.m_attributes.size(); }
     
    const std::string & description ( ) const
    { return m_holder.m_description; }

    const std::string & name ( ) const
    { return m_holder.m_name; }    
  
    const Attribute & attribute ( size_t index ) const;
    
    const Attribute & attribute ( const std::string & name ) const;
    
    size_t attributePosition( const std::string & name ) const;
    
    const IPCPartition & partition() const  
    { return m_holder.m_partition; }
    
  private:
    ISInfoDocument( const Holder & holder )
      : m_holder( holder )
    { ; }
    
  private:
    
    const Holder & m_holder;
};

#endif    // define IS_INFODOCUMENT_H
