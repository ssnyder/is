#ifndef IS_INFO_STREAM_H
#define IS_INFO_STREAM_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/infoiterator.h
//
//      public header file for the IS library
//
//      Sergei Kolos November 2008
//
//      description:
//		ISInfoStream provides fast access to IS objects
//////////////////////////////////////////////////////////////////////////////////////
#include <chrono>
#include <condition_variable>
#include <thread>
#include <vector>

#include <boost/noncopyable.hpp>

#include <ipc/partition.h>

#include <is/criteria.h>
#include <is/info.h>

class ISInfoStreamImpl;

class ISInfoStream: boost::noncopyable
{
    friend class ISInfoStreamImpl;

public:
    ISInfoStream(const IPCPartition & partition,
	const std::string & server_name,
	const ISCriteria & = ISCriteria(".*"),
	bool synchronous = false,
	int history_depth = 1,
	ISInfo::HistorySorted order = ISInfo::ByTime);

    ~ISInfoStream();

    bool eof() const;

    void skip(size_t entries = 1);

    const IPCPartition & partition() const
    {
	return m_partition;
    }

    const std::string & name() const;

    const std::string & objectName() const;

    const std::string & serverName() const
    {
	return m_server_name;
    }

    int tag() const;

    ISType type() const;

    OWLTime time() const;

    ISInfoStream & operator>>(ISInfo & info);

    template<class T>
    ISInfoStream & operator>>(std::vector<T> & values);

    unsigned int entries() const
    {
	return m_info.size();
    }

protected:
    void reset()
    {
	m_pos = 0;
    }

    bool move(int offset)
    {
	m_pos += offset;
	return __valid_position__();
    }

private:
    void constructionCompleted();
    void add(const char * name, const is::type & type, const is::value_list & list);

    bool __valid_position__() const
    {
        return ( m_pos >= 0 && m_pos < (int)m_info.size() );
    }

    void __assert_position__() const
    {
        ERS_ASSERT_MSG( __valid_position__(), "attempting to read beyond stream boundaries" );
    }

private:
    struct InfoHolder
    {
	InfoHolder(const std::string & sid, const char * oid,
	    const is::type & type, const is::value & value);

	const std::string m_oid;
	const std::string m_name;
	const is::type m_type;
	is::value m_value;
    };

private:
    mutable std::mutex m_mutex;
    mutable std::condition_variable m_condition;

    const IPCPartition m_partition;
    const std::string m_server_name;
    std::vector<InfoHolder *> m_info;
    int m_pos;
    bool m_eof;
    ISInfoStreamImpl * m_impl;
};

template<class T>
ISInfoStream &
ISInfoStream::operator>>(std::vector<T> & values)
{
    for (size_t i = 0; i < values.size(); i++)
	(*this) >> values[i];
    return *this;
}

#endif    // define IS_INFO_ITERATOR_H
