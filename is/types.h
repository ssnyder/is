#ifndef IS_TYPES_H
#define IS_TYPES_H

//////////////////////////////////////////////////////////////////////////////////////
//
//      is/types.h
//
//      private header file for the IS library
//
//      Sergei Kolos November 2004
//
//      description:
//		Defines the types supported by an IS information
//////////////////////////////////////////////////////////////////////////////////////
#include <stdint.h>

#include <boost/type_traits/is_enum.hpp>
#include <boost/type_traits/integral_promotion.hpp>

#include <owl/time.h>

class ISInfo;

//////////////////////////////////////////////////////////////
// this is the list of the types, which are supported
// by the IS information attributes
//////////////////////////////////////////////////////////////
#define IS_TYPES(decl)					\
decl(bool,Boolean)		decl ## _SEPARATOR	\
decl(char,S8)			decl ## _SEPARATOR	\
decl(unsigned char,U8)		decl ## _SEPARATOR	\
decl(short,S16)			decl ## _SEPARATOR	\
decl(unsigned short,U16)	decl ## _SEPARATOR	\
decl(int,S32)			decl ## _SEPARATOR	\
decl(unsigned int,U32)		decl ## _SEPARATOR	\
decl(int64_t,S64)		decl ## _SEPARATOR      \
decl(uint64_t,U64)		decl ## _SEPARATOR      \
decl(float,Float)		decl ## _SEPARATOR	\
decl(double,Double)		decl ## _SEPARATOR	\
decl(OWLDate,Date)		decl ## _SEPARATOR	\
decl(OWLTime,Time)		decl ## _SEPARATOR	\
decl(std::string,String)	decl ## _SEPARATOR	\
decl(decl ## _OBJECT_TYPE,InfoObject)

namespace is
{
    template <	typename T, 
    		bool arch32 = ( sizeof(long) == sizeof(int) ), 
                bool an_enum = boost::is_enum<T>::value > 
    struct adaptor					{ typedef T Type; };
    
    // Convert 'enum' to 'int'
    template <	typename T, bool arch32 >	
    struct adaptor<T, arch32, true>			{ typedef typename boost::integral_promotion<T>::type Type; };

    // For backward compatibility 'long' types are converted to 'int' on 32-bit platforms
    template <> 
    struct adaptor<long, true, false>			{ typedef int Type; };
    template <> 
    struct adaptor<const long, true, false>		{ typedef const int Type; };
    template <> 
    struct adaptor<unsigned long, true, false>		{ typedef unsigned int Type; };
    template <> 
    struct adaptor<const unsigned long, true, false>	{ typedef const unsigned int Type; };

    template <typename T>
    inline typename adaptor<T>::Type & cast( T & from )
    {
	return *(typename adaptor<T>::Type *)( &from );
    }    
    
    template <typename T>
    inline typename adaptor<T>::Type * cast( T * from )
    {
	return (typename adaptor<T>::Type *)( from );
    }
}

#endif
