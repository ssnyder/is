////////////////////////////////////////////////////////////////////////
//        f_source.cc
//
//        Test application for the IS library
//
//        Sergei Kolos,    January 2000
//
//        description:
//		Test sending commands to IS object providers
//	    
////////////////////////////////////////////////////////////////////////

#include <unistd.h>

#include <iostream>

#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <is/test/LargeInfo.h>
#include <is/infoany.h>
#include <is/infodocument.h>
#include <is/infoprovider.h>
#include <is/infodictionary.h>

using is::test::LargeInfo;

CmdArgStrList	info_names('N', "names", "name", "names of information to send commands.", CmdArg::isREQ | CmdArg::isLIST);
CmdArgStr	server_name('n', "server", "server-name", "server to work with.",CmdArg::isREQ);
CmdArgInt	delay ('d', "delay", "seconds","delay between commands");

void test(ISInfoDictionary& id, ISInfo& ti, const char * test_name, int verbose);   

int main(int argc, char ** argv)
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
    }
    
    // Declare arguments
    CmdArgBool	verbose ('v', "verbose", "turn on verbose mode.");
    CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.");
 
    // Declare command object and its argument-iterator
    CmdLine    cmd(*argv, &verbose, &delay, &partition_name, &server_name, &info_names, NULL);
    CmdArgvIter    arg_iter(--argc, ++argv);
 
    cmd.description(	"This program is part of the IS functionality test suit.\n"
			"It send commands to information objects.");
    
    // Parse arguments
    cmd.parse(arg_iter);

    IPCPartition	p(partition_name);
    ISInfoDictionary	id(p);
    
    LargeInfo info;
    
    try
    {   
	test( id, info, "Generated information class", verbose );
    }
    catch( daq::is::Exception & ex )
    {
    	is::fatal( ex );
    }
    
    return 0;
}

void test( ISInfoDictionary& id, ISInfo& ti, const char * test_name, int verbose )
{            
    for ( unsigned int i = 0; i < info_names.count(); i++ )
    {
    	std::cerr << "Information Name is " << info_names[i] << std::endl;
    	std::string	name(server_name);
    	name += ".";
    	name += info_names[i];
    	
    	id.getValue( name.c_str(), ti );
     	std::cout << "getValue successfull " << std::endl;
    	
    	ti.sendCommand( "reset" );
     	std::cout << "sendCommand successfull " << std::endl;
    	
	sleep((unsigned)delay);
	
	ISInfoAny	isa;
    	id.getValue( name.c_str(), isa );
     	std::cout << "getValue successfull " << std::endl;
	if ( verbose )
	{
	    std::cout << "Tested " << test_name << " ";
	    std::cout << isa;
	}
    	    	
    	isa.sendCommand( "reset" );
	std::cout << "sendCommand successfull " << std::endl;
    	
	sleep((unsigned)delay);
    }
}

