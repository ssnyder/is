////////////////////////////////////////////////////////////////////////
//            f_receiver.cc
//
//            Test application for the IS library
//
//            Sergei Kolos,    January 2000
//
//            description:
//	            Test the functionality of the ISInfoReceiver class
//	
////////////////////////////////////////////////////////////////////////

#include <iostream>

#include <ers/ers.h>
#include <cmdl/cmdargs.h>

#include <ipc/core.h>
#include <ipc/signal.h>

#include <is/infoany.h>
#include <is/infodictionary.h>
#include <is/test/MediumInfo.h>
#include <is/test/LargeInfo.h>
#include <is/inforeceiver.h>

using is::test::LargeInfo;
using is::test::MediumInfo;

CmdArgBool	verbose ('v', "verbose", "turn on verbose mode.");

void process_callback(const ISCallbackEvent & isc)
{
    if (!verbose)
	return;

    OWLTime t(OWLTime::Microseconds);
    std::cerr << isc.name() <<  " " << isc.time() << " " << t << " "
	<< (t.total_mksec_utc() - isc.time().total_mksec_utc()) << std::endl;

    std::cout << "name	      : " << isc.name() << std::endl;
    std::cout << "server name : " << isc.serverName() << std::endl;
    std::cout << "object name : " << isc.objectName() << std::endl;
    std::cout << "Reason code : " << isc.reason() << std::endl;
    std::cout << "time	      : " << isc.time() << std::endl;
    std::cout << "type	      : " << isc.type() << std::endl;
    std::cout << "tag	      : " << isc.tag() << std::endl;
}

void process_callback(const ISCallbackInfo & isc)
{
    process_callback( (ISCallbackEvent&)isc );
    
    if ( verbose )
    {
	is::test::LargeInfo li;
	is::test::MediumInfo mi;
	if ( li.type() == isc.type() )
	{
	    isc.value(li);
	    std::cout << li;
	}
	else if ( mi.type() == isc.type() )
	{
	    isc.value(mi);
	    std::cout << mi;
	}
    }

    if ( verbose )
    {
	ISInfoAny isa;
	isc.value(isa);
	std::cout << isa;
    }

    if ( verbose )
    {
	is::test::LargeInfo li;
	is::test::MediumInfo mi;
	if ( li.type() == isc.type() )
	{
	    isc.value(li);
	    std::cout << li;
	}
	else if ( mi.type() == isc.type() )
	{
	    isc.value(mi);
	    std::cout << mi;
	}
    }
}

void info_callback(const ISCallbackInfo * isc)
{
    std::cerr << "Info Callback FUNCTION:" << std::endl;
    process_callback( *isc );
}

void event_callback(const ISCallbackEvent * ise)
{
    std::cout << "Event Callback FUNCTION:" << std::endl;
    process_callback( *ise );
}

struct InfoCallback
{
    void operator()( const ISCallbackInfo * isc )
    {
	std::cout << "Info Callback FUNCTOR:" << std::endl;
        process_callback( *isc );
    }
};

struct EventCallback
{
    void operator()( const ISCallbackEvent * ise )
    {
	std::cout << "Event Callback FUNCTOR:" << std::endl;
        process_callback( *ise );
    }
};

struct CustomInfoCallback
{
    void callback( const ISCallbackInfo * isc )
    {
	std::cout << "Info Callback MEMBER FUNCTION:" << std::endl;
        process_callback( *isc );
    }
};

struct CustomEventCallback
{
    void callback( const ISCallbackEvent * ise )
    {
	std::cout << "Event Callback MEMBER FUNCTION:" << std::endl;
        process_callback( *ise );
    }
};

int main(int argc, char ** argv)
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
    }
    
    // Declare arguments
    CmdArgStr		partition_name('p', "partition", "partition-name", "partition to work in.");
    CmdArgBool		value ('V', "value", "subscribe for information values.");
    CmdArgBool		notunsubscribe ('u', "notunsubscribe", "do not unsubscribe.");
    CmdArgStr		server_name('n', "server", "server-name", "server to subscribe to.",CmdArg::isREQ);
    CmdArgStrList	info_names ('N', "names", "names", "names of information to subscribe for.");
    CmdArgStrList	expressions('R', "rexpressions", "regular-expressions", "list of regular expressions to subscribe with.");
    CmdArgChar		type ('t', "type", "object_type", "define type of the information objects to subscribe\n"
    					"M - MediumInfo class\n"
    					"L - LargeInfo class\n");
    CmdArgChar		type_mode ('m', "mode", "type_mode", "define how to use the type for subscription\n"
    					"I - invert type (i.e. subscribe for all types but not that)\n"
    					"W - week type (i.e. subscribe for all subtypes of this type)\n");
    CmdArgChar		logic ('l', "logic", "criteria_logic", "define how to combine type and regular expressions for subscription\n"
    					"A - 'and' logic (default)\n"
    					"O - 'or'  logic\n");
    CmdArgInt		event_mask ('e', "events", "event-mask", "bitmask for events to subscribe, bits positions meaning is 0=Created, 1=Updated, 2=Deleted, 3=Subscribed.");
    
    // Declare command object and its argument-iterator
    CmdLine	cmd(*argv, &verbose, &value, &partition_name, &server_name, &event_mask,
    			   &info_names, &expressions, &type, &type_mode, &logic, &notunsubscribe, NULL);
    
    CmdArgvIter	arg_iter(--argc, ++argv);
 
    notunsubscribe = false;
    value = false;
    logic = 'A';
    event_mask = 7;
    
    cmd.description(	"This program is part of the IS functionality test suit.\n"
			"It checks callback functionality.");
    
    // Parse arguments
    cmd.parse(arg_iter);

    IPCPartition	p(partition_name);

    ISInfoReceiver receiver(p);

    int m = (int)event_mask;
    std::initializer_list<ISInfo::Reason> emask = {
	(m & 1) ? ISInfo::Created : ISInfo::Reason(-1),
	(m & 2) ? ISInfo::Updated : ISInfo::Reason(-1),
	(m & 4) ? ISInfo::Deleted : ISInfo::Reason(-1),
	(m & 8) ? ISInfo::Subscribed : ISInfo::Reason(-1)};
    try
    {
	CustomEventCallback custom_event_callback;
	CustomInfoCallback  custom_info_callback;
	for ( size_t i = 0; i < info_names.count(); i++ )
	{
	    std::string name(server_name);
	    name += ".";
	    name += info_names[i];

	    if ( i%3 == 0 )
	    {
		if ( value )
		    receiver.subscribe(name, info_callback, nullptr, emask);
		else
		    receiver.subscribe(name, event_callback, nullptr, emask);

		std::cout << "Subscribe to " << info_names[i] << " successfull " << std::endl;
	    }

	    if ( i%3 == 1 )
	    {
		if ( value )
		    receiver.subscribe(name, InfoCallback(), nullptr, emask);
		else
		    receiver.subscribe(name, EventCallback(), nullptr, emask);

		std::cout << "Subscribe to " << info_names[i] << " successfull " << std::endl;
	    }

	    if ( i%3 == 2 )
	    {
		if ( value )
		    receiver.subscribe(name, &CustomInfoCallback::callback, &custom_info_callback, emask);
		else
		    receiver.subscribe(name, &CustomEventCallback::callback, &custom_event_callback, emask);

		std::cout << "Subscribe to " << info_names[i] << " successfull " << std::endl;
	    }
	}

	ISType is_type;
	if ( type.flags() && CmdArg::GIVEN )
	{
	    if ( type == 'L' )
		is_type = LargeInfo::type();
	    else if ( type == 'M' )
		is_type = MediumInfo::type();
	    else
	    {
		std::cerr << "Wrong type parameter value is given \"" << type << "\"" << std::endl;
		return 1;
	    }
	}

	if ( type_mode.flags() && CmdArg::GIVEN )
	{
	    if ( type_mode == 'I' )
		is_type = !is_type;
	    else if ( type_mode == 'W' )
		is_type = ~is_type;
	    else
	    {
		std::cerr << "Wrong type mode parameter value is given \"" << type_mode << "\"" << std::endl;
		return 1;
	    }
	}

	for ( size_t i = 0; i < expressions.count() || ( type.flags() && CmdArg::GIVEN ); )
	{
	    ISCriteria criteria( "" );
	    if ( i < expressions.count() && ( type.flags() && CmdArg::GIVEN ) )
	    {
		if ( logic == 'A' )
		    criteria = (const char*)expressions[i] && is_type;
		else if ( logic == 'O' )
		    criteria = (const char*)expressions[i] || is_type;
		else
		{
		    std::cerr << "Wrong logic parameter value is given \"" << logic << "\"" << std::endl;
		    return 1;
		}
	    }
	    else if ( i < expressions.count() )
	    {
		criteria = ISCriteria( (const char*)expressions[i] );
	    }
	    else if ( type.flags() && CmdArg::GIVEN )
	    {
		criteria = ISCriteria( is_type );
	    }

	    if ( i%3 == 0 )
	    {
		if ( value )
		    receiver.subscribe( (const char*)server_name, criteria, info_callback, nullptr, emask );
		else
		    receiver.subscribe( (const char*)server_name, criteria, event_callback, nullptr, emask );

		std::cout << "Query Subscribe to " << criteria << " successfull " << std::endl;
	    }

	    if ( i%3 == 1 )
	    {
		if ( value )
		    receiver.subscribe( (const char*)server_name, criteria, InfoCallback(), nullptr, emask );
		else
		    receiver.subscribe( (const char*)server_name, criteria, EventCallback(), nullptr, emask );

		std::cout << "Query Subscribe to " << criteria << " successfull " << std::endl;
	    }

	    if ( i%3 == 2 )
	    {
		if ( value )
		    receiver.subscribe( (const char*)server_name, criteria, &CustomInfoCallback::callback, &custom_info_callback, emask );
		else
		    receiver.subscribe( (const char*)server_name, criteria, &CustomEventCallback::callback, &custom_event_callback, emask );

		std::cout << "Query Subscribe to " << criteria << " successfull " << std::endl;
	    }

	    if ( ++i >= expressions.count() )
		break;
	}

	int sig = daq::ipc::signal::wait_for();

	std::cout << " Signal " << sig << " received." << std::endl;
	
        if ( notunsubscribe )
        {
            return 0;
        }
        
	std::cout << " ( Unsubscribing ..." ;
	for ( size_t i = 0; i < info_names.count(); i++ )
	{
	    std::string name(server_name);
	    name += ".";
	    name += info_names[i];
	    receiver.unsubscribe(name);
	    std::cerr << "Unsubscribe to " << info_names[i] << " successfull " << std::endl;
	}

	for ( size_t i = 0; i < expressions.count() || ( type.flags() && CmdArg::GIVEN ); )
	{
	    ISCriteria criteria( "" );
	    if ( i < expressions.count() && ( type.flags() && CmdArg::GIVEN ) )
	    {
		if ( logic == 'A' )
		    criteria = (const char*)expressions[i] && is_type;
		else if ( logic == 'O' )
		    criteria = (const char*)expressions[i] || is_type;
		else
		{
		    std::cerr << "Wrong logic parameter value is given \"" << logic << "\"" << std::endl;
		    return 1;
		}
	    }
	    else if ( i < expressions.count() )
	    {
		criteria = ISCriteria(std::string(expressions[i]));
	    }
	    else if ( type.flags() && CmdArg::GIVEN )
	    {
		criteria = ISCriteria(is_type);
	    }

	    receiver.unsubscribe( (const char*)server_name, criteria );
	    std::cerr << "Query Unsubscribe with " << criteria << " successfull " << std::endl;

	    if ( ++i >= expressions.count() )
		break;
	}
	std::cerr << " done )" << std::endl;
    }
    catch( daq::is::Exception & ex )
    {
    	is::fatal( ex );
    }

    return 0;
}
