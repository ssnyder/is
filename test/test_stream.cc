////////////////////////////////////////////////////////////////////////
//            p_source.cc
//
//            Test application for the IS library
//
//            Sergei Kolos,    August 2002
//
//            description:
//                measures the performance of the IS update operation
////////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <stdio.h>
#include <math.h>

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>

#include <ipc/core.h>
#include <ipc/partition.h>

#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <owl/timer.h>

#include <is/test/LargeInfo.h>
#include <is/test/MediumInfo.h>
#include <is/infodictionary.h>
#include <is/infostream.h>
#include <is/infoT.h>

ERS_DECLARE_ISSUE(is, StreamFailed,
        "IS stream contains only '" << number << "' objects", ((int)number ))

void stat(const std::vector<double> & data, double & avg, double & dev) {
    avg = 0.;
    for (size_t i = 0; i < data.size(); i++) {
        avg += data[i];
    }
    avg /= data.size();

    dev = 0.;
    for (size_t i = 0; i < data.size(); i++) {
        dev += pow(data[i] - avg, 2);
    }
    dev = sqrt(dev/data.size());
}

int main(int argc, char ** argv) {
    try {
        IPCCore::init(argc, argv);
    } catch (daq::ipc::Exception & ex) {
        is::fatal(ex);
    }

    CmdArgStr partition_name('p', "partition", "partition-name",
            "partition to work in.");
    CmdArgStr suffix('S', "suffix", "name_suffix",
            "suffix for generated information names", CmdArg::isREQ);
    CmdArgChar size('s', "size", "object_size",
            "define size of the information objects to be used\n"
                    "S - small objects [one integer attribute]\n"
                    "M - medium objects (default) [one attribute of each basic type]\n"
                    "L - large objects [one plain attribute plus one array attribute of each basic type]\n");
    CmdArgBool quiet('q', "quiet", "print only summary lines to standard error.");
    CmdArgInt info_num('N', "number", "information-number",
            "number of informations to be created ( 10 by default ).");
    CmdArgInt repeate_num('r', "repetition", "repetition-number",
            "number of repetitions (1000 by default).");
    CmdArgStr server_name('n', "server", "server-name", "server to work with.",
            CmdArg::isREQ);

    CmdLine cmd(*argv, &quiet, &partition_name,
            &server_name, &info_num, &repeate_num, &suffix, &size, NULL);
    CmdArgvIter arg_iter(--argc, ++argv);

    size = 'M';
    repeate_num = 1000;
    info_num = 10;

    cmd.description("This program is part of the IS performance test suit.\n");

    cmd.parse(arg_iter);

    is::test::LargeInfo li;
    is::test::MediumInfo mi;
    ISInfoLong isl(400000000);

    ISInfo * info;
    switch (size) {
    case 'L':
        info = &li;
        break;
    case 'M':
        info = &mi;
        break;
    case 'S':
        info = &isl;
        break;
    default:
        std::cerr << "[" << size << "] - invalid value for the --size parameter "
                << std::endl;
        return 1;
    }

    IPCPartition partition(partition_name);
    ISInfoDictionary id(partition);
    for (int i = 0; i < info_num; i++) {
        char tmp[256];
        sprintf(tmp, "%s.%s.%d", (const char*)server_name, (const char*)suffix, i);
        try {
            id.insert(tmp, *info);
        } catch (daq::is::Exception & ex) {
            ers::error(ex);
        }
    }

    std::cerr << "IS stream test started : info size is \"" << size << "\", info number is " << info_num;
    std::cerr << ", repetitions number is " << repeate_num << std::endl;

    std::cout.setf(std::ios::fixed, std::ios::floatfield);
    std::cout << std::setprecision(2);

    std::vector<double> data;
    double tmin = 100000000.;
    double tmax = 0.;

    ISCriteria criteria(std::string((const char*)suffix) + "\\..*", info->type());
    for (int j = 0; j < repeate_num; j++) {
        OWLTimer timer;
        timer.start();
        try {
            ISInfoStream stream(partition, (const char*)server_name, criteria);

            int cnt = 0;
            while (!stream.eof()) {
                stream.skip(1);
                ++cnt;
            }
            timer.stop();
            if ( !quiet ) {
                std::cout << "IS Stream receives " << cnt << " objects in "
                        << timer.totalTime() * 1000 << " ms" << std::endl;
            }

            if (cnt != info_num) {
                ers::error(is::StreamFailed(ERS_HERE, cnt));
            }
        } catch (ers::Issue & ex) {
            ers::fatal(ex);
            return 1;
        }

        data.push_back(timer.totalTime());
        tmin = (tmin < timer.totalTime()) ? tmin : timer.totalTime();
        tmax = (tmax > timer.totalTime()) ? tmax : timer.totalTime();
    }

    double avg, dev;
    stat(data, avg, dev);
    std::cout << "IS stream test finished : " << "{ min/avg/max/dev times = " << tmin * 1000. << "/"
            << avg * 1000. << "/" << tmax * 1000. << "/" << dev * 1000. << " ms }" << std::endl;

    for (int i = 0; i < info_num; i++) {
        char tmp[256];
        sprintf(tmp, "%s.%s.%d", (const char*) server_name, (const char*) suffix, i);
        try {
            id.remove(tmp);
        } catch (daq::is::Exception & ex) {
            ers::error(ex);
        }
    }

    return 0;
}

