////////////////////////////////////////////////////////////////////////
//        p_receiver.cc
//
//        Test application for the IS library
//
//        Sergei Kolos,    January 2000
//
//        description:
//                measure the everage time for the IS callback invocation 
//    for the following events:
//       - create information
//       - update information
//       - delete information
////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <atomic>

#include <ers/ers.h>
#include <owl/timer.h>
#include <cmdl/cmdargs.h>

#include <is/infoany.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>

#include <ipc/core.h>
#include <ipc/signal.h>

int		m_active_callbacks;		
int		Updates = 1;
std::atomic_int	Count(0);
int		Verbose = 0;
int		Delay = 0;

void callback( const ISCallbackInfo * isc )
{    
    static OWLTimer t;
    
    if ( Count == 0 )
    {
	t.start();	// Record starting time
    }
         
    if ( Verbose )
	std::cout << "\"" << isc->name()
        	  << "\" " << isc->reason()
                  << " \"" << OWLTime( OWLTime::Microseconds ).total_mksec_utc() - isc->time().total_mksec_utc()
                  << "\"" << std::endl;
    
    if ( Delay )
    	usleep( Delay );
    
    if ( Verbose > 1 )
    {
	ISInfoAny isa;
	isc->value(isa);
	std::cout << isa;
    }

    if ( Verbose > 2 )
    {
	ISInfoAny isa;
	isc->value(isa);
	std::cout << isa;
    }

    if ( ++Count%Updates == 0 )
    {
	t.stop();
        std::cout << "Receiver " << getpid() << " has been notified " << Updates << " times." << std::endl;
        std::cout << "Mean Time for 1 update is : " << t.totalTime()/Updates << " seconds" << std::endl;
	t.reset();
        t.start();
    } 
}

void evt_callback( const ISCallbackEvent * isc )
{    
    static OWLTimer t;
    
    if ( Count == 0 )
    {
	t.start();	// Record starting time
    }
         
    if ( Verbose )
	std::cout << "\"" << isc->name()
        	  << "\" " << isc->reason()
                  << " \"" << OWLTime( OWLTime::Microseconds ).total_mksec_utc() - isc->time().total_mksec_utc()
                  << "\"" << std::endl;    
       
    if ( Delay )
    	usleep( Delay );
        
    if ( ++Count%Updates == 0 )
    {
	t.stop();
        std::cout << "Receiver " << getpid() << " has been notified " << Updates << " times." << std::endl;
        std::cout << "Mean Time for 1 update is : " << t.totalTime()/Updates << " seconds" << std::endl;
	t.reset();
        t.start();
    }
}

int main(int argc, char ** argv)
{
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
    }
    
        // Declare arguments
    CmdArgStr		partition_name('p', "partition", "partition-name", "partition to work in.");
    CmdArgStrList	server_names('n', "server", "[server-name ...]", "servers to subscribe to.",CmdArg::isREQ);
    CmdArgInt		verbose ('v', "verbose", "verbose-level", "set up verbosity level (1 or 2).");
    CmdArgInt		updates('u', "update", "update-number", "after this number of updates the receiver prints timing.");
    CmdArgBool		sync_mode('s', "sync", "synchronize callbacks.");
    CmdArgBool		event ('e', "event", "use event subscription mode.");
    CmdArgInt		delay ('d', "delay", "delay", "delay for processing callback (us) default is 0.");

 
         // Declare command object and its argument-iterator
    CmdLine    cmd(*argv, &verbose, &partition_name, &server_names, &updates, &sync_mode, &event, &delay, NULL);
    CmdArgvIter    arg_iter(--argc, ++argv);
 
    updates = 1;
    delay = 0;
    cmd.description(	"This program implements performance tests for the Information Service.\n"
			"It prints for every callback invocation the following information:\n"
			"\"info name\" \"reason\" \"difference between current time and information time in seconds\".");

    // Parse arguments
    cmd.parse(arg_iter);
    
    Verbose = verbose;
    Updates = updates;
    Delay = delay;
    
    IPCPartition	partition(partition_name);
    ISInfoReceiver	ir(partition,sync_mode);
        
    ISCriteria criteria( std::string(".*") );
    try
    {
	for ( size_t i=0; i < server_names.count(); i++ )
	{
	    if ( event )
		ir.subscribe( (const char*)server_names[i], criteria, evt_callback, &partition );
	    else
		ir.subscribe( (const char*)server_names[i], criteria, callback );
	    std::cout << "Subscribe to all successfull " << std::endl;
	}

        int sig = daq::ipc::signal::wait_for();

	std::cout << " Signal " << sig << " received." << std::endl;

	std::cout   << "Receiver " << getpid() << " has been notified "
		    << Count << " times." << std::endl;

	std::cout << " Exiting ..." ;
	std::cout << " ( Unsubscribing ..." ;
	for ( size_t i = 0; i < server_names.count(); i++ )
	{
	    ir.unsubscribe( (const char*)server_names[i], criteria );
	    std::cout << "unsubscribe to all successfull ";
	}
	std::cout << " Done )";
	std::cout << " done." << std::endl;
    }
    catch( daq::is::Exception & ex )
    {
    	is::fatal( ex );
    }
    
    return 0; 
}
