package is.tool;

import java.util.StringTokenizer;
import java.util.Date;
import java.util.Locale;
import java.text.DateFormat;
import java.io.PrintWriter;

abstract class Generator implements CodeProducer
{
    void printClassComment( PrintWriter out, String description )
    {
	String comment = "/**\n";

	StringTokenizer st = new StringTokenizer( description, "\n" );
	
	while( st.hasMoreTokens() )
	{
	    comment += " * " + st.nextToken() + "\n";
	}
	comment += " * \n"
		+  " * @author  produced by the IS generator\n"
		+  " */\n";
	
	out.println( comment );
    }
    
    void printAttributeComment( PrintWriter out, String description )
    {	    
	String comment = "    /**\n";
	
	StringTokenizer st = new StringTokenizer( description, "\n" );
	while( st.hasMoreTokens() )
	{
	    comment +=  "     * " + st.nextToken() + "\n";
	}
	comment += "     */";
	out.println( comment );
    }

    void printUserCodeComments( PrintWriter out )
    {
	out.println( );
	out.println( CodeWriter.beginUserCode );
	out.println( );
	out.println( CodeWriter.endUserCode );    
    }
}
