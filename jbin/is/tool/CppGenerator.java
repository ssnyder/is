package is.tool;

import java.io.*;
import java.util.*;


class CppGenerator extends Generator
{    
    static private final String	SIZE_SUFFIX = "_size";
    static private final int	MAX_LINE_LENGTH = 80;
    static final java.text.SimpleDateFormat date_formatter 
    		= new java.text.SimpleDateFormat( "dd/MM/yyyy", java.util.Locale.US );
    static final java.text.SimpleDateFormat time_formatter 
    		= new java.text.SimpleDateFormat( "dd/MM/yyyy HH:mm:ss", java.util.Locale.US );
    
    private Hashtable<String,String>	types = new Hashtable<String,String>();
    private int				max_type_length = 30;
    private String			superclass_name;
    
    private void initialize( Repository.Class cl, CodeProducer.Options opt )
    {
	types.clear();
	types.put( "bool",  "bool" );
	types.put( "s8",  "char" );
	types.put( "s16", "short" );
	types.put( "s32", "int" );
	types.put( "s64", "int64_t" );
	types.put( "u8",  "unsigned char" );
	types.put( "u16", "unsigned short" );
	types.put( "u32", "unsigned int" );
	types.put( "u64", "uint64_t" );
	types.put( "float", "float" );
	types.put( "double", "double" );
	types.put( "string", "std::string" );
	types.put( "date", "OWLDate" );
	types.put( "time", "OWLTime" );
	
	superclass_name = cl.getSuperclassName( opt.with_named_template );
	if ( cl.superclass.equals( "Info" ) )
	    superclass_name = "IS" + superclass_name;
    }
    
    public String produceHeader( String filename )
    {
	String str = "#ifndef " + filename.replace( '.', '_' ).toUpperCase() + "\n"
		   + "#define " + filename.replace( '.', '_' ).toUpperCase() + "\n\n";
	return str;
    }
    
    public String produceFooter( String filename )
    {
	return ( "#endif // " + filename.replace( '.', '_' ).toUpperCase() + "\n" );
    }
    
    public String produceCode( Repository.Class cl, CodeProducer.Options opt )
    {
	initialize( cl, opt );
	
	StringWriter sw = new StringWriter( );
	PrintWriter out = new PrintWriter( sw );
	
	declareIncludes( out, cl, opt );
	declareInfo( out, cl, opt );
	
	return sw.toString();
    }
    
    String makeHeaderName( String class_name )
    {
    	//
        // Removes template parameter name from the class name
        //
    	StringTokenizer st = new StringTokenizer( class_name, "<" );
        return st.nextToken();
    }
    
    void declareIncludes( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	String code;

	if ( cl.superclass.equals( "Info" ) )
	{
	    if ( opt.with_named_template )
		code = "#include <is/namedinfo.h>\n\n";
	    else
		code = "#include <is/info.h>\n\n";
	}
	else
	{ 
	    String header_file_name = makeHeaderName( superclass_name );
	    code = "#include \"";
            for ( int i = 0; i < opt.directories.size(); i++ )
		code += opt.directories.get( i ) + "/";
	    code += header_file_name + ".h\"\n";
	}
	
        boolean need_vector = false;
        boolean need_time = false;
        HashSet<String> custom_types = new HashSet<String>();

	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    if ( types.get( a.type ) == null && a.type.indexOf( "enum" ) < 0 )
	    {
		if (	( !a.type.equals( cl.name ) || opt.with_named_template )
                     && !custom_types.contains( a.type ) )
		{
		    custom_types.add( a.type );
		    code += "#include \"";
		    for ( int j = 0; j < opt.directories.size(); j++ )
			code += opt.directories.get( j ) + "/";
		    code += a.type + ".h\"\n";
		}
	    }
	    if ( a.multi_value.booleanValue() && opt.with_vector )
	    {
		need_vector = true;
	    }
	    if (    a.type.equals( "time" )
		 || a.type.equals( "date" ) )
	    {
		need_time = true;
	    }

	    String attribute_type = null;
	    if ( a.multi_value.booleanValue() && opt.with_vector )
	    {
		attribute_type = "std::vector<" + a.type + ">";
	    }
	    else
	    {
		attribute_type = a.type;
	    }
	    max_type_length = max_type_length > ( attribute_type.length() + 3 )
			? max_type_length
			: ( attribute_type.length() + 3 );
	}
	
	code += "#include <string>\n";
	code += "#include <ostream>\n";
	if ( need_vector )
            code += "#include <vector>\n";
	if ( need_time )
            code += "#include <owl/time.h>\n";

	out.println( code );
    }
    
    void declareInfo( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {	
	printUserCodeComments( out );
	
	if ( opt.with_namespace )
        {
	    for ( int i = 0; i < opt.namespaces.size(); i++ )
            	out.println( "\nnamespace " + opt.namespaces.get( i ) + "\n{" );
	}
        
	printClassComment( out, cl.getDescription() );
	
	out.println( "class " + cl.getClassName( opt.with_named_template )
				+ " : public "
				+ superclass_name + " {\npublic:" );

	declareAttributes ( out, cl, opt );
        declareType( out, cl, opt );
	
	if ( opt.with_print_function )
	    declarePrint( out, cl, opt );
            
	if ( opt.with_named_template )
	    declareNamedConstructor( out, cl, opt );
	else
	    declareConstructor( out, cl, opt );
	
	declarePublishGuts( out, cl, opt );
	declareRefreshGuts( out, cl, opt );

	defineConstructor( out, cl, opt );
	
	printUserCodeComments( out );
	out.println( "};" );
	printUserCodeComments( out );
        
	if ( opt.with_print_function )
	    declareOutOperator( out, cl, opt );
        
	if ( opt.with_namespace )
        {
	    for ( int i = 0; i < opt.namespaces.size(); i++ )
            	out.println( "}\n" );
	}
    }
    
    void declareConstructor( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	out.println( "    " + cl.getClassName( opt.with_named_template ) + "( )" );
	out.println( "      : " + superclass_name + "( \"" + cl.getName() + "\" )" );
	out.println( "    {\n\tinitialize();\n    }\n" );
	declareDestructor ( out, cl, opt );
	out.println( "protected:" );
	out.println( "    " + cl.getClassName( opt.with_named_template ) + "( const std::string & type )" );
	out.println( "      : " + superclass_name + "( type )" );
	out.println( "    {\n\tinitialize();\n    }\n" );
    }
    
    void declareNamedConstructor( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	out.println( "    " + cl.getClassName( opt.with_named_template ) + "( const IPCPartition & partition, const std::string & name )" );
	out.println( "      : " + superclass_name + "( partition, name, \"" + cl.getName() + "\" )" );
	out.println( "    {\n\tinitialize();\n    }\n" );
	declareDestructor ( out, cl, opt );
	out.println( "protected:" );
	out.println( "    " + cl.getClassName( opt.with_named_template ) + "( const IPCPartition & partition, const std::string & name, const std::string & type )" );
	out.println( "      : " + superclass_name + "( partition, name, type )" );
	out.println( "    {\n\tinitialize();\n    }\n" );
    }

    void defineCopyStuff( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
    	if ( opt.with_vector )
            return;	// default copy constructor and copy oerator are fine
	
	boolean arrays_found = false;
        for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    
	    if ( a.multi_value.booleanValue() )
            {// there are arrays in this class
            	arrays_found = true;
                break;
            }
        }
        if ( arrays_found )
        {
	    out.println( "    " + cl.getClassName( opt.with_named_template ) + "( const " 
            			+ cl.getClassName( opt.with_named_template ) + " & );" );
	    out.println( "    " + cl.getClassName( opt.with_named_template ) + "& operator=( const " 
            			+ cl.getClassName( opt.with_named_template ) + " & );" );
	    out.println( );
        }
    }
    
    void defineConstructor( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	out.println( "private:" );
        defineCopyStuff( out, cl, opt );
	out.println( "    void initialize()" );
	out.println( "    {" );
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    if ( a.init_value.length() == 0 )
	    {
		if ( a.multi_value.booleanValue() && !opt.with_vector )
		{
		    out.println( "\t" + a.getName() + " = 0;" );
		    out.println( "\t" + a.getName() + SIZE_SUFFIX + " = 0;" );
		}
		continue;
	    }
	    
	    String t = types.get ( a.type );
	    String n = a.getName();
	    if (    ( n.equals( "name" ) && opt.with_named_template )
		 || ( n.equals( "partition" ) && opt.with_named_template )
		 ||   n.equals( "type" ) )
	    {
		n = "this -> " + n;
	    }
	    String init_start = "";
	    String init_end = "";
	    if (    a.type.equals( "date" )
		 || a.type.equals( "time" ) )
	    {
		init_start = t + "( \"";
		init_end = "\" )";		
	    }
	    else if ( a.type.equals( "string" ) )
	    {
		init_start = "\"";
		init_end = "\"";		
	    }
	    
	    if ( a.multi_value.booleanValue() )
	    {	    
		StringTokenizer st = new StringTokenizer( a.init_value, "," );
		int size = st.countTokens();
		if ( opt.with_vector )
		{
		    if ( size > 0 )
		    	out.println( "\t" + n + ".resize( " + size + " );" );
		}
		else
		{
		    out.println( "\t" + n + " = new " + t + "[" + n + SIZE_SUFFIX + " = " + size + "];" );
		}
                
		for( int j = 0; j < size; j++ )
		{
		    String init_value = st.nextToken().trim();
		    if ( a.type.equals( "date" ) )
		    {
			init_value = convertDate( init_value );
		    }
		    else if ( a.type.equals( "time" ) )
		    {
			init_value = convertTime( init_value );
		    }
		    out.println( "\t" + n + "[" + j + "] = " + init_start + init_value + init_end + ";" );
		}                
	    }
	    else
	    {
		String init_value = a.init_value;
                if ( a.type.equals( "date" ) )
                {
                    init_value = convertDate( init_value );
                }
		else if ( a.type.equals( "time" ) )
                {
                    init_value = convertTime( init_value );
                }
		out.println( "\t" + n + " = " + init_start + init_value + init_end + ";" );
	    }
	}

	printUserCodeComments( out );

	out.println( "    }" );
	out.println( );
    }
    
    void declareDestructor( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	out.println( "    ~" + cl.getClassName( opt.with_named_template ) + "(){" );
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    
	    if ( a.multi_value.booleanValue() && !opt.with_vector )
	    {	    
		out.println( "\tif ( " + a.getName() + SIZE_SUFFIX + " != 0 ) delete[] " + a.getName() + ";" );
	    }
	}

	printUserCodeComments( out );

	out.println( "    }" );
	out.println( );
    }
    
    void declarePrint( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	out.println( "    std::ostream & print( std::ostream & out ) const {" );

	out.println( "\t" + superclass_name + "::print( out );" );

	out.println( "\tout << std::endl;" );
        
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	     Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	     if ( a.multi_value.booleanValue() )
	     {
		String SizeVar;
                if ( opt.with_vector )
                {
                    SizeVar = a.getName() + ".size()";
		}
                else
                {
                    SizeVar = a.getName() + SIZE_SUFFIX;
                }
		out.println("\tout << \"" + a.getName() + "[\" << " + SizeVar + " << \"]:\\t// " + a.getDescription() + "\" << std::endl;" );
		out.println("\tfor ( size_t i = 0; i < " + SizeVar + "; ++i )");
		out.println("\t    out << i << \" : \" << " + a.getName() + "[i] << std::endl;");
            }
            else
	    {
		out.print("\tout << \"" + a.getName() + ": \" << " + a.getName() + " << \"\\t// " + a.getDescription() + "\"" );
                out.println( i != cl.attributes.size() - 1 ? " << std::endl;" : ";" );
	    }
	}
	out.println("\treturn out;");
	out.println( "    }" );
	out.println( );
    }

    void declareOutOperator( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	out.println( "inline std::ostream & operator<<( std::ostream & out, const " + cl.getClassName( opt.with_named_template ) + " & info ) {" );
	out.println("    info.print( out );");
	out.println("    return out;");
	out.println( "}" );
	out.println( );
    }

    void declarePublishGuts( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	out.println( "    void publishGuts( ISostream & out ){" );
	
	if ( !cl.superclass.equals( "Info" ) )
	{
	    out.println( "\t" + superclass_name + "::publishGuts( out );" );
	}
	
	String output = "\tout";
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    if ( a.multi_value.booleanValue() && !opt.with_vector )
	    {
                if ( !output.equals( "\tout" ) )
		{
		    out.println( output + ";" );
		    output = "\tout";
		}
		out.println( "\tout.put( " + a.getName() + ", " + a.getName() + SIZE_SUFFIX + " );" );
	    }
	    else
	    {
		if ( output.length() > MAX_LINE_LENGTH )
		{
		    out.println( output + ";" );
		    output = "\tout";
		}
		output += " << " + a.getName();
	    }
	}
	if ( !output.equals( "\tout" )  )
	{
	    out.println( output + ";" );
	}
	
	out.println( "    }" );
	out.println( );
    }
    
    void declareRefreshGuts( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	out.println( "    void refreshGuts( ISistream & in ){" );
	
	if ( !cl.superclass.equals( "Info" ) )
	{
	    out.println( "\t" + superclass_name + "::refreshGuts( in );" );
	}
	
	String output = "\tin";
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    if ( a.multi_value.booleanValue() && !opt.with_vector )
	    {
		if ( !output.equals( "\tin" ) )
		{
		    out.println( output + ";" );
		    output = "\tin";
		}
		String n = a.getName();
		out.println( "\tdelete[] " + n + ";" );
		out.println( "\tin.get( &" + n + ", " + n + SIZE_SUFFIX + " );" );
	    }
	    else
	    {
		if ( output.length() > MAX_LINE_LENGTH )
		{
		    out.println( output + ";" );
		    output = "\tin";
		}
		output += " >> " + a.getName();
	    }
	}
	
	if ( !output.equals( "\tin" )  )
	{
	    out.println( output + ";" );
	}
	
	out.println( "    }" );
	out.println( );
    }
    
    void declareAttributes( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	declareEnumerations( out, cl );
	
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    
	    printAttributeComment( out, a.getDescription() );
	    
	    String star = new String ( a.multi_value.booleanValue() && !opt.with_vector ? "*" : "" );
	    String attribute_type = types.get( a.type );
            if ( attribute_type == null )
            	attribute_type = a.type;
                
	    if ( a.multi_value.booleanValue() && opt.with_vector )
	    {
	    	attribute_type = "std::vector<" + attribute_type + ">";
	    }
	    
	    String s1 = attribute_type + " " + star;
	    s1 += addSpaces( max_type_length - s1.length() );
	    String s2 = a.getName() + ";";
	    out.println( "    " + s1 + s2 );

	    if ( a.multi_value.booleanValue() && !opt.with_vector )
	    {
		out.println( );
		out.println( "    /**\n     * size of the " + a.getName() + " array\n     */" );
		s1 = "size_t ";
		s1 += addSpaces( max_type_length - s1.length() );
		s2 = a.getName() + SIZE_SUFFIX + ";";
		out.println( "    " + s1 + s2 );
	    }
	    out.println( );
	}
	out.println( );
    }

    void declareEnumerations( PrintWriter out, Repository.Class cl )
    {
	for ( int i = 0; i < cl.attributes.size(); i++ )
	{
	    Repository.Attribute a = (Repository.Attribute)cl.attributes.get(i);
	    String attribute_type = types.get ( a.type );
	    if ( attribute_type == null && a.type.indexOf( "enum" ) >= 0 )
	    {
		attribute_type = StringUtil.InvertCapitalisation( a.getName() );
		types.put( a.type, attribute_type );
		out.print( "    enum " + attribute_type + " {" );
		out.println( a.type.substring( a.type.indexOf( "{" ) + 1 ) + ";\n" );
		max_type_length = max_type_length > ( attribute_type.length() + 3 )
			    ? max_type_length
			    : ( attribute_type.length() + 3 );
	    }
	}
	out.println( );
    }
    
    void declareType( PrintWriter out, Repository.Class cl, CodeProducer.Options opt )
    {
	String params = ( opt.with_named_template ) ? " IPCPartition(), \"\" " : " ";
	out.println( "    static const ISType & type() {" );
	out.println( "\tstatic const ISType type_ = " 
            		+ cl.getClassName( opt.with_named_template ) 
                        + "(" + params + ").ISInfo::type();" );
	out.println( "\treturn type_;" );
	out.println( "    }" );
	out.println( );
    }
    
    String addSpaces( int size )
    {
        char spaces[] = new char[size];
	for ( int i = 0; i < size; i++ )
	    spaces[i] = ' ';
	return new String ( spaces );
    }
    
    String convertDate( String date )
    {
        return date_formatter.format( new is.Date( date ) );
    }
    
    String convertTime( String time )
    {
        is.Time t = new is.Time( time );
        
        return ( time_formatter.format( t ) + '.' + String.valueOf( t.getTimeMicro() % 1000000 ) );
    }
}
