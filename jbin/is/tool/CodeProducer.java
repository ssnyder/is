package is.tool;

import java.util.Vector;

interface CodeProducer
{
    public class Options
    {
    	public boolean		with_vector = true;
	public boolean		with_comments = true;
	public boolean		with_copy_semantic = true;
    	public boolean		with_named_template = false;
    	public boolean		with_namespace = true;
    	public boolean		with_print_function = true;
    	public Vector<String>	namespaces = new Vector<String>();
    	public Vector<String>	directories = new Vector<String>();
    }
    
    String produceHeader( String filename );
    String produceFooter( String filename );
    String produceCode	( Repository.Class cl, Options opt );
}
