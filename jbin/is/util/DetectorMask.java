package is.util;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import daq.eformat.*;

class DetectorMask extends JWindow implements MouseListener
{      
    private JPanel detectors_panel = new JPanel();
    private JPanel sub_detectors_panel = new JPanel();
    private static final Color bgColor = new java.awt.Color( 184, 207, 229 );

    DetectorMask( )
    {
	getRootPane().setLayout( new BoxLayout( getRootPane(), BoxLayout.LINE_AXIS ) );
	getRootPane().setBorder( new EmptyBorder( 10, 10 , 10 , 10 ) );
	getRootPane().setBackground( bgColor );

	detectors_panel.setLayout( new BoxLayout( detectors_panel, BoxLayout.PAGE_AXIS ) );
	sub_detectors_panel.setLayout( new BoxLayout( sub_detectors_panel, BoxLayout.PAGE_AXIS ) );
	detectors_panel.setBackground( bgColor );
	sub_detectors_panel.setBackground( bgColor );

	getRootPane().add( detectors_panel );
	getRootPane().add( Box.createRigidArea( new Dimension( 10, 0 ) ) );
	getRootPane().add( sub_detectors_panel );
	getRootPane().addMouseListener(this);
	setAlwaysOnTop( true );
    }

    void setValue( String bitmask )
    {
        detectors_panel.removeAll();
        sub_detectors_panel.removeAll();
        daq.eformat.DetectorMask dmask = daq.eformat.DetectorMask.decode(bitmask);
        
        java.util.Iterator<daq.eformat.DetectorMask.DETECTOR> detectors = dmask.getDetectors().iterator();
        while ( detectors.hasNext() )
        {
            detectors_panel.add( new JLabel( detectors.next().name() ) );
	}
        
        java.util.Iterator<daq.eformat.DetectorMask.SUBDETECTOR> sub_detectors = dmask.getSubdetectors().iterator();
        while ( sub_detectors.hasNext() )
        {
            sub_detectors_panel.add( new JLabel( sub_detectors.next().name() ) );
	}
    }

    public void show( )
    {
        setLocation( MouseInfo.getPointerInfo().getLocation() );
        pack();
        super.show();
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                toFront();
                repaint();
            }
        });
    }
    
    public void mouseClicked(MouseEvent e)
    {
	dispose();
    }

    public void mouseExited(MouseEvent e)
    {
	dispose();
    }

    public void mouseEntered(MouseEvent e) { }
    public void mousePressed(MouseEvent e) { }
    public void mouseReleased(MouseEvent e) { }
}
