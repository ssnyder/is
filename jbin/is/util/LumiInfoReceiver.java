package is.util;

import java.awt.*;
import java.util.*;
import javax.swing.JLabel;

class LumiInfoReceiver extends InfoReceiver
{
    private static final java.text.SimpleDateFormat time_formatter
		 = new java.text.SimpleDateFormat( "HH:mm:ss", java.util.Locale.US );
                 
    LumiInfoReceiver( ipc.Partition partition, Color bg_color )
    {
	super( partition, "RunParams.LumiBlock",
        	new int[] { 1 }, new String[] { "Lumi Block" }, bg_color );
    }

    protected void updateAll( is.AnyInfo info )
    {                
	try {
	    String s = info.getAttribute( 1 ).toString();
	    java.util.Date d = new java.util.Date((long)(Integer)info.getAttribute( 2 ) * 1000);
            update( 0, s );
            values[0].setToolTipText( time_formatter.format( d ) );
	}
	catch( Exception ex ) {
	    super.updateAll( info );
	}        	
    }
}

