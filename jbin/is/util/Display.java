/*
 * Display.java
 *
 * Created on 18 July 2007
 *
 * Serguei Kolos UCI
 *
 */
 
package is.util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Display extends JFrame
{    
    public Display( String partition_name )
    {
	setUndecorated( true );
	getRootPane().setWindowDecorationStyle( JRootPane.PLAIN_DIALOG );
        setContentPane( new MainFrame( partition_name, this ) );        
	setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        setTitle( partition_name + " Run Status" );
    }
    
    public static void main( String ac[] )
    {    
	if ( ac.length < 1 )
        {
            System.err.println( "USAGE: is.util.Display <partition name> " );
            return;
        }

        Display dialog = new Display( ac[0] );
        dialog.pack();
        dialog.show();
    }
}
