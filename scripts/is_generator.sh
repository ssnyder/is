#! /bin/sh
#
#--------------------------------------------------------------------
# 
# The is_generator.sh script is used to run the IS generator.
# 
# Use 'is_generator.sh --help' to learn more about generator options.
# 
#--------------------------------------------------------------------
#
# Written 20/08/99 by Serguei Kolos <Serguei.Kolos@cern.ch>
#
# Modified:
#	-
#
#--------------------------------------------------------------------

params=$@

if test -x "${TDAQ_JAVA_HOME}/bin/java"
then
	JAVA_VM="${TDAQ_JAVA_HOME}/bin/java"
else
	if test -x "${JAVA_HOME}/bin/java"
	then
		JAVA_VM="${JAVA_HOME}/bin/java"
	else
		echo "ERROR:: Java virtual machine not found."
		echo "HINT::  Set TDAQ_JAVA_HOME environment variable to the home directory of JDK v 1.3 or higher."
		exit
	fi
fi

${JAVA_VM} -classpath ${CLASSPATH} is.tool.CodeGenerator $@

################################################################################
