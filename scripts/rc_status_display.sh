#!/bin/sh

if test $# -lt 1 ; then
        echo "Usage: rc_status_display.sh <partition name>"
        exit 1
fi

is_util_file=`echo $0 | sed -e 's/\/bin\/rc_status_display.sh/\/lib\/is_util.jar/g'`

$TDAQ_JAVA_HOME/bin/java -classpath ${is_util_file}:$TDAQ_CLASSPATH is.util.Display $1
